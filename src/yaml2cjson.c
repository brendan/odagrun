/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file yalm2cjson.c
 *  \brief Parse yaml and json to a cJSON object
 *  \author Created by Danny Goossen on 10/5/2017.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */


#include "deployd.h"
#include "yaml2cjson.h"

/**
 \brief set object type and content of unquoted, unfolded and non Litural value item
 \param in_buffer value to process
 \return a cJSON object
 */
cJSON * process_YAML_PLAIN_SCALAR_STYLE(char * in_buffer);

cJSON * process_YAML_PLAIN_SCALAR_STYLE(char * in_buffer)
{
	cJSON * result=NULL;
	// check bool / NULL
	if (!in_buffer) return NULL;
	if (strlen(in_buffer)==0) return (cJSON_CreateString(""));
	
	char * buffer=NULL;
	buffer=strdup(in_buffer);
	if (buffer)
	{
		if( strchr(buffer,'.')==NULL)
		{
			char * errCheck;
			int i = (int)strtol(buffer, &errCheck,(10));
			if(errCheck == buffer+strlen(buffer))
				result=cJSON_CreateNumber(i);
		}
		oc_free(&buffer);
		buffer=NULL;
	}
	if (result) return result;
	buffer=strdup(in_buffer);
	if (buffer)
	{
		char * errCheck;
		double f = strtod(buffer, &errCheck);
		if(errCheck == buffer+strlen(buffer))
			result=cJSON_CreateNumber(f);
		oc_free(&buffer);
		buffer=NULL;
	}
	if (result) return result;
	buffer=strdup(in_buffer);
	if (buffer)
	{
		upper_string(buffer);
		size_t len=strlen(buffer);
		if (len==4 && strncmp((const char *)buffer, "TRUE", 4)==0)
			result=cJSON_CreateTrue();
		else if (len==5 && strncmp((const char *)buffer, "FALSE", 5)==0)
			result=cJSON_CreateFalse();
		else if (len==3 && strncmp((const char *)buffer, "YES", 3)==0)
			result=cJSON_CreateTrue();
		else if (len==2 && strncmp((const char *)buffer, "NO", 2)==0)
			result=cJSON_CreateFalse();
		else if (len==2 && strncmp((const char *)buffer, "ON", 2)==0)
			result=cJSON_CreateTrue();
		else if (len==3 && strncmp((const char *)buffer, "OFF", 3)==0)
			result=cJSON_CreateFalse();
		else if (len==1 && strncmp((const char *)buffer, "Y", 1)==0)
			result=cJSON_CreateTrue();
		else if (len==1 && strncmp((const char *)buffer, "N", 1)==0)
			result=cJSON_CreateFalse();
		else if (len==4 && strncmp((const char *)buffer, "NULL", 4)==0)
			result=cJSON_CreateNull();
		oc_free(&buffer);
		buffer=NULL;
	}
	
	if (result) return result;
	else
		result=cJSON_CreateString(in_buffer);
	return result;
}

// doesn't print styles.
static cJSON * yaml_node_json(yaml_document_t *document_p, yaml_node_t *node,cJSON* env_vars)
{
   static int x = 0;
   x++;
   int node_n = x;
   cJSON * result=NULL;
   yaml_node_t *next_node_p;

   switch (node->type) {
      case YAML_NO_NODE:
         printf("Empty node(%d):\n", node_n);
         break;
      case YAML_SCALAR_NODE:
      {
         if (strcmp((const char*)node->tag,YAML_NULL_TAG)==0)
            result=cJSON_CreateNull();
         else if (strcmp((const char*)node->tag,YAML_STR_TAG)==0)
		 {
			if (node->data.scalar.style==YAML_PLAIN_SCALAR_STYLE)
			{
				char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
			    if (buf)
				{
					result=process_YAML_PLAIN_SCALAR_STYLE(buf);
					oc_free(&buf);
				}
			}
			 else if (node->data.scalar.style==YAML_DOUBLE_QUOTED_SCALAR_STYLE && env_vars)
			 {
				 char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
				 if (buf)
				 {
					 // todo, rework to not 3 times copy buffer!!!!
				     if (strchr(buf,'$'))
					 {
						 char *tmp=resolve_const_vars(buf,env_vars);
						 if (tmp)
						 {
							 result=cJSON_CreateString(tmp);
							 oc_free(&tmp);
						 }
						 else
							 result=cJSON_CreateString_n((const char* )node->data.scalar.value, node->data.scalar.length);
					 }
					 else
						 result=cJSON_CreateString_n((const char* )node->data.scalar.value, node->data.scalar.length);
					 oc_free(&buf);
				 }
			 }
			else
            	result=cJSON_CreateString_n((const char* )node->data.scalar.value, node->data.scalar.length);
		 }
		 else if (strcmp((const char*)node->tag,YAML_BOOL_TAG)==0)
         {
            upper_string_n((char*)node->data.scalar.value, node->data.scalar.length);
            if (node->data.scalar.length==4 && strncmp((const char *)node->data.scalar.value, "TRUE", 4)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==5 && strncmp((const char *)node->data.scalar.value, "FALSE", 5)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==3 && strncmp((const char *)node->data.scalar.value, "YES", 3)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==2 && strncmp((const char *)node->data.scalar.value, "NO", 2)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==2 && strncmp((const char *)node->data.scalar.value, "ON", 2)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==3 && strncmp((const char *)node->data.scalar.value, "OFF", 3)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "Y", 1)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "N", 1)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "1", 1)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "0", 1)==0)
               result=cJSON_CreateFalse();
            else
			{
				char * errCheck;
				char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
				int i = (int)strtol(buf, &errCheck,(10));
				oc_free(&buf);
				if(errCheck == buf)
					result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
				else
					result=cJSON_CreateNumber(i);
			}
//TODO why was this here?
               //result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
         }
         else if (strcmp((const char*)node->tag,YAML_INT_TAG)==0)
         {
            char * errCheck;
            char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
            int i = (int)strtol(buf, &errCheck,(10));
            oc_free(&buf);
            if(errCheck == buf)
               result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
            else
               result=cJSON_CreateNumber(i);
         }
         else if (strcmp((const char*)node->tag,YAML_FLOAT_TAG)==0)
         {
            char * errCheck;
            char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
            float f = (float)strtod(buf, &errCheck);
            oc_free(&buf);
            if(errCheck == buf)
               result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
            else
               result=cJSON_CreateNumber(f);
         }
         else if (strcmp((const char*)node->tag,YAML_TIMESTAMP_TAG)==0)
            result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
         break;
      }
      case YAML_SEQUENCE_NODE:
      {
         yaml_node_item_t *i_node;
         cJSON * temp=NULL;
         for (i_node = node->data.sequence.items.start; i_node < node->data.sequence.items.top; i_node++) {
            next_node_p = yaml_document_get_node(document_p, *i_node);
            if (next_node_p)
            {
               temp=yaml_node_json(document_p, next_node_p,env_vars);
               if (temp)
               {
                  if (result==NULL ) result=cJSON_CreateArray();
                  cJSON_AddItemToArray(result, temp);
               }
            }
         }
         break;
      }
      case YAML_MAPPING_NODE:
      {
         yaml_node_pair_t *i_node_p;
         cJSON * temp=NULL;
         for (i_node_p = node->data.mapping.pairs.start; i_node_p < node->data.mapping.pairs.top; i_node_p++) {

            next_node_p = yaml_document_get_node(document_p, i_node_p->value);
            if (next_node_p) {
               temp=yaml_node_json(document_p, next_node_p,env_vars);
            }
            next_node_p = yaml_document_get_node(document_p, i_node_p->key);
            if (next_node_p && next_node_p->type==YAML_SCALAR_NODE)
            {
               if (result==NULL) result=cJSON_CreateObject();
               if (strncmp("<<", (const char*)next_node_p->data.scalar.value, next_node_p->data.scalar.length)==0)
               { // merge, deconstruct temp
                  if (temp && temp->type==cJSON_Object)
                  {
                     cJSON * temp_item= temp->child;
                     temp->child=NULL;
                     cJSON * traverse;
                     temp->child=NULL;
                     if (result->child)
                     {
                        traverse=result->child;
                        while ( traverse->next !=NULL) traverse=traverse->next;
                        traverse->next=temp_item;
                        temp_item->prev=traverse;
                     }
                     else
                     {
                        result->child=temp_item;
                     }
                  } else if (temp && temp->type==cJSON_Array)
                  {
                     // loop the array and add children to result
                     cJSON * pos=NULL;
                     int item=0;
                     cJSON * traverse;
                     cJSON_ArrayForEach(pos,temp)
                     {
                        if (result->child)
                        {
                           traverse=result->child;
                           while ( traverse->next !=NULL) traverse=traverse->next;
                           traverse->next=pos->child;
                           pos->child=NULL;
                        }
                        else
                        {
                           result->child=pos->child;
                           pos->child=NULL;
                        }
                        item ++;
                     }
                  }
                  else
                  {
                     cJSON_AddItemToObject(result, "", temp);
                  }
                  cJSON_Delete(temp);
               }
               else
			   {
				   if (temp)
                     cJSON_AddItemToObject_n(result, (const char*)next_node_p->data.scalar.value, next_node_p->data.scalar.length, temp);
				   else
					  cJSON_AddItemToObject_n(result, (const char*)next_node_p->data.scalar.value, next_node_p->data.scalar.length, cJSON_CreateObject());
			   }
            }else if (next_node_p)
            {
               cJSON * temp_key=yaml_node_json(document_p, next_node_p,env_vars);
               if (temp_key)
			   {
					char * special_key=cJSON_PrintUnformatted(temp_key);
					char * print_key=NULL;
					// TODO can maybe do something with raw
					asprintf(&print_key,".error_unsupported_key: %s",special_key);
					cJSON_AddItemToObject(result,print_key,temp);
					oc_free(&special_key);
					oc_free(&print_key);
               }
            }
         }
         break;
      }
      default:
         break;
   }
   return result;
}

static void yaml_document_2_cJSON(yaml_document_t *document_p, cJSON ** json_doc,cJSON*env_vars)
{
   cJSON * result=NULL;
   result=yaml_node_json(document_p, yaml_document_get_root_node(document_p),env_vars);
   if (result && *json_doc==NULL) *json_doc=cJSON_CreateArray();
   if (result) cJSON_AddItemToArray(*json_doc,result);
}

cJSON * yaml_sting_2_cJSON_env ( __attribute__((unused))void * opaque , cJSON * env_vars,const char * yaml)
{
   cJSON * result=NULL;
   yaml_parser_t  parser;
   yaml_document_t document;
   yaml_parser_initialize(&parser);
   yaml_parser_set_input_string(&parser, (const unsigned char *) yaml, strlen(yaml));

   int done = 0;
   while (!done)
   {
      if (!yaml_parser_load(&parser, &document)) {
         debug( "Failed to load document \n");
         // todo feedback
         break;
      }

      done = (!yaml_document_get_root_node(&document));

      if (!done)
        yaml_document_2_cJSON(&document,&result,env_vars);

      yaml_document_delete(&document);
   }
   yaml_parser_delete(&parser);
   return result;
}

cJSON * yaml_sting_2_cJSON ( __attribute__((unused))void * opaque , const char * yaml)
{
	cJSON * result=NULL;
	yaml_parser_t  parser;
	yaml_document_t document;
	yaml_parser_initialize(&parser);
	yaml_parser_set_input_string(&parser, (const unsigned char *) yaml, strlen(yaml));
	
	int done = 0;
	while (!done)
	{
		if (!yaml_parser_load(&parser, &document)) {
			debug( "Failed to load document \n");
			// todo feedback
			break;
		}
		
		done = (!yaml_document_get_root_node(&document));
		
		if (!done)
			yaml_document_2_cJSON(&document,&result,NULL);
		
		yaml_document_delete(&document);
	}
	yaml_parser_delete(&parser);
	return result;
}

cJSON * yaml_file_2_cJSON ( __attribute__((unused))void * opaque ,const char * fileName, ...)
{
	char *filePath=CSPRINTF(fileName);
	if (!filePath)
	{
		debug("error getting filePath from vargs\n");
		return NULL;
	}
	FILE * file=fopen(filePath, "r");
	oc_free(&filePath); filePath=NULL;
	if (!file)
	{
		debug("error opening %s",filePath);
		return NULL;
	}
	cJSON * result=NULL;
	yaml_parser_t  parser;
	yaml_document_t document;
	yaml_parser_initialize(&parser);
	
	yaml_parser_set_input_file(&parser,file);
	int done = 0;
	while (!done)
	{
		if (!yaml_parser_load(&parser, &document)) {
			debug( "Failed to load document \n");
			// todo feedback
			break;
		}
		
		done = (!yaml_document_get_root_node(&document));
		
		if (!done)
			yaml_document_2_cJSON(&document,&result, NULL);
		
		yaml_document_delete(&document);
	}
	yaml_parser_delete(&parser);
	if (file) fclose(file);
	return result;
}

cJSON * yaml_file_doc0_2_cJSON ( char ** errormsg ,const char * fileName, ...)
{
	char *filePath=CSPRINTF(fileName);
	if (!filePath)
	{
		if (errormsg) asprintf(errormsg, "error getting filePath from vargs\n");
		return NULL;
	}
	FILE * file=fopen(filePath, "r");
	oc_free(&filePath); filePath=NULL;
	if (!file)
	{
		if (errormsg) asprintf(errormsg, "error opening %s",filePath);
		return NULL;
	}
	cJSON * result=NULL;
	yaml_parser_t  parser;
	yaml_document_t document;
	yaml_parser_initialize(&parser);
	
	yaml_parser_set_input_file(&parser,file);
	int done = 0;
	while (!done)
	{
		if (!yaml_parser_load(&parser, &document)) {
			if (errormsg) asprintf(errormsg, "Failed to load document \n");
			// todo feedback
			break;
		}
		
		done = (!yaml_document_get_root_node(&document));
		
		if (!done)
			yaml_document_2_cJSON(&document,&result, NULL);
		
		yaml_document_delete(&document);
	}
	yaml_parser_delete(&parser);
	if (file) fclose(file);
	if (result && cJSON_IsArray(result) && cJSON_GetArraySize(result)>0)
	{
		cJSON * tmp=cJSON_DetachItemFromArray(result, 0);
		cJSON_Delete(result);
		result=tmp;
	}
	return result;
}


cJSON * yaml_file_2_cJSON_env ( __attribute__((unused))void * opaque ,cJSON * env_vars,const char * fileName, ...)
{
	char *filePath=CSPRINTF(fileName);
	if (!filePath)
	{
		debug("error getting filePath from vargs\n");
		return NULL;
	}
	FILE * file=fopen(filePath, "r");
	oc_free(&filePath);
	if (!file)
	{
		debug("error opening %s",filePath);
		return NULL;
	}
	cJSON * result=NULL;
	yaml_parser_t  parser;
	yaml_document_t document;
	yaml_parser_initialize(&parser);
	
	yaml_parser_set_input_file(&parser,file);
	int done = 0;
	while (!done)
	{
		if (!yaml_parser_load(&parser, &document)) {
			debug( "Failed to load document \n");
			// todo feedback
			break;
		}
		
		done = (!yaml_document_get_root_node(&document));
		
		if (!done)
			yaml_document_2_cJSON(&document,&result, env_vars);
		
		yaml_document_delete(&document);
	}
	yaml_parser_delete(&parser);
	if (file) fclose(file);
	return result;
}
