//
//  jose_error.c
//  cjose_cjson
//
//  Created by Danny Goossen on 22/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//
#include <openssl/err.h>
#include "jose_error.h"
/**
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2014-2016 Cisco Systems, Inc.  All Rights Reserved.
 */




////////////////////////////////////////////////////////////////////////////////
static const char *_ERR_MSG_TABLE[] = { "no error", "invalid argument", "invalid state", "out of memory", "crypto error" };

////////////////////////////////////////////////////////////////////////////////
const char *jose_err_message(jose_errcode code)
{
   const char *retval = NULL;
   if (JOSE_ERR_CRYPTO == code)
   {
      // for crypto errors, return the most recent openssl error as message
      long err = ERR_get_error();
      while (0 != err)
      {
         retval = ERR_error_string(err, NULL);
         err = ERR_get_error();
      }
   }
   if (NULL == retval)
   {
      retval = _ERR_MSG_TABLE[code];
   }
   return retval;
}
