/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/*! \Environment.c
 *  \brief Universal Environment manipulation, real env / cJSON struct
 *  Used for e.g. substitude Environment, where depending on the init of environment we use real environment or a cJSON structure.
 *
 *  \author Created by Danny Goossen on  8/8/18.
 *  \copyright 2018, Danny Goossen. MIT License
*/

#include "Environment.h"
#include <stdlib.h>
#include "cJSON_deploy.h"

/**
 \brief Environment: set
 */
int env_set_env(Environment*e, const char * k,const char *v, int o);
/**
 \brief Environment: unset
 */
int env_unset_env(Environment*e,const char * k);
/**
 \brief Environment: set
 */
const char * env_get_env(Environment*e,const char * k);
/**
 \brief Environment: cJSON set
 */
int cJSON_set_env(Environment*e, const char * k,const char *v, int o);
/**
 \brief Environment: cJSON unset
 */
int cJSON_unset_env(Environment*e,const char * k);
/**
 \brief Environment: cJSON get
 */
const char * cJSON_get_env(Environment*e,const char * k);


Environment * Environment_init(void * p,environment_type_t type)
{
	if (type==environment_type_cJSON && !p) return NULL;
	Environment * e=calloc(1, sizeof(Environment));
	e->type=type;
	if (type==environment_type_env)
	{
		e->setenv=env_set_env;
		e->unsetenv=env_unset_env;
		e->getenv=env_get_env;
	}
	if (type==environment_type_cJSON) {
		e->environment=p;
		e->setenv=cJSON_set_env;
		e->unsetenv=cJSON_unset_env;
		e->getenv=cJSON_get_env;
	}
	return e;
}


void Environment_clear(Environment**self)
{
	if (self)
	{
		if (*self)
		{
			free(*self);
			*self=NULL;
		}
	}
}

// wrapper implementations

int env_set_env(__attribute__((unused))Environment*e, const char * k,const char *v, int o)
{
	return setenv(k, v, o);
}

int env_unset_env(__attribute__((unused))Environment*e,const char * k)
{
	return unsetenv(k);
}

const char * env_get_env(__attribute__((unused))Environment*e,const char * k)
{
	return getenv(k);
}

int cJSON_set_env(Environment*e, const char * k,const char *v, int o)
{
	if (o)
		cJSON_add_string((cJSON *)e->environment, k,v);
	else
	{
		cJSON * i=cJSON_GetObjectItem((cJSON *)e->environment, k);
		if (!i) cJSON_add_string((cJSON *)e->environment, k,v);
	}
	return 0;
}

int cJSON_unset_env(Environment*e,const char * k)
{
	cJSON_DeleteItemFromObject((cJSON *)e->environment, k);
	return 0;
}

const char * cJSON_get_env(Environment*e,const char * k)
{
	if (k && strlen(k)>0)
		return cJSON_get_key((cJSON *)e->environment, k);
	else
		return NULL;
}

