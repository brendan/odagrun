/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file do_job.c
 *  \brief the executer of the job
 *  \author Danny Goossen
 *  \date 9/10/17
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#include "deployd.h"
#include "wordexp_var.h"
#include "registry.h"
#include "c-ares.h"
#include "cache.h"
#include "curl_commands.h"
#include "DockerHub.h"
#include "MicroBadgerUpdate.h"
#include "ImageStream.h"
#include "FileCopy.h"
#include "QUAY_commands.h"
#include "Environment.h"

cJSON* read_job_file( const char * dirpath, const char * filename)
{
   cJSON * job_json=NULL;
   if (!dirpath || !filename) return NULL;
   //printf("%s/%s\n",dirpath,filename);
   char filepath[1024];
   snprintf(filepath, 1024,"%s/%s",dirpath,filename);
   FILE *f = fopen(filepath, "r");
   if (f == NULL)
   {
      error("process_job_file: %s\n",strerror(errno));
      return NULL;
   }
   dynbuffer * mem=dynbuffer_init();
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      dynbuffer_write(buf, mem );
      bzero(buf, 1024);
   }
   fclose(f);
   //fprintf(stderr,"readfile: >\n%s\n",mem.memory);
   ;
   if (dynbuffer_len(mem)>1) job_json= cJSON_Parse(dynbuffer_get(mem));
   dynbuffer_clear(&mem);
   // validate json
   if (!job_json)
   {
      error("Failed to get job.yaml %s \n",filepath);
      return NULL;
   }
   else
   {
      alert(" got job\n");
      return job_json;
   }
}

int do_local_shell(cJSON* job,parameter_t * parameters)
{
	char * token=read_a_file_v( "%s/token",parameters->service_dir);
	char *namespace=read_a_file_v("%s/namespace",parameters->service_dir);


	
	// TODO findout registry name in ip-adress otherwise problems!!! (github issue ...... known issue)
	char * ImageStream_domain=NULL;
	
	char cert_file_dest[1024];
	snprintf(cert_file_dest, 1024, "/bin-runner/rootfs%s",DEFAULT_CA_BUNDLE);

	void *trace=NULL;
	cJSON * id_j=cJSON_GetObjectItem(job, "id");
	int id=0;
	if (cJSON_IsNumber(id_j)) id=id_j->valueint;
	alert("runner %2d: Start Job %d (%s)\n",1,id,cJSON_get_key(job, "ci_url"));
	char temp[1024];
	snprintf(temp,1024,"%s/api/v4/jobs/%d",cJSON_get_key(job, "ci_url"),id);
	
	init_dynamic_trace(&trace,cJSON_get_key(job, "token") , temp,id,parameters->ca_bundle);
	
	char * registry_ip=getip("docker-registry.default.svc.cluster.local");
	char * tmp_is=getenv("OKD_REGISTRY_IP");
	if (tmp_is)
	{
		ImageStream_domain=strdup(tmp_is);;
	}
	else if (registry_ip)
	{
		asprintf(&ImageStream_domain,"%s:%d", registry_ip,5000);
	}
	else
	{
		ImageStream_domain=strdup("172.30.1.1:5000");
	}
	if (registry_ip) oc_free(&registry_ip);
	
	int ImageStream_Insecure=0;
	
	int ImageStream_v1Only=0;
	char * is_registry_v1only=getenv("OKD_REGISTRY_V1only");
	if (is_registry_v1only)
	{
		debug("OKD_REGISTRY_V1only=%s\n",is_registry_v1only);
		char * tmp=strdup(is_registry_v1only);
		is_registry_v1only=tmp;
		
		lower_string(is_registry_v1only);
		if(strcmp(is_registry_v1only,"true")==0 || strcmp(is_registry_v1only,"yes")==0 || strcmp(is_registry_v1only,"1")==0)
			ImageStream_v1Only=1;
		oc_free(&is_registry_v1only);
		is_registry_v1only=NULL;
	}
	int ImageStream_v2support=0;
	char * is_registry_v2support=getenv("OKD_REGISTRY_V2support");
	if (is_registry_v2support)
	{
		debug("OKD_REGISTRY_V2support=%s\n",is_registry_v2support);
		char * tmp=strdup(is_registry_v2support);
		is_registry_v2support=tmp;
		
		lower_string(is_registry_v2support);
		if(strcmp(is_registry_v2support,"true")==0 || strcmp(is_registry_v2support,"yes")==0 || strcmp(is_registry_v2support,"1")==0)
			ImageStream_v2support=1;
		oc_free(&is_registry_v2support);
		is_registry_v2support=NULL;
	}
	char * master_url=NULL;
	const char * master_url_env=getenv("OKD_MASTER_URL");
	if (master_url_env)
		master_url=strdup(master_url_env);
	else
		master_url=strdup("https://openshift.default.svc.cluster.local:443");
		
	debug("OKD_REGISTRY_V1only=%d\n",ImageStream_v1Only);
	
	//git_threads_init();
	// check for jobs and execute!!! yah
	
	parameters->shell =check_shell();
	int res=0;
	
	cJSON *env_vars=NULL;
	if(!res)
	{
	 cJSON * var=NULL;
		print_json(cJSON_GetObjectItem(job, "variables"));
	 cJSON_ArrayForEach(var,cJSON_GetObjectItem(job, "variables"))
	 {
			cJSON * value_object=cJSON_GetObjectItem(var, "value");
		 if (strncmp(cJSON_get_key(var, "key"), "CI_BUILD_", 9)!=0 && value_object )
		 { // remove legacy CI_BUILD_xxxx vars
			 if (!env_vars) env_vars=cJSON_CreateObject();
			 if (!cJSON_get_key(env_vars,cJSON_get_key(var, "key")))
				 cJSON_AddItemToObject(env_vars, cJSON_get_key(var, "key"),  cJSON_Duplicate(value_object, 1));
			 else
				 cJSON_ReplaceItemInObject(env_vars,cJSON_get_key(var, "key"),cJSON_Duplicate(value_object, 1));
		 }
	 }
	}
	if (env_vars)
	{
		cJSON_AddItemToObject(job, "env_vars", env_vars);
		
	}
	if(!res)
	{
		Write_dyn_trace_pad(trace, bold_yellow,58, "* Check ImageStream Registry ...");
		int reg_check=dkr_api_check_registry_secure( ImageStream_domain, cert_file_dest,trace);
		if (reg_check==1)
		{
			ImageStream_Insecure=1;
			res=0;
		}
		else if (reg_check==0)
		{
			ImageStream_Insecure=0;
			res=0;
		}
		else
		{
			ImageStream_Insecure=0;
			res=-1;
		}
	}
	if (!res) debug("problem determening secure registry\n");
	cJSON *sys_vars=NULL;
	sys_vars=cJSON_CreateObject();
	if (sys_vars)
	{
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-IP",ImageStream_domain);
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-OC-NAMESPACE",namespace);
		cJSON_safe_addBool2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-Insecure",ImageStream_Insecure);
		cJSON_safe_addBool2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-v1Only",ImageStream_v1Only);
		cJSON_safe_addBool2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-v2Support",ImageStream_v2support);
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-TOKEN",token);
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-DOCKER-REGISTRY",DOCKER_REGISTRY);
		cJSON_safe_addString2Obj(sys_vars,"OC-MASTER-URL",master_url);
		cJSON_safe_addBool2Obj(sys_vars,"OC-LOCAL-SHELL",cJSON_True);
		cJSON_AddItemToObject(job, "sys_vars", sys_vars);
	}
	/*
	cJSON * cache=cJSON_GetObjectItem(env_vars, "cache");
	if ( cache && cJSON_IsString(cache) )
	{
		cJSON * temp=yaml_sting_2_cJSON(NULL,cache->valuestring );
		if (temp && cJSON_IsArray(temp) )
		{
			cache=cJSON_GetArrayItem(temp, 0);
		}
		else
			cache=NULL;
	}
	else cache=NULL;
	*/
	//TODO change id to string
	cJSON_add_string_v(job, "ci_work_dir", "/builds/%d",id);
	cJSON_add_string_v(env_vars, "CI_PROJECT_DIR", "/builds/%d/project_dir",id );
	cJSON_add_string_v(job, "CI_PROJECT_DIR", "/builds/%d/project_dir",id );
	
	int layers_pull=0;
	
	layers_pull=parse_git_cache(NULL, job, 1);
		
	layers_pull=layers_pull+parse_workspaces(NULL,  job,1);
	
	
	
	git_libgit2_init();
	git_libgit2_opts(GIT_OPT_SET_SSL_CERT_LOCATIONS, DEFAULT_CA_BUNDLE, NULL);
	curl_global_init(CURL_GLOBAL_SSL); //TODO move this to main
	git_libgit2_opts(GIT_OPT_ENABLE_OFS_DELTA, 1);

	
	
	do_job(job, parameters);
	
	curl_global_cleanup(); //TODO move this to main
	thread_cleanup(); //TODO move this to main
	git_libgit2_shutdown();
	alert("exit oc-executer\n");

	if (ImageStream_domain) oc_free(&ImageStream_domain);
	if (master_url) oc_free(&master_url);
	return layers_pull;
}
/*
struct command
{
	
};
struct commands
{
	const char command
};

int process_internal(const char * command,cJSON * job,struct trace_Struct *trace)
{
	
	return 0;
}
*/


int do_job(cJSON* job,parameter_t * parameters )
{
   void *trace=NULL;//=&v_trace;
   
   int res=0; // failed or success
	
   cJSON * id_j=cJSON_GetObjectItem(job, "id");
   int id=0;
   if (cJSON_IsNumber(id_j)) id=id_j->valueint;
   alert("runner: Start Job %d (%s)\n",id,cJSON_get_key(job, "ci_url"));
   char temp[1024];
   snprintf(temp,1024,"%s/api/v4/jobs/%d",cJSON_get_key(job, "ci_url"),id);
   
   init_dynamic_trace(&trace,cJSON_get_key(job, "token") , temp,id,parameters->ca_bundle);
   
   if (cJSON_GetObjectItem(job, "dispatcher"))
   {
      alert("Got dispatcher\n");
      Write_dyn_trace(trace,none, cJSON_get_key(job, "dispatcher"));
   }
   else alert("No dispatcher trace in job\n");
   alert("version: %s\n",PACKAGE_VERSION);
   
   char hostname[1024];
   gethostname(hostname, 1024);
  
   
   Write_dyn_trace(trace,white,"oc-executer (%s) running on %s\n",PACKAGE_VERSION, hostname);
   update_details(trace);
	//merge ws env with env vars, in correct order!
	// Meaning ws env can be overwritten by variables in ci yml
	// also no dupplicates !!!
	// need to revise this for auth !!!!
	//
	cJSON *env_vars=cJSON_CreateObject();
	Environment *Env=NULL;
	Env=Environment_init(env_vars, environment_type_cJSON);

	cJSON * ws_env=cJSON_DetachItemFromObject(job, "ws_environment");
	if (ws_env)
	{
		cJSON*item=NULL;
		cJSON_ArrayForEach(item, ws_env)
		{
			const char * vs=item->string;
			if (vs)
			{
				Environment_setenv(Env, vs, item->valuestring,1);
			}
		}
		cJSON_Delete(ws_env);
	}
	cJSON * org_job_env=cJSON_DetachItemFromObject(job, "env_vars");
	if (org_job_env)
	{
		cJSON*item=NULL;
		cJSON_ArrayForEach(item, org_job_env)
		{
			const char * vs=item->string;
			if (vs)
			{
				Environment_setenv(Env, vs, item->valuestring,1);
			}
		}
		cJSON_Delete(org_job_env);
	}
	cJSON_AddItemToObject(job, "env_vars", env_vars);
	cJSON *sys_vars=cJSON_GetObjectItem(job, "sys_vars");
	cJSON * localShell=cJSON_GetObjectItem(sys_vars, "OC-LOCAL-SHELL");
	debug("env_vars:\n");
	print_json(env_vars);
	
	if (localShell && cJSON_IsTrue(localShell))
	{
		cJSON_add_string_v(job, "ci_work_dir", "/builds/%d",id);
		cJSON_add_string_v(env_vars, "CI_PROJECT_DIR", "/builds/%d/project_dir",id );
		cJSON_add_string_v(job, "CI_PROJECT_DIR", "/builds/%d/project_dir",id );
	}
	else
	{
		cJSON_add_string(job, "ci_work_dir", "/builds");
		cJSON_add_string(env_vars, "CI_PROJECT_DIR", "/builds/project_dir" );
		cJSON_add_string(job, "CI_PROJECT_DIR", "/builds/project_dir" );
	}
	
	
	cJSON_add_string(env_vars, "nl", "\n");
	
	const char * CI_PROJECT_DIR=cJSON_get_key(job, "CI_PROJECT_DIR");
	const char * ci_work_dir=cJSON_get_key(job, "ci_work_dir");
	
	vmkdir("%s",CI_PROJECT_DIR);
	chdir(CI_PROJECT_DIR);

	cJSON * u_artifacts= cJSON_GetArrayItem( cJSON_GetObjectItem(job,"artifacts"),0);
   // usleep(1000000);
   // prepare env_vars for deployctl
   // first flatten the env vars
   debug ("job ptr %p",job);
   
   Write_dyn_trace(trace,bold_yellow,"\n* Preparing Environment:\n");
   update_details(trace);
	
	
      // check artiname, so far no processing!!! To dO
	
   debug("start environment vars \n");
	/*
   if ( (0) )
   {
      char * print_it=cJSON_PrintBuffered(env_vars,1,1);
      if (print_it)
      {
         
         alert("env:\n %s\n\n",print_it);
         oc_free(&print_it);
         print_it=NULL;
      }
   }
	*/
	char * oc_service_token=NULL;
	char * oc_namespace=NULL;

   oc_service_token=read_a_file( "/var/run/secrets/kubernetes.io/serviceaccount/token");
   oc_namespace=read_a_file("/var/run/secrets/kubernetes.io/serviceaccount/namespace");
	
	
	cJSON_AddStringToObject(env_vars, "OKD_NAMESPACE", oc_namespace);
	cJSON_AddStringToObject(env_vars, "OKD_SERVICE_TOKEN", oc_service_token);
	
	
   debug("start steps\n");
   cJSON*deploy_cmds=NULL;
   if (res==0)
   {
      //int count=0;
      
      cJSON*steps=cJSON_GetObjectItem(job, "steps");
      cJSON*script=cJSON_GetObjectItem((cJSON_GetArrayItem(steps, 0)),"script");
      cJSON*step=NULL;
      //if (cJSON_GetArraySize(script)>0)
      //   Write_dyn_trace_pad(trace,yellow,60,"  + prepare commands from script:");
      cJSON_ArrayForEach(step,script)
      {
        
            if(!deploy_cmds) deploy_cmds=cJSON_CreateArray();
            cJSON_AddItemToArray(deploy_cmds, cJSON_CreateString(step->valuestring ));
      }
      //if (count==0) Write_dyn_trace(trace, green,"  [OK]\n");
      
      //update_details(trace);
   }

   
	
   
   // fetch git ci_project_url if needed
   if (res==0 )
   {
      if (cJSON_GetObjectItem(env_vars,"GIT_STRATEGY") && validate_key(env_vars,"GIT_STRATEGY","none")==0)
         Write_dyn_trace(trace,bold_yellow,"\n* Skipped git on request\n");
      else
      {
          int remote_objects_update=0;
         res=lgit2_checkout(job, trace,&remote_objects_update);
		 if (!res)
         {
             cache_update_remote_objects(job,remote_objects_update);
			 lgit2_describe(job,trace);
         }
         update_details(trace);
      }
      
   }
	// TODO cp -r iso

	
	cJSON * cache_links=cJSON_GetObjectItem(job, "layers_links");
	if (localShell && cJSON_IsTrue(localShell) && cache_links)
	{
		cJSON * slink=NULL;
		cJSON_ArrayForEach(slink,cache_links )
		{
			
			const char * cache_name=cJSON_get_key(slink, "name");
			// local shell problem path, cache_path can be array of string
			const char * cache_path=cJSON_get_key(slink, "path");
			const char * cache_location=cJSON_get_key(slink, "location");
			if (cache_name && strcmp(cache_name,"git_cache")!=0 &&cache_path)
			{
				
				char * dest=NULL;
				if (strchr(cache_path,'/')==cache_path)
					dest=strdup(cache_path);
				else
				    asprintf(&dest,"%s/%s",CI_PROJECT_DIR,cache_path);
				if (dest)
				{
					vmkdir("%s",dest);
					vmkdir("%s",cache_location);
					char * cp_command=NULL;
					asprintf(&cp_command, "cp -aR %s/ %s/",cache_location,dest);
					int res=system(cp_command);
					if (cp_command) oc_free(&cp_command);
					if (res)
						debug("failed copy cache\n");
					oc_free(&dest);
				}
			}
		}
	}
    
    cache_update_initial_filecount(job,trace,localShell);
    
    
   // cancel??
   if (parameters->hubsignal || parameters->exitsignal)
   {
      if (parameters->hubsignal)
         Write_dyn_trace(trace,bold_red,"\n-- Runner recived Reload signal\n");
      else
         Write_dyn_trace(trace,bold_red,"\n-- Runner recived EXIT signal\n");
      res=1;
      Write_dyn_trace(trace,yellow,"\n Please retry JOB later on, stopping deployment\n");
      update_details(trace);
   }
   
   // fetch artifacts if needed
   if (res==0)
   {
      cJSON * artifacts=prepare_artifacts(job);
      if (artifacts && cJSON_GetArraySize(artifacts)>0)
      {
         char zip_dir[1024];
         snprintf(zip_dir,1024,"%s/.zips",ci_work_dir);
         
         snprintf(temp,1024,"\n* %2d JOB artifacts to     Download   -   Unzip  \n",cJSON_GetArraySize(artifacts));
         Write_dyn_trace(trace,bold_yellow ,temp);
         snprintf(temp,1024,"  ---------------------------------------------\n");
         Write_dyn_trace(trace,bold_yellow ,temp);
         set_mark_dynamic_trace(trace);
         scan_artifacts_status(artifacts,trace);
         update_details(trace);
         
         int count=cJSON_GetArraySize(artifacts);
         res=count;
         int update_error=0;
         int res_prev=0;
         struct dl_artifacts_s dl_artifacts;
         snprintf(temp,1024,"%d",id);
         init_download_artifacts(&dl_artifacts, count, ci_work_dir, zip_dir,parameters->ca_bundle);
         int count_u=0;
         while ( res>0 && !update_error && !parameters->hubsignal && !parameters->exitsignal) // loop till finish artifacts
         {
            int progress_update=0;
            res=download_artifacts(artifacts,&dl_artifacts);
            progress_update=progress_change(&dl_artifacts);
            if ((res!=res_prev) || res <1 || progress_update) // resduce network trafic, only report changes
            {
               if ((count_u%5 ==0) || res <1 || progress_update)
               {
                  scan_artifacts_status(artifacts,trace);
                   
                  update_error= update_details(trace);
                  if (update_error){
                     printf("\n** main update details error, canceling downloads\n");
                     cancel_download_artifacts(&dl_artifacts);
                  }
               }
               count_u ++;
            }
            res_prev=res;
         }
         // cleanup the xip dir
         // TODO add again, temp disable remove for debug
         //printf("Finish, not removing zips, res=%d\n",res);
         
         
         close_download_artifacts(&dl_artifacts);
         vrmdir("%s",  zip_dir);
         if (update_error) res=-1;
      }
      update_details(trace);
      cJSON_Delete(artifacts);
   }
   
   
   if (parameters->hubsignal || parameters->exitsignal)
   {
      if (parameters->hubsignal)
         Write_dyn_trace(trace,bold_red,"\n-- Runner received reload signal\n");
      else
         Write_dyn_trace(trace,bold_red,"\n-- Runner received EXIT signal\n");
      res=1;
      Write_dyn_trace(trace,yellow,"\n Please retry JOB later on, stopping deployment\n");
      update_details(trace);
   }
	
   data_exchange_t * data_exhange= init_data_exchange(parameters, trace,Env);
   if (res==0 && deploy_cmds && cJSON_GetArraySize( deploy_cmds)>0)
   {
      // set environment
      Environment_setenv(Env,"TERM","xterm-256color",1);
      Environment_setenv(Env,"CLICOLOR","1",1);
	  Environment_setenv(Env,"PWD",Environment_getenv(Env, "CI_PROJECT_DIR"),1);
	  Environment_setenv(Env,"HOME",ci_work_dir,1);
	   char * oc_commit_tag_slug=slug_it(Environment_getenv(Env, "CI_COMMIT_TAG"), 0);
	   if (oc_commit_tag_slug)
	   {
		   Environment_setenv(Env, "ODAGRUN_COMMIT_TAG_SLUG", oc_commit_tag_slug, 1);
	   	   oc_free(&oc_commit_tag_slug);
	   }
	   
	  // for expansion variables
	   update_details(trace);
	   Write_dyn_trace(trace, bold_yellow,"\n* Start SCRIPT JOB_NAME: ");
	   if (cJSON_get_key(env_vars, "CI_JOB_NAME"))
		   Write_dyn_trace(trace, cyan,"%s\n",cJSON_get_key(env_vars, "CI_JOB_NAME"));
	   char * reverse_project_path=name_space_rev(Environment_getenv(Env, "CI_PROJECT_PATH"));
			 if (reverse_project_path)
			 {
				 Environment_setenv(Env, "ODAGRUN_REVERSE_PROJECT_PATH", reverse_project_path, 1);
				 oc_free(&reverse_project_path);
			 }
			 reverse_project_path=name_space_rev_slug(Environment_getenv(Env, "CI_PROJECT_PATH"));
			 if (reverse_project_path)
			 {
				 Environment_setenv(Env, "ODAGRUN_REVERSE_PROJECT_PATH_SLUG", reverse_project_path, 1);
				 oc_free(&reverse_project_path);
			 }

         cJSON*step=NULL;
         cJSON_ArrayForEach(step,deploy_cmds)
         {
			 // for expansion of variables
			 
			 struct tm *tm_gmt;
			 time_t t;
			 t = time(NULL);
			 tm_gmt = gmtime(&t);
			 char date_buffer[36];
			 strftime(date_buffer, 36, "%Y-%m-%dT%H:%M:%SZ", tm_gmt);
			 Environment_setenv(Env,"ODAGRUN_RFC3339_DATE",date_buffer,1);
			 strftime(date_buffer, 36, "%Y%m%d", tm_gmt);
			 Environment_setenv(Env,"ODAGRUN_SHORT_DATE",date_buffer,1);
			 
			 
            char  displaydir[PATH_MAX];
            if (strncmp(Environment_getenv(Env, "PWD"),CI_PROJECT_DIR,strlen(CI_PROJECT_DIR))==0)
            {
               snprintf(displaydir, PATH_MAX, "prj%s",Environment_getenv(Env, "PWD")+strlen(CI_PROJECT_DIR));
               Write_dyn_trace(trace,  cyan, "\n[%s]",displaydir);
            }
            else
            {
               snprintf(displaydir, PATH_MAX,"%s",Environment_getenv(Env, "PWD"));
               Write_dyn_trace(trace, magenta, "\n[%s]",displaydir);
            }
            Write_dyn_trace(trace, bold_cyan, "$ ");
            Write_dyn_trace(trace, bold_yellow, "%s\n",step->valuestring);
			 
			 // need to set dir, otherwise filecopy command is not in correct dir!
			 
			chdir(Environment_getenv(Env, "PWD"));
			 
            if (strstr(step->valuestring,"registry_push")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				int exp_res=bootstrap_expander_registry_push(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!exp_res)
				{
					// just to be sure we're in the correct directory
					
					int i=0;
					debug ("Calling %s with:\n",wev.argv[0]);
					for (i=1;i<wev.argc;i++) debug("\t%s\n",wev.argv[i]);
					res=registry_push(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"registry_tag_image")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				int exp_res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!exp_res)
				{
						res=registry_tag_image(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail [FAIL]\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"ImageStream_delete_tag")==step->valuestring || strstr(step->valuestring,"ImageStream_Delete_Tag")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if(!res)
				{
						res=ImageStream_delete_tag(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			 
			else if (strstr(step->valuestring,"ImageStream_delete")==step->valuestring ||
					 strstr(step->valuestring,"ImageStream_Delete")==step->valuestring ||
					 strstr(step->valuestring,"ImageStream_Delete_Image")==step->valuestring ||
					 strstr(step->valuestring,"ImageStream_delete_image")==step->valuestring
					 )
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
					res=ImageStream_delete(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"DockerHub_delete_tag")==step->valuestring || strstr(step->valuestring,"DockerHub_Delete_Tag")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
						res=DockerHub_delete_tag(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"DockerHub_delete_repository")==step->valuestring || strstr(step->valuestring,"DockerHub_Delete_Repository")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
				
					res=DockerHub_delete_repository(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"DockerHub_set_description")==step->valuestring || strstr(step->valuestring,"DockerHub_Set_Description")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
					
					res=DockerHub_set_description(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			 
			else if (strstr(step->valuestring,"QUAY_delete_tag")==step->valuestring || strstr(step->valuestring,"QUAY_delete_tag")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
					res=QUAY_command_delete_tag(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"QUAY_delete_repository")==step->valuestring || strstr(step->valuestring,"QUAY_Delete_Repository")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
					
					res=QUAY_command_delete_repository(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"QUAY_set_description")==step->valuestring || strstr(step->valuestring,"QUAY_Set_Description")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!res)
				{
					
					res=QUAY_command_set_description(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			 else if (strstr(step->valuestring,"MicroBadger_Update")==step->valuestring || strstr(step->valuestring,"MicroBadger_update")==step->valuestring)
			 {
				 word_exp_var_t wev;
				 memset(&wev, 0, sizeof(word_exp_var_t));
				 //strip posible multiline \n
				 while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				 res=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				 if (!res)
				 {
					 
					 res=microbadgerupdate(job,trace, wev.argc,wev.argv);
				 }
				 else
				 {
					 Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					 Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					 res=-1;
					 
				 }
				 word_exp_var_free(&wev);
			 }
			else if (strstr(step->valuestring,"curl_post")==step->valuestring || strstr(step->valuestring,"PostHook")==step->valuestring)
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				int resw=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!resw)
				{
						res=curl_post(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			else if (strstr(step->valuestring,"copy")==step->valuestring )
			{
				word_exp_var_t wev;
				memset(&wev, 0, sizeof(word_exp_var_t));
				//strip posible multiline \n
				while (strlen(step->valuestring)>1 && step->valuestring[strlen(step->valuestring)-1]=='\n') step->valuestring[strlen(step->valuestring)-1]='\0';
				int resw=word_exp_var_dyn(&wev, step->valuestring, env_vars, Environment_getenv(Env, "PWD"));
				if (!resw)
				{
					res=FileCopy(job,trace, wev.argc,wev.argv);
				}
				else
				{
					Write_dyn_trace(trace, red, "Word Expression Fail!\n");
					Write_dyn_trace(trace, none, "%s\n",wev.errmsg);
					res=-1;
					
				}
				word_exp_var_free(&wev);
			}
			else
            {
               // and execute
               data_exhange->shellcommand=step->valuestring;
               if (!res) res= exec_color(data_exhange);
            }
            update_details(trace);
            if (res) break;
         }
         Write_dyn_trace(trace, bold_yellow,"\n<<<< SCRIPT: ");
         if (!res)
            Write_dyn_trace(trace, green,"SUCCESS\n");
         else
            Write_dyn_trace(trace, red,"FAILED\n");
   }
   else if (res==0)
   {
      Write_dyn_trace(trace, bold_yellow,"\n* Script: ");
      Write_dyn_trace(trace, bold_magenta,"   *** No deployctl commands ***\n");
      update_details(trace);
   }
   update_details(trace);
   // and clean it up
   if (deploy_cmds) cJSON_Delete( deploy_cmds);
	if(Env) Environment_clear(&Env);
   //setdebug();
   debug("starting artifacts\n");
   
   int disable_upload=0;
   if ( !disable_upload &&
       ( // job failed and always or on failure, make artifacts
        (res && (validate_key(u_artifacts,"when","always")==0  || validate_key(u_artifacts,"when","on_failure")==0) )
        || // or job succeeds an artifacts NOT on failure
        (!res && validate_key(u_artifacts,"when","on_failure")!=0)
        )
       && // and
       ( // path OR/AND untracked => artifacts UpLoad
        (cJSON_GetObjectItem(u_artifacts, "paths")     && cJSON_GetObjectItem(u_artifacts, "paths")->type    !=cJSON_NULL ) || \
        (cJSON_GetObjectItem(u_artifacts, "untracked") && cJSON_IsTrue(cJSON_GetObjectItem(u_artifacts, "untracked" )) )
        )
       )
   {
		int result=0;
		char upload_zip_name[1024];
		snprintf(upload_zip_name,1024,"%s/artifacts.zip",ci_work_dir);

		debug("Upload artifacts: untacked: %s\n",cJSON_get_key(u_artifacts,"untracked") );
		set_mark_dynamic_trace(trace);
		Write_dyn_trace(trace,bold_yellow,"\n* Artifacts:\n");

		cJSON * filelist=NULL;
		int untracked_n=0;
		int path_files_n=0;
		if (cJSON_GetObjectItem(u_artifacts, "untracked") &&cJSON_IsTrue(cJSON_GetObjectItem(u_artifacts, "untracked")))
		{
			 // OK
			 Write_dyn_trace(trace,none,"  + Finding untracked files ....");
			 update_details(trace);
			 result=untracked(CI_PROJECT_DIR,&filelist);
			 if (!result && filelist) untracked_n=cJSON_GetArraySize(filelist);
			 Write_dyn_trace(trace,none," Found %d file(s)\n",untracked_n);
			 update_details(trace);
		}
		else
			debug("untracked: false !!\n");
		debug("start path filelist\n");
		if (cJSON_GetObjectItem(u_artifacts, "paths")     && cJSON_GetObjectItem(u_artifacts, "paths")->type    !=cJSON_NULL)
		{
			Write_dyn_trace(trace,none,"  + Finding files from paths ...");
			update_details(trace);
			// got a list of paths to explore and add to the filelist
			result= result+getfilelist(CI_PROJECT_DIR,cJSON_GetObjectItem(u_artifacts, "paths"),&filelist);
			if (!result && filelist)  path_files_n=cJSON_GetArraySize(filelist)-untracked_n;


			Write_dyn_trace(trace,none," Found %d file(s)\n",path_files_n);
			update_details(trace);
		}
		debug("print file-list\n");
		//get_artifact_auth(job);

		if (!result && filelist)
		{
		  if (filelist)
		  {
			 /*
			  alert ("artifacts files:\n");
			  Write_dyn_trace(trace, cyan,"  + artifact files:\n");
				 char * print_it= cJSON_Print(filelist);
				 if (print_it)
				 {
					Write_dyn_trace(trace, cyan,"%s\n",print_it);
					oc_free(&print_it);
				 }
			  */
			   /*
			  cJSON_ArrayForEach(file,filelist)
			  {
			  Write_dyn_trace(trace, cyan,"    - %s\n",file->valuestring);
			  alert(" - %s\n",file->valuestring);
			  }
				*/
				// zip_it
				debug("zip %d files to %s\n",cJSON_GetArraySize(filelist),upload_zip_name);
				result=list_zip_it(upload_zip_name,CI_PROJECT_DIR,filelist);
				debug("list_zip return\n");
				if (!res) res=result;
			}
		}
		if (!result)
		{
			debug("uploading\n");
			Write_dyn_trace_pad(trace,none,75,"  + zipped and Uploading ");
			result=upload_artifact(job,upload_zip_name,parameters->ca_bundle);
			if (!res) res=result;
		}
		if (!result)
		{
			Write_dyn_trace(trace,green," [OK]\n");
		}
		else
		{
			Write_dyn_trace(trace,red," [FAIL]\n");
		}
		update_details(trace);
		if (filelist) cJSON_Delete(filelist);
		filelist=NULL;
	}
	else
	{
		debug("artifacts SKIPPED\n");
	}
	debug("start clean work_spaces\n");
	
	// clean cache if needed
	if (!res) cache_do_clean(job,trace,localShell);
	
	// check and push git-cache and workspaces if needed
	chdir(CI_PROJECT_DIR);
	if (!res) res=cache_do_push(job,trace,localShell);
	
	if (localShell)
	{
		Write_dyn_trace(trace,bold_yellow,"\n* Clean_up\n");
		update_details(trace);

		vrmdir("%s", ci_work_dir);
	}
	debug("workdir=%s\n",ci_work_dir);

	// return result to gitlab
	if (res==0)
	{
	  
	  Write_dyn_trace(trace, bold_green,"\n\t+++ Job Succeeded +++\n");
	  set_dyn_trace_state(trace, "success");
	}
	else
	{
	  Write_dyn_trace(trace,bold_red,"\n\t--- Job FAILED. ---\n");
	  set_dyn_trace_state(trace, "failed");
	}
	int fail_count=0;
	int cancel=0;
	while ((cancel=update_details(trace))>0 && fail_count<3) {fail_count++; usleep(100000* fail_count);}
	if (cancel==-1)
		alert("runner: job %d :canceled by user\n",id);
	else
	{
	  if (res==0)
		 alert("runner: job %d : SUCCESS\n",id);
	  else
		 alert("runner:job %d :FAILED\n",id);
	}
	if (job) cJSON_Delete(job);
	if (trace) free_dynamic_trace(&trace);
	// DONE
return 0;
}
