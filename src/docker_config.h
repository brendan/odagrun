//
//  docker_config.h
//  odagrun
//
//  Created by Danny Goossen on 26/6/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __odagrun__docker_config__
#define __odagrun__docker_config__

#include <stdio.h>

/**
 \brief converts a raw config from a processed yalm to a docker config
 
 \param config cJSON input object
 \return cJSON processed object, NULL on errors or no data
 \note client is responsible for freeing return object
 */
cJSON * process_docker_config(const cJSON * config);

/**
 \brief Merges the (v2) config:config of new into old
 preconditions, old is NULL or correctly formated
 and New is correctly formated with process_docker_config()
 
 */
int docker_merge_config(cJSON ** old, const cJSON * new);

/**
 \brief returns a cJSON docker config object with defaults
 */
cJSON * docker_config_get_default_config(void);

#endif /* defined(__odagrun__docker_config__) */
