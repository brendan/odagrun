//
//  cache.h
//  odagrun
//
//  Created by Danny Goossen on 27/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __odagrun__cache__
#define __odagrun__cache__

#include <stdio.h>
#include "wordexp_var.h"
#include "dkr_api.h"

int parse_git_cache( struct dkr_api_data_s * apidata, cJSON * job,int local_shell);
int parse_workspaces( struct dkr_api_data_s * apidata, cJSON * job,int local_shell);

void cache_update_initial_filecount(cJSON * job,struct trace_Struct * trace,cJSON * localShell);
void cache_update_remote_objects(cJSON*job,int remote_objects_update);

int append_image_2_layer_data( void * apidata,const char* image, cJSON ** layer_data ,int * SchemaVersion,const char * display_name);

int produce_config(cJSON*layerdata,cJSON ** config);
int produce_manifest_V2(cJSON*layerdata,cJSON ** manifest,char *config_digest,int config_size);
int produce_manifest_V1(cJSON*layerdata,cJSON ** manifest,char * name,char*tag);

int calc_thresshold_files(const cJSON * threshold_path_obj, cJSON* job,char ** errormsg);

int cache_do_push(cJSON * job,struct trace_Struct * trace, cJSON* localShell);

void cache_do_clean(cJSON * job, struct trace_Struct *trace,cJSON * localShell);


#endif /* defined(__odagrun__cache__) */
