/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
/*! \file wordexp_var.c
 *  \briefexpand safely variables and commandlines
 *  \author Created by Danny Goossen on 23/11/17.
 *  \copyright 2018, Danny Goossen. MIT License
 *
 */

#include "deployd.h"
#include "wordexp_var.h"


/**
 * \brief helper function to make socket non blocking
 * \param sock the socket
 * \return -1 on failure,
 * \note on failure, caller is responsible to close socket
 */
static int socket_nonblocking (int sock);

int word_exp_var(char * output_buf,int buff_len, const char* valuestring ,const cJSON * env_vars)
{

	//alert("word_exp: start substitute %s\n",valuestring);
	// feedback buffer
	int v_exit_code=0;
	int * exitcode=&v_exit_code;

	pid_t child_pid;

	int aStdoutPipe[2];
	if (pipe(aStdoutPipe) < 0) {sock_error("allocating pipe for child stdout redirect");snprintf(output_buf,buff_len,"System error\n");return(15);}
	// Fork_it
	child_pid = fork();

	if(child_pid == 0)
	{
		//close uneeded read pipeend
		close(aStdoutPipe[PIPE_READ]);

		// set environment
		if (env_vars && cJSON_GetArraySize(env_vars)>0)
		{
			cJSON * pos=NULL;
			cJSON_ArrayForEach(pos, env_vars)
			{
				if (cJSON_IsString(pos))
				{
					setenv(pos->string,pos->valuestring,1);
					//fprintf(stderr,"add env var: %s\n",pos->valuestring);
				}
			}
		}
		// expand it
		wordexp_t word_re;


			// errors on undef and commands
			int res=0;
			switch (res=wordexp(  valuestring, &word_re, WRDE_NOCMD|WRDE_SHOWERR))
			{
				case WRDE_NOSPACE:
					/* If the error was WRDE_NOSPACE,
					 then perhaps part of the result was allocated. */
					wordfree (&word_re);
					break;
				default: /* Some other error. */
					break;
			}
			if (res==0)
			{
				// write back the result
				int i=0;
				for (i=0;i<word_re.we_wordc;i++)
				{
					write( aStdoutPipe[PIPE_WRITE],word_re.we_wordv[i],strlen(word_re.we_wordv[i]));
					if (i+1!=word_re.we_wordc) write( aStdoutPipe[PIPE_WRITE]," ",1);
				}
				//cleanup
				wordfree(&word_re);
			}
		//signal linux end, linux does not get signal on close
		shutdown(aStdoutPipe[PIPE_WRITE], SHUT_WR);
		close(aStdoutPipe[PIPE_WRITE]);
		//exit, job done succesfull
		exit(res);
	}
	else if (child_pid>0) // The Parent
	{
		// parent wait's and return result
		// close unused file descriptors, these are for child only
		close(aStdoutPipe[PIPE_WRITE]);
		usleep(100); // wait a bit to get things settled
		fd_set rds;
		int sfd=aStdoutPipe[PIPE_READ];
		int s_ret=0;
		int s_err=0;
		size_t o_read=0;
		int o_r_error=0;
		struct timeval tv;
		tv.tv_sec = 10;
		tv.tv_usec = 0;
		int timeout=0;
		memset(output_buf,0,buff_len); // clear buffer!!!
		socket_nonblocking(aStdoutPipe[PIPE_READ]);
		do
		{
			FD_ZERO( &rds );
			FD_SET( aStdoutPipe[PIPE_READ], &rds );
			tv.tv_sec = 10;
			s_ret=select(sfd+1, &rds, NULL, NULL, &tv);
			if (s_ret==SOCKET_ERROR) s_err=errno;
			else if (s_ret>0)
			{
				size_t len=strlen(output_buf);
				size_t read_now= buff_len-len-1;
				if (read_now<1)
				{
					error("bash_it truncating output\n");
					char trunck[1024];
					o_read=read(aStdoutPipe[PIPE_READ], trunck, 1024);
				}
				else
				{
					char * to_buf=output_buf+len;
					o_read=read(aStdoutPipe[PIPE_READ], to_buf, read_now);
				}
				if (o_read==0) break;
				else if (o_read >0);
				else if ((o_r_error=errno)!=EINTR && o_r_error!=EAGAIN) { sock_error_no("std_out pipe read", o_r_error);break; }
			}
			else
			{
				snprintf((output_buf),buff_len,"System Error: timeout substituding commands!!!");
				*exitcode=1;
				timeout=1;
				break;
			}
		} while (s_ret!=SOCKET_ERROR || ( s_ret==SOCKET_ERROR && (s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK)));
		close(aStdoutPipe[PIPE_READ]);
		usleep(100);
		int status;
		if (timeout)
		{
			debug("Timeout, kill child\n");
			kill(child_pid,SIGKILL); // TODO need to check with WHOANG
		}
		pid_t w=0;
		int this_error=0;
		while ((w= waitpid(child_pid,&status, 0)) ==-1 && (this_error=errno)==EINTR )
		{
			debug("bash_it: interupted Syscall\n");
		}
		if (w==-1) {
			sock_error_no("waitpid",this_error);
			if (!*exitcode) snprintf((output_buf),buff_len,"System error: error on 'waitpid()': %s!!!",strerror(this_error));
			*exitcode=1;
		}
		else if (WIFEXITED(status))
		{
			*exitcode=WEXITSTATUS(status);
			if (*exitcode)
				info("exit cmd: bash_it w error: %d\n", *exitcode);
			else
			{
				//info("[OK]\n");
				//sprintf(output_buf+strlen(output_buf),"\t[OK]\n");
				;
			}
		}
		else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
			alert("command stopped/killed by signal %d : %s", WTERMSIG(status),strsignal(WSTOPSIG(status)));
			*exitcode=1;
			if (!timeout)
				snprintf((output_buf),buff_len,"System Error: Bash stopped/killed by signal %d : %s!!!", WTERMSIG(status),strsignal(WSTOPSIG(status)));
		}
		else
		{    /* Non-standard case -- may never happen */
			error("Unexpected status child (0x%x)\n", status);*exitcode=status;}
	}
	else
	{
		error("Fork failed \n"); snprintf((output_buf),buff_len,"System Error: FORK failed!!!");*exitcode=1;}
	return (*exitcode);
}



void word_exp_var_free(word_exp_var_t * wev)
{
	if (wev->argv) free((char**)wev->argv);
	wev->argv=NULL;
	if(wev->args_obj) cJSON_Delete(wev->args_obj);
	wev->args_obj=NULL;
	if(wev->errmsg) free(wev->errmsg);
	wev->errmsg=NULL;
	wev->argc=0;
}


int word_exp_var_dyn(word_exp_var_t * wev, const char* valuestring ,const cJSON * env_vars,const char * dir)
{

	//alert("word_exp: start substitute %s\n",valuestring);
	// feedback buffer
	int v_exit_code=0;
	int * exitcode=&v_exit_code;

	pid_t child_pid;

	int aStdoutPipe[2];
	if (pipe(aStdoutPipe) < 0)
	{
		sock_error("allocating pipe for child stdout redirect");

		asprintf(&wev->errmsg,"System error\n");
		return(15);
	}
	// Fork_it
	child_pid = fork();

	if(child_pid == 0)
	{
		//close uneeded read pipeend
		close(aStdoutPipe[PIPE_READ]);
		dup2(aStdoutPipe[PIPE_WRITE], STDERR_FILENO);
		// set environment
		if (env_vars && cJSON_GetArraySize(env_vars)>0)
		{
			cJSON * pos=NULL;
			cJSON_ArrayForEach(pos, env_vars)
			{
				if (cJSON_IsString(pos))
				{
					setenv(pos->string,pos->valuestring,1);
				}
			}
		}
		//const char * project_dir=cJSON_get_key(env_vars, "CI_PROJECT_DIR");
		//int samedir=0;
		//if (dir && project_dir && strcmp(project_dir,dir)==0) samedir=1;
		// expand it
		wordexp_t word_re;
		// TODO need safe_chdir, stat first!!!
		if (dir && strlen(dir)>0) chdir(dir);

		// errors on undef and commands
		int res=0;
		switch (res=wordexp(  valuestring, &word_re, WRDE_NOCMD))
		{
			case 0:
			{
				// write back the result
				cJSON * cjsonStringArray=cJSON_CreateStringArray((const char**)word_re.we_wordv,(int) word_re.we_wordc);
				if (cjsonStringArray)
				{
					/*
					cJSON * item_s=NULL;
				    // loop results and prepend path if needed

					cJSON_ArrayForEach(item_s, cjsonStringArray)
					{
						if (!(strchr(item_s->valuestring,'/')==word_re.we_wordv[i] || samedir || !project_dir))
						{
							char *tmp=item_s->valuestring;
							asprintf(&item_s->valuestring, "%s/%s",project_dir,tmp);
							oc_free(&tmp);
						}
					}
					 */
					// serialize
					char * result=cJSON_Print(cjsonStringArray);
					size_t size_total=strlen(result);
					size_t size_total_writen=0;
					size_t size_towrite=0;
					ssize_t size_writen=1;
					while (size_total_writen<size_total && size_writen>0)
					{
						size_towrite=size_total-size_total_writen;
						size_writen=write( aStdoutPipe[PIPE_WRITE],&result[size_total_writen],size_towrite);
						if (size_writen>=0) size_total_writen+=size_writen;
					}
					if (size_writen<0) res=-1;
					cJSON_Delete(cjsonStringArray);
					cjsonStringArray=NULL;

					oc_free(&result);
					result=NULL;
				}
				else fprintf(stderr, "No string array\n");
				//cleanup
				wordfree(&word_re);

			}
				break;
			case WRDE_BADVAL:
				fprintf(stderr,"An undefined shell variable was referenced.\n");
				break;
			case WRDE_BADCHAR:
				fprintf(stderr,"Illegal occurrence of newline or one of |, &, ;, <, >, (, ),{, }.\n");
				break;
			case WRDE_SYNTAX:
				fprintf(stderr,"Syntax error, such as unbalanced parentheses or unmatched quotes.\n");
				break;
			case WRDE_CMDSUB:
				fprintf(stderr,"Command substitution like `cmd` and $(cmd) are not allowed.\n");
				break;
			case WRDE_NOSPACE:
				/* If the error was WRDE_NOSPACE,
				 then perhaps part of the result was allocated. */
				fprintf(stderr,"Out of Memory.\n");
				wordfree (&word_re);
				break;
			default: /* Some other error. */
				break;
		}
		//signal linux end, linux does not get signal on close
		shutdown(aStdoutPipe[PIPE_WRITE], SHUT_WR);
		close(aStdoutPipe[PIPE_WRITE]);
		//exit, job done succesfull
		exit(res);
	}
	else if (child_pid>0) // The Parent
	{
		// parent wait's and return result
		// close unused file descriptors, these are for child only
		close(aStdoutPipe[PIPE_WRITE]);
		usleep(100); // wait a bit to get things settled
		fd_set rds;
		int sfd=aStdoutPipe[PIPE_READ];
		int s_ret=0;
		int s_err=0;
		size_t o_read=0;
		int o_r_error=0;
		struct timeval tv;
		tv.tv_sec = 10;
		tv.tv_usec = 0;
		int timeout=0;
		dynbuffer * dyn_buff=dynbuffer_init();
		socket_nonblocking(aStdoutPipe[PIPE_READ]);
		do
		{
			char * read_buffer[1024];
			memset(read_buffer, 0, 1024);
			FD_ZERO( &rds );
			FD_SET( aStdoutPipe[PIPE_READ], &rds );
			tv.tv_sec = 10;
			s_ret=select(sfd+1, &rds, NULL, NULL, &tv);
			if (s_ret==SOCKET_ERROR) s_err=errno;
			else if (s_ret>0)
			{


				o_read=read(aStdoutPipe[PIPE_READ], read_buffer,1024);
				if (o_read==0) break;
				else if (o_read >0) dynbuffer_write_n(read_buffer, o_read, dyn_buff);
				else if ((o_r_error=errno)!=EINTR && o_r_error!=EAGAIN) { sock_error_no("std_out pipe read", o_r_error);break; }
			}
			else
			{
				dynbuffer_write_v(dyn_buff,"System Error: timeout substituding commands!!!");
				*exitcode=1;
				timeout=1;
				break;
			}
		} while (s_ret!=SOCKET_ERROR || ( s_ret==SOCKET_ERROR && (s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK)));
		close(aStdoutPipe[PIPE_READ]);
		usleep(100);
		int status;
		if (timeout)
		{
			debug("Timeout, kill child\n");
			kill(child_pid,SIGKILL); // TODO need to check with WHOANG
		}
		pid_t w=0;
		int this_error=0;
		while ((w= waitpid(child_pid,&status, 0)) ==-1 && (this_error=errno)==EINTR )
		{
			debug("bash_it: interupted Syscall\n");
		}
		if (w==-1) {
			sock_error_no("waitpid",this_error);
			if (!*exitcode) dynbuffer_write_v(dyn_buff,"System error: error on 'waitpid()': %s!!!",strerror(this_error));
			*exitcode=1;
		}
		else if (WIFEXITED(status))
		{
			*exitcode=WEXITSTATUS(status);
			if (*exitcode)
				info("exit cmd: wordexp w error: %d\n", *exitcode);
			else
			{
				//info("[OK]\n");
				//sprintf(output_buf+strlen(output_buf),"\t[OK]\n");
				;
			}
		}
		else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
			alert("command stopped/killed by signal %d : %s", WTERMSIG(status),strsignal(WSTOPSIG(status)));
			*exitcode=1;
			if (!timeout)
				dynbuffer_write_v(dyn_buff,"System Error: wordexp stopped/killed by signal %d : %s!!!", WTERMSIG(status),strsignal(WSTOPSIG(status)));
		}
		else
		{    /* Non-standard case -- may never happen */
			error("Unexpected status child (0x%x)\n", status);*exitcode=status;
		}
		if (*exitcode)
			dynbuffer_pop(&dyn_buff, &wev->errmsg, NULL);
		else
		{
			char * tmp=NULL;
			dynbuffer_pop(&dyn_buff, &tmp, NULL);
			wev->args_obj=cJSON_Parse(tmp);
			if (wev->args_obj)
			{
				wev->argc=cJSON_GetArraySize(wev->args_obj);
				wev->argv=calloc(wev->argc+1,sizeof(char*const)); // end with null pointer, just in case we need!
				char**walk=(char**)wev->argv;
				cJSON*item=NULL;
				cJSON_ArrayForEach (item,wev->args_obj)
				{
					*walk=item->valuestring;
					walk++;
				}
			}
		}
		dynbuffer_clear(&dyn_buff);
	}
	else
	{
		error("Fork failed \n");
		asprintf(&wev->errmsg,"System Error: FORK failed!!!");
		*exitcode=1;
	}

	return (*exitcode);
}


static int socket_nonblocking (int sock)
{
	int flags;

	if(sock != INVALID_SOCKET)
	{
		flags = fcntl (sock, F_GETFL, 0);
		return fcntl (sock, F_SETFL, flags | O_NONBLOCK | O_CLOEXEC | SO_KEEPALIVE);//| O_NDELAY);
	}
	return -1;

}
