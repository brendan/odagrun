/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

/*! @file utils.c
 *  @brief extra / modified c-functions
 *  @author danny@gioxa.com
 *  @date 10/5/17
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

 #include "common.h"

// stdio.h with _GNU_SOURCE already defines _XOPEN_SOURCE 500
//#define _XOPEN_SOURCE 500
#include <ftw.h>
typedef unsigned short int u_short;
#include <fts.h>

#include "deployd.h"

#ifndef OPEN_MAX
#define OPEN_MAX 1023
#endif

#include "wordexp_var.h"


/*------------------------------------------------------------------------
 * Convert string to uppercase
 *------------------------------------------------------------------------*/
void upper_string(char s[]) {
   if (!s) return;
   int c = 0;

   while (s[c] != '\0') {
      if (s[c] >= 'a' && s[c] <= 'z') {
         s[c] = s[c] - 32;
      }
      c++;
   }
}
/*------------------------------------------------------------------------
 * Convert string to lower case
 *------------------------------------------------------------------------*/
void lower_string(char s[]) {
   if (!s) return;
   int c = 0;

   while (s[c] != '\0') {
      if (s[c] >= 'A' && s[c] <= 'Z') {
         s[c] = s[c] + 32;
      }
      c++;
   }
}

/*------------------------------------------------------------------------
 * Convert non null terminated string to uppercase
 *------------------------------------------------------------------------*/

void upper_string_n(char s[],size_t n) {
   if (!s) return;
   int c = 0;

   while (s[c] != '\0' && c<n) {
      if (s[c] >= 'a' && s[c] <= 'z') {
         s[c] = s[c] - 32;
      }
      c++;
   }
}


void split_path_file_2_path(char** p, const char *pf) {
    if (!pf || strlen(pf)==0) return;
   const char *slash = pf;
   char *next;
   while ((next = strpbrk(slash + 1, "\\/"))) slash = next;
   if (pf != slash) slash++;
   *p =cJSON_strdup_n((const unsigned char *)pf, slash - pf);
}


void * memrchr(const void *s,int c, size_t n)
{
   const unsigned char *cp;

   if (n != 0) {
      cp = (unsigned char *)s + n;
      do {
         if (*(--cp) == (unsigned char)c)
            return (void *)cp;
      } while (--n != 0);
   }
   return (void *)0;
}


char * normalizePath(const char* pwd, const char * src, char* res) {
   size_t res_len;
   size_t src_len = strlen(src);

   const char * ptr = src;
   const char * end = &src[src_len];
   const char * next;

   if (src_len == 0 || src[0] != '/') {
      // relative path
      size_t pwd_len;

      pwd_len = strlen(pwd);
      memcpy(res, pwd, pwd_len);
      res_len = pwd_len;
   } else {
      res_len = 0;
   }
   for (ptr = src; ptr < end; ptr=next+1) {
      size_t len;
      next = (char*)memchr(ptr, '/', end-ptr);
      if (next == NULL) {
         next = end;
      }
      len = next-ptr;
      switch(len) {
         case 2:
            if (ptr[0] == '.' && ptr[1] == '.') {
               const char * slash = (char*)memrchr(res, '/', res_len);
               if (slash != NULL) {
                  res_len = slash - res;
               }
               continue;
            }
            break;
         case 1:
            if (ptr[0] == '.') {
               continue;
            }
            break;
         case 0:
            continue;
      }

      if (res_len != 1)
         res[res_len++] = '/';

      memcpy(&res[res_len], ptr, len);
      res_len += len;
   }

   if (res_len == 0) {
      res[res_len++] = '/';
   }
   res[res_len] = '\0';
   return res;
}

/**
 \brief nftw() extended with payload \a data for callback function
 The function nftw_x() is the same as nftw(), except that it has one additional argument, \a data as payload for the fn()
 \see https://linux.die.net/man/3/nftw for the base function nftw()
 */
static int
nftw_x(const char *path, int (*fn)(const char *, const struct stat *, int,
                                 struct FTW *,void * data), int nfds, int ftwflags,void * data)
{
   const char *paths[2];
   struct FTW ftw;
   FTSENT *cur;
   FTS *ftsp;
   int ftsflags, fnflag, error, postorder, sverrno;

   /* XXX - nfds is currently unused */
   if (nfds < 1 || nfds > OPEN_MAX) {
      errno = EINVAL;
      return (-1);
   }

   ftsflags = FTS_COMFOLLOW;
   if (!(ftwflags & FTW_CHDIR))
      ftsflags |= FTS_NOCHDIR;
   if (ftwflags & FTW_MOUNT)
      ftsflags |= FTS_XDEV;
   if (ftwflags & FTW_PHYS)
      ftsflags |= FTS_PHYSICAL;
   postorder = (ftwflags & FTW_DEPTH) != 0;
   paths[0] = path;
   paths[1] = NULL;
   ftsp = fts_open((char * const *)paths, ftsflags, NULL);
   if (ftsp == NULL)
      return (-1);
   error = 0;
   while ((cur = fts_read(ftsp)) != NULL) {
      switch (cur->fts_info) {
         case FTS_D:
            if (postorder)
               continue;
            fnflag = FTW_D;
            break;
         case FTS_DNR:
            fnflag = FTW_DNR;
            break;
         case FTS_DP:
            if (!postorder)
               continue;
            fnflag = FTW_DP;
            break;
         case FTS_F:
         case FTS_DEFAULT:
            fnflag = FTW_F;
            break;
         case FTS_NS:
         case FTS_NSOK:
            fnflag = FTW_NS;
            break;
         case FTS_SL:
            fnflag = FTW_SL;
            break;
         case FTS_SLNONE:
            fnflag = FTW_SLN;
            break;
         case FTS_DC:
            errno = ELOOP;
            /* FALLTHROUGH */
         default:
            error = -1;
            goto done;
      }
      ftw.base = cur->fts_pathlen - cur->fts_namelen;
      ftw.level = cur->fts_level;
      error = fn(cur->fts_path, cur->fts_statp, fnflag, &ftw,data);
      if (error != 0)
         break;
   }
done:
   sverrno = errno;
   (void) fts_close(ftsp);
   errno = sverrno;
   return (error);
}

/**
 \brief callback funtion for nftw() rm -r
 */
static int remove_it(const char *path, __attribute__((unused)) const struct stat *s, int flag, __attribute__((unused)) struct FTW *f)
{
   int status=0;
   int (*rm_func)( const char * );

   switch( flag ) {
      default:     rm_func = unlink; break;
      case FTW_DP: rm_func = rmdir;
   }
  status = (rm_func( path ), status != 0 );
    //  perror( path );
   return status;
}

/**
 \brief wrapper for nftw_x payload used for getfilelist_item()
 */
struct payload{
   cJSON ** filelist;
};

/**
 \brief add path to filelist with attributes from \p mode
 \param path to add to list
 \param mode attributes to add
 \return 0 on success
 */
static int add_to_file_list(const char *path,int mode,cJSON ** filelist)
{
   int result=0;
   size_t len=strlen(path);
   if ((S_ISDIR(mode) && len>2 && strcmp(path+len-2, ".")!=0 && len >3 && strcmp(path+len-3, "..")!=0 )|| !S_ISDIR(mode))
   {
      if (!*filelist) *filelist=cJSON_CreateArray();
      if (*filelist)
      {
         cJSON * tmpo=cJSON_CreateObject();
         cJSON_AddStringToObject(tmpo, "path", path);
         cJSON_AddNumberToObject(tmpo, "mode", mode);
         cJSON_AddItemToArray(*filelist,tmpo);
      }
      else
         result=1;
   }
   return result;
}

/**
 * \brief calback function for the nftw_x for getfilelist_item()
 */
static int list_it(const char *path, const struct stat *s, int flag, __attribute__((unused)) struct FTW *f,void* data)
{
   int status=0;
   switch( flag ) {
      default:     ; break;
         /* we'll handle dir/symlinks/files */
      case FTW_SL:
      case FTW_F:
      case FTW_D:
         status=add_to_file_list( path,s->st_mode ,data) ;break;
   }
   return status;
}

/**
 \brief get filelist from \p item, traverse if \p item is a directory.
 \param projectdir base directory
 \param item to process
 \param filelist pointer to the result
 */
static void getfilelist_item(const char * projectdir,const char *item,cJSON ** filelist)
{
   debug("getfilelist_item %s in %s\n",item,projectdir);
   struct stat sb;
   if (strlen(item)>=PATH_MAX)
   {
      debug("item exceeds PATHMAX length\n");
      return;
   }
   char normalized_path_buf[PATH_MAX];
   const char * normalized_path=normalizePath(projectdir, item, normalized_path_buf);
   debug("normalized_path from %s => %s\n",item, normalized_path);
   if (!normalized_path)
   {
      debug("normalized path error\n");
   }
   char realpathbuffer[PATH_MAX];
   // check if we stay within the project dir
   char * therealpath=realpath(item, realpathbuffer);
   debug("therealpath %s\n",therealpath);
   if (therealpath)
   {

      if (strncmp(therealpath, projectdir, strlen(projectdir))!=0 && strncmp(therealpath,"/cache/",7)!=0)
      {
         debug("real_path outside project dir path\n");
         return;
      }
   }
   else
   {
      error("problem finding real path for %s: %s\n",item,strerror(errno));
      return;
   }
   if (stat(item, &sb) == 0 && S_ISDIR(sb.st_mode))
   {

      //add_to_file_list(normalized_path, sb.st_mode, filelist);
      if (nftw_x((char*)normalized_path, list_it, 6 ,FTW_PHYS ,(void*)filelist)) //| FTW_DEPTH
      {
         perror( normalized_path );
         error("nftw return non 0\n");
         return ;
      }
   }
   else if (stat(item, &sb) == 0 && (S_ISLNK(sb.st_mode)|| S_ISREG(sb.st_mode)))
      add_to_file_list(normalized_path, sb.st_mode, filelist);
}


// public
int getfilelist(const char * projectdir,cJSON * paths,cJSON ** filelist)
{
   if (!projectdir || !paths || !filelist ) return -1;
   char tmp[PATH_MAX];
   cJSON * path;
   cJSON_ArrayForEach(path,paths)
   {
      int res =snprintf((char*)tmp,PATH_MAX,"%s/%s",projectdir,path->valuestring);
      if (res>=PATH_MAX) continue;
      debug("get filelist for path %s\n",tmp);
      wordexp_t word_re;
      // errors on undef and commands
      switch (wordexp(tmp, &word_re, WRDE_UNDEF))
      {
         case 0: /* Successful. */
            {
               int i;
               debug("wordexp arti path found %d matches",word_re.we_wordc);
               for (i=0;i<word_re.we_wordc;i++)
               {

                  getfilelist_item(projectdir,word_re.we_wordv[i],filelist);
               }
               wordfree (&word_re);
            }
            break;
         case WRDE_NOSPACE:
            /* If the error was WRDE_NOSPACE,
             then perhaps part of the result was allocated. */
            error("WRDE_NOSPACE \n");
            wordfree (&word_re);
            break;
         default: /* Some other error. */
            error("some othe wordexp error \n");
            break;
      }
      //cleanup
   }
   return 0;
}

void vrmdir(const char *dir, ...) {
	if (!dir) return;
	char *tmp=CSPRINTF(dir);
	struct stat sb;

	if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
	{
		if (nftw((char*)tmp, remove_it, 6 ,FTW_PHYS | FTW_DEPTH))
		{
			perror( tmp );
		}
	}
	if (tmp) free(tmp);
}

void vrmdir_s(const char *dir, ...) {
	if (!dir) return;
	char *tmp=CSPRINTF(dir);
	struct stat sb;

	if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
	{
		if (nftw((char*)tmp, remove_it, 6 , FTW_DEPTH))
		{
			perror( tmp );
		}
	}
	if (tmp) free(tmp);
}

int v_exist_dir(const char *dir, ...)
{
	if (!dir) return 0;
	char *tmp=CSPRINTF(dir);
	struct stat sb;
	int result=0;
	if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
		result=1;
	if (tmp) free(tmp);
	return result;
}

// mdir -p
void vmkdir(const char *dir, ...) {
	if (!dir) return;
	char *tmp=CSPRINTF(dir);
	char *p=NULL;
	size_t len=0;
	struct stat sb;
	if (tmp)
	{
		if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
		{
			//debug("directory %s exists\n",tmp);
		}
		else
		{
			len = strlen(tmp);
			if(tmp[len - 1] == '/')
				tmp[len - 1] = 0;
			//debug("mkdir %s\n",tmp);
			for(p = tmp + 1; *p; p++)
				if(*p == '/') {
					*p = 0;
					if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
					{
						//debug("sub directory %s exists\n",tmp);
					}
					else{
						//debug("mkdir sub %s\n",tmp);
						mkdir(tmp,S_IRWXG | S_IRWXU);
					}
					*p = '/';
				}
			//debug("mkdir sub %s\n",tmp);
			mkdir(tmp, S_IRWXG | S_IRWXU);
		}
		free(tmp);
	}
}

char * vcmkdir(const char *dir, ...) {
    if (!dir) return NULL;
	char *tmp=CSPRINTF(dir);
	char *p=NULL;
	size_t len=0;
	struct stat sb;
	if (tmp)
	{
		if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
		{
			//debug("directory %s exists\n",tmp);
		}
		else
		{
			len = strlen(tmp);
			if(tmp[len - 1] == '/')
				tmp[len - 1] = 0;
			//debug("mkdir %s\n",tmp);
			for(p = tmp + 1; *p; p++)
				if(*p == '/') {
					*p = 0;
					if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
					{
						//debug("sub directory %s exists\n",tmp);
					}
					else{
						//debug("mkdir sub %s\n",tmp);
						mkdir(tmp,S_IRWXG | S_IRWXU);
					}
					*p = '/';
				}
			//debug("mkdir sub %s\n",tmp);
			mkdir(tmp, S_IRWXG | S_IRWXU);
		}
	}
	return tmp;
}


int write_file_v(const char *data,size_t len,const char * filepath,... )
{
	if (!filepath) return -1;
	char * filename=CSPRINTF(filepath);
	if (!data) return -1;
	if(!filename)
	{
		error("processing filepath, no memory???\n");
		return -1;
	}

   FILE * f;
   if (len)
      f = fopen ( filename, "wb" );
   else
   {
      len=strlen(data);
      f = fopen ( filename, "w" );
   }
   if ( !f ) {
      /* handle error */
      free(filename);
      return -1;
   }
   size_t wrote=fwrite(data, 1, len, f);
   fclose(f);
	free(filename);
   return (wrote!=len);
}


char * read_a_file_v(const char * filename,...)
{
	if(!filename) return NULL;
	char * dirpath=CSPRINTF(filename);

	if(!dirpath)
	{
		error("processing filepath, no memory???\n");
		return NULL;
	}

   int f=open(dirpath,O_RDONLY);
   if (f==INVALID_SOCKET)
   {

      debug("read_v file %s: %s\n",dirpath,strerror(errno));
	   free(dirpath);
      return NULL;
   }
   dynbuffer *mem=dynbuffer_init();
   char buf[0x10000];
   bzero(buf, 0x10000);

   ssize_t n=0;
   while ((n=read(f,buf,0xffff)) > 0)

      //   while( ( fgets( buf, 1023, f )) != NULL )
   {
      dynbuffer_write_n(buf, n, mem );
      bzero(buf, 0x10000);
   }
   // fclose(f);
	free(dirpath);
   close(f);
	char * result=NULL;
	dynbuffer_pop(&mem, &result, NULL);
   return result;
}

char * read_a_file(const char * dirpath)
{
   if (!dirpath) return NULL;
   //FILE *f = fopen(dirpath, "r");
   //if (f == NULL)
   int f=open(dirpath,O_RDONLY);
   if (f==INVALID_SOCKET)
   {
      debug("read_previous release.json: %s\n",strerror(errno));
      return NULL;
   }
   dynbuffer* mem=dynbuffer_init();
   char buf[0x10000];
   bzero(buf, 0x10000);

   ssize_t n=0;
   while ((n=read(f,buf,0xffff)) > 0)

//   while( ( fgets( buf, 1023, f )) != NULL )
   {
      dynbuffer_write_n(buf, n, mem );
      bzero(buf, 0x10000);
   }
  // fclose(f);
   close(f);
	char * result=NULL;
	dynbuffer_pop(&mem, &result, NULL);
	return result;
}

static int cp_file_x(const char * source,const char * destination, int append);


int cp_file(const char * source,const char * destination)
{
   return(cp_file_x(source, destination, 0));
}
int append_file_2_file(const char * source,const char * destination)
{
   return(cp_file_x(source, destination, 1));
}

int append_data_2_file(const char * data,const char * destination)
{
   FILE  *exeout;
   exeout = fopen(destination, "ab");
   if (exeout == NULL) {
      /* handle error */
      debug("error opening: %s\n",destination);
      perror("file open for writing");
      return(-1);
   }

   size_t n=strlen(data);
   size_t m = fwrite(data, 1, n, exeout);
   if (fclose(exeout)) perror("close output file");
   return (int)m;
}

static int cp_file_x(const char * source,const char * destination, int append)
{
	if (!source || !destination) return -1;
    ssize_t count=0;
   FILE *exein, *exeout;
   exein = fopen(source, "rb");
   if (exein == NULL) {
      /* handle error */
	    debug("error opening: %s\n",source);
      perror("file open for reading");
      return(-1);
   }
   if (append)
   	exeout = fopen(destination, "ab");
   else
      exeout = fopen(destination, "wb");
   if (exeout == NULL) {
      /* handle error */
      debug("error opening: %s\n",destination);
      perror("file open for writing");
       if (fclose(exein)) perror("close input file");
      return(-1);
   }
   size_t n, m;
   unsigned char buff[8192];
   do {
      n = fread(buff, 1, sizeof buff, exein);
      if (n) m = fwrite(buff, 1, n, exeout);
      else   m = 0;
      count=count+m;
   } while ((n > 0) && (n == m));
   if (m)
   {
      perror("copy");
      count=-1;
   }
   if (fclose(exeout)) perror("close output file");
   if (fclose(exein)) perror("close input file");
   return (int)count;
}


static char const SIZE_PREFIXES[] = "kMGTPEZY";

const char *
format_size(char buf[FORMAT_SIZE_BUF], uint64_t sz)
{
	memset(buf,0,FORMAT_SIZE_BUF);
   int pfx = 0;
   unsigned int m, rem, hrem;
   uint64_t a,n;
   if (sz <= 0) {
      memcpy(buf, "0 B", 3);
      return buf;
   }
   a = sz;
   if (a < 1000) {
      n = a;
      snprintf(buf, FORMAT_SIZE_BUF, "%llu B", n);
      return buf;
   }
   for (pfx = 0, hrem = 0; ; pfx++) {
      rem = a % 1000ULL;
      a = a / 1000ULL;
      if (!SIZE_PREFIXES[pfx + 1] || a < 1000ULL)
         break;
      hrem |= rem;
   }
   n = a;
   if (n < 10) {
      if (rem >= 950) {
         buf[0] = '1';
         buf[1] = '0';
         buf[2] = ' ';
         buf[3] = SIZE_PREFIXES[pfx];
         buf[4] = 'B';
         buf[5] = '\0';
         return buf;
      } else {
         m = rem / 100;
         rem = rem % 100;
         if (rem > 50 || (rem == 50 && ((m & 1) || hrem)))
            m++;
         snprintf(buf, FORMAT_SIZE_BUF,
                  "%llu.%u %cB", n, m, SIZE_PREFIXES[pfx]);
      }
   } else {
      if (rem > 500 || (rem == 500 && ((n & 1) || hrem)))
         n++;
      if (n >= 1000 && SIZE_PREFIXES[pfx + 1]) {
         buf[0] = '1';
         buf[1] = '.';
         buf[2] = '0';
         buf[3] = ' ';
         buf[4] = SIZE_PREFIXES[pfx+1];
         buf[5] = 'B';
         buf[6] = '\0';
      } else {
         snprintf(buf, FORMAT_SIZE_BUF,
                  "%llu %cB", n, SIZE_PREFIXES[pfx]);
      }
   }
   return buf;
}





char * cvaprintf(const char * __restrict format , va_list vargs)
{
	va_list vargf;
	va_copy(vargf, vargs);

	int size=vsnprintf(NULL,0 ,format,vargs);
	va_end(vargs);
	if (size<0) return NULL; else size++;

	char * result=calloc(1,size);
	size=vsnprintf(result,size ,format,vargf);
	va_end(vargf);
	if (size<0)
	{
		if (result) free(result);
		result=NULL;
	}
	return result;
}

void strip_nl(char * data)
{
	if(data)
	{
		while (strlen(data)>1 && data[strlen(data)-1]=='\n') data[strlen(data)-1]='\0';
	}
	return;
}

char * get_opt_value(const char * optarg,const char* default_value)
{
	char * result=NULL;
	if (optarg)
	{
		asprintf(&result,"%s",optarg);
	}
	else if (default_value)
	{
		asprintf(&result,"%s",default_value);
	}
	else
	{
		result=calloc(1,1);
	}
	return result;
}

#ifndef HAVEPIPE2
#define pipe2(x,y) ({ int res=pipe(x); if (res==0) { fcntl(x[0], F_SETFL, y); fcntl(x[1], F_SETFL, y);} res;})
#endif

int pipe_redirect_stderr(int my_pipe[2])
{
	int t=0;
	if ((t=pipe2(my_pipe,O_NONBLOCK|FD_CLOEXEC))==0)
	{
		int e = dup(fileno(stderr));
		dup2(my_pipe[1], 2);
		return e;
	}
	else
		return -1;
}

void pipe_redirect_undo(int my_pipe[2],int saved_stderr,char**errormsg)
{
	if (saved_stderr !=-1)
	{
		char reading_buf[1024];
		memset(reading_buf, 0, 1024);
		fcntl(my_pipe[0], F_SETFL, O_NONBLOCK);
		ssize_t r=read(my_pipe[0], reading_buf, 1024);
		if (r>0)
			asprintf(errormsg, "%s",reading_buf);
		dup2(saved_stderr,fileno(stderr));
		close(saved_stderr);
		if (my_pipe[0]>0)close(my_pipe[0]);
		if (my_pipe[1]>0)close(my_pipe[1]);
	}
}


int xis_dir (const char *d)
{
	DIR *dirptr;
	
	if (access ( d, F_OK ) != -1 ) {
		// file exists
		if ((dirptr = opendir (d)) != NULL) {
			closedir (dirptr);
		} else {
			return -2; /* d exists, but not dir */
		}
	} else {
		return -1;     /* d does not exist */
	}
	
	return 0;
}

int safe_chmod(const char * target,mode_t m)
{
	if (!target) return -2;
	int res=0;
	int fd=open(target,O_RDONLY);
	if (fd !=INVALID_SOCKET)
		res=fchmod(fd, m);
	else
		res=fd;
	return res;
}

void oc_free(char ** m)
{
	if (m)
	{
		if (*m)
		{
			free(*m);
			*m=NULL;
		}
	}
}

int astrcatf(char ** target,const char * fmt,...)
{
	if (!target || !fmt) return -1;
	va_list vargs; va_start(vargs, fmt);
	char * s=NULL;
	int r=vasprintf(&s,fmt,vargs);
	va_end(vargs);
	if (r==-1) return -1;
	if (!*target)
	{ // *target string was empty, return new fmt string
			*target=s;
			s=NULL;
			return r;
	}
	else
	{ // have target string, append
		if (s)
		{
			char *t=NULL;
			int l=asprintf(&t, "%s%s",*target,s);
			free(s);
			s=NULL;
			if (l!=-1)
			{  // append success, return new string
				free(*target);
				*target=t;
				t=NULL;
			}
			else r=-1;
		}
		else r=-1;
	}
	return r;
}

