//
//  transfer_blobs.h
//  odagrun
//
//  Created by Danny Goossen on 29/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//
/*! \file transfer_blobs.h
 *  \brief Header file for parallel transfering blobs
 *  \author Created by Danny Goossen on 29/1/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 */
#ifndef __odagrun__transfer_blobs__
#define __odagrun__transfer_blobs__


#include "common.h"
#include "dkr_api.h"

/**
 * \brief docker registry transfer status
 * Enumeration of docker registry stgatus of transfer codes.
 */
typedef enum {
   /** Pending */
   DR_TR_STAT_PENDING,
   
   /** transfer completed */
   DR_TR_STAT_OK,
   
   /** transfer in progress */
   DR_TR_STAT_TRANSFER,
   
   /** transfer error */
   DR_TR_STAT_ERROR,
   
   /** transfer not needed */
   DR_TR_STAT_NA,
   
   /** transfer retrying */
   DR_TR_STAT_RETRY,

} dkr_tr_code;



/*!
 *  @brief transfer the blobs of a manifest from to image
 *
 *  @param manifest to transfer
 *  @param from_image images to transfer from
 *  @param to_image image_name to transfer to
 *  @param api_data struct dkr_api_data_s * (for authorisation)
 *  @return 0 on success;
 *
 */
int transfer_blobs(cJSON * manifest, char * from_image, char * to_image, struct dkr_api_data_s * api_data , struct trace_Struct * trace);



#endif /* defined(__odagrun__transfer_blobs__) */
