/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file QUAY_commands.h
 *  \brief odagrun internal commands for QUAY registry
 *  \author Created by Danny Goossen on 19/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 *
 */

#ifndef __oc_runner__QUAY_commands__
#define __oc_runner__QUAY_commands__


#include "common.h"

/**
 \brief  Set the docker short and long description of a docker Hub repository
 
 QUAY_api api
 
 \code QUAY_set_description [ --description=<description>][ --allow_fail]\endcode
 
 Defaults
 --description: ${ODAGRUN_IMAGE_DESCRIPTION}
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int QUAY_command_set_description(const cJSON * job,struct trace_Struct * trace,size_t argc,char* const *argv);

/**
 \brief Delete a Repository from Docker Hub
 
 Delete on existing Repository on the QUAY_api
 
 Command
 \code QUAY_delete_repository --name=<name> [--allow_fail]\endcode
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int QUAY_command_delete_repository(const cJSON * job,struct trace_Struct * trace,size_t argc,char* const*argv);

/**
 \brief  Delete a image-tag from QUAY_api registry
 
 Delete an existing image tag from QUAY_api
 
 Command \code QUAY_delete_tag --name=<name> --reference=<reference> [--allow_fail]\endcode
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int QUAY_command_delete_tag(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const *argv);


#endif /* defined(__oc_runner__QUAY_commands__) */
