/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file QUAY_api.c
 *  \brief api functions QUAY
 *  \author Created by Danny Goossen on 19/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 *
 */


#include "QUAY_api.h"
#include "deployd.h"
#include "simple_http.h"



// API interface to simple http

/**
 \brief create an QUAY response like internal error
 ApiError {
 title (string): Unique error code to identify the type of error.,
 detail (string, optional): Details about the specific instance of the error.,
 }
 \param title of the error
 \param detail of the error
 \return a json object conform QUAY api result opbject
 */
cJSON* internal_error_create(const char * title, const char * detail,...);

/**
 \brief create an QUAY response like internal error

 ApiError {
 title (string): Unique error code to identify the type of error.,
 detail (string, optional): Details about the specific instance of the error.,
 }
 \param info a prefix for the error
 \param code the curl error code
 \return a json object conform QUAY api result opbject
 */
cJSON* internal_http_error_create(const char * info,int code);




int QUAY_api_QUAY_create_repository(struct QUAY_api_data_s * api_data, const char *image, const char * description,const char * visibility,cJSON ** result)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	char * result_string=NULL;
	int res=0;
	
	res=chop_image(image, &registry_name, &namespace, &reference);
	if (res)
	{
		if (result) *result=internal_error_create("image","Could not parse image: %s",image);
	}
	else res=strcmp(registry_name,"quay.io");
	if (res)
	{
		if (result) *result=internal_error_create("Users Options","No a Quay image");
	}
	char * reponame=NULL;
	char * reponamespace=NULL;
	if (!res)
		res=chop_namespace(namespace, &reponamespace, &reponame);
	if (res)
	{
		if (result) *result=internal_error_create("image","Could not parse namespace: %s",namespace);
	}
	char * payload_str=NULL;
	if (!res)
	{
		cJSON * payload=NULL;
		payload=cJSON_CreateObject();
		if (payload)
		{
			/*
			 {
			 "repo_kind": "image",
			 "namespace": "gioxa",
			 "visibility": "public",
			 "repository": "testapi",
			 "description": "my new repo"
			 }
			 */
			cJSON_add_string(payload, "repo_kind", "image");
			cJSON_add_string(payload, "namespace", reponamespace);
			cJSON_add_string(payload, "repository", reponame);
			
			cJSON_add_string(payload, "description", description);
			cJSON_add_string(payload, "visibility", visibility);
			
			payload_str=cJSON_Print(payload);
			cJSON_Delete(payload);
			if (!payload_str)
			{
				if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
				res=-1;
			}
			
		}
		else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	
	cJSON * headers=NULL;
	if (!res)
	{
		headers=cJSON_CreateObject();
		if (headers)
		{
			cJSON_add_string_v(headers, "Authorization", "Bearer %s",api_data->token);
			cJSON_add_string(headers,"Accept","application/json");
			cJSON_add_string(headers, "Content-Type", "application/json");
		}else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	if (!res)
	{
		int count=0;
		do{
			if (result_string) oc_free(&result_string);
			result_string=NULL;
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, payload_str, 0,headers ,&result_string, 0, NULL, "POST", "https://%s/api/v1/repository",QUAY_REGISTRY);
		} while (((res>=500 && res<600) || res<0) && count<5);
		if (result_string && result) (*result)=cJSON_Parse(result_string);
		if (result &&!*result)
		{
			*result=internal_http_error_create("Curl Error", res);
		}
		
	}
	if (payload_str) oc_free(&payload_str);
	if (headers) {cJSON_Delete(headers);headers=NULL;}
	if (reponame) oc_free(&reponame);
	if (reponamespace)oc_free(&reponamespace);
	if (result_string) {oc_free(&result_string);result_string=NULL;}
	if(registry_name) oc_free(&registry_name);
	if(namespace) oc_free(&namespace);
	if(reference) oc_free(&reference);
	return res!=201;
}

int QUAY_api_QUAY_set_description(struct QUAY_api_data_s * api_data, const char *image,const char * description,cJSON**result)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	char * result_string=NULL;
	int res=0;
	
	res=chop_image(image, &registry_name, &namespace, &reference);
	if (res)
	{
		if (result) *result=internal_error_create("image","Could not parse image: %s",image);
	}
	else res=strcmp(registry_name,"quay.io");
	if (res)
	{
		if (result) *result=internal_error_create("Users Options","No a Quay image");
	}
	char * payload_str=NULL;
	if (!res)
	{
		cJSON * payload=NULL;
		payload=cJSON_CreateObject();
		if (payload)
		{
			if (description)
				cJSON_add_string(payload, "description", description);
			payload_str=cJSON_Print(payload);
			cJSON_Delete(payload);
			if (!payload_str)
			{
				if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
				res=-1;
			}
		}
		else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	
	cJSON * headers=NULL;
	if (!res)
	{
		headers=cJSON_CreateObject();
		if (headers)
		{
			cJSON_add_string_v(headers, "Authorization", "Bearer %s",api_data->token);
			cJSON_add_string(headers,"Accept","application/json");
			cJSON_add_string(headers, "Content-Type", "application/json");
			
		}else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	if (!res)
	{
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, payload_str, 0,headers ,&result_string, 0, NULL, "PUT", "https://%s/api/v1/repository/%s",QUAY_REGISTRY,namespace);
		} while (((res>=500 && res<600) || res<0) && count<5);
		if (result_string && result) (*result)=cJSON_Parse(result_string);
		if (result && !*result)
		{
			*result=internal_http_error_create("Curl Error", res);
		}
		
	}
	if (payload_str) oc_free(&payload_str);
	if (headers) {cJSON_Delete(headers);headers=NULL;}
	if (result_string) {oc_free(&result_string);result_string=NULL;}
	if(registry_name) oc_free(&registry_name);
	if(namespace) oc_free(&namespace);
	if(reference) oc_free(&reference);
	return res!=200;
}


int QUAY_api_QUAY_set_visibility(struct QUAY_api_data_s * api_data, const char *image,const char *visibility,cJSON**result)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	char * result_string=NULL;
	int res=0;
	
	res=chop_image(image, &registry_name, &namespace, &reference);
	if (res)
	{
		if (result) *result=internal_error_create("image","Could not parse image: %s",image);
	}
	else res=strcmp(registry_name,"quay.io");
	if (res)
	{
		if (result) *result=internal_error_create("Users Options","No a Quay image");
	}
	char * payload_str=NULL;
	if (!res)
	{
		cJSON * payload=NULL;
		payload=cJSON_CreateObject();
		if (payload)
		{
				cJSON_add_string(payload, "visibility", visibility);
			payload_str=cJSON_Print(payload);
			cJSON_Delete(payload);
			if (!payload_str)
			{
				if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
				res=-1;
			}
		}
		else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
		
	}
	cJSON * headers=NULL;
	if (!res)
	{
		headers=cJSON_CreateObject();
		if (headers)
		{
			cJSON_add_string_v(headers, "Authorization", "Bearer %s",api_data->token);
			cJSON_add_string(headers,"Accept","application/json");
			cJSON_add_string(headers, "Content-Type", "application/json");
		}else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	if (!res)
	{
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, payload_str, 0,headers ,&result_string, 0, NULL, "POST", "https://%s/api/v1/repository/%s/changevisibility",QUAY_REGISTRY,namespace);
		} while (((res>=500 && res<600) || res<0) && count<5);
		if (result_string && result) (*result)=cJSON_Parse(result_string);
		if (result && !*result)
		{
			*result=internal_http_error_create("Curl Error", res);
		}
		
	}
	if (payload_str) oc_free(&payload_str);
	if (headers) {cJSON_Delete(headers);headers=NULL;}
	if (result_string) {oc_free(&result_string);result_string=NULL;}
	if(registry_name) oc_free(&registry_name);
	if(namespace) oc_free(&namespace);
	if(reference) oc_free(&reference);
	return res!=201;
}

int QUAY_api_QUAY_delete_tag(struct QUAY_api_data_s * api_data, const char *image,const char * tag, cJSON **result)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	char * result_string=NULL;
	int res=0;
	
	res=chop_image(image, &registry_name, &namespace, &reference);
	if (res)
	{
		if (result) *result=internal_error_create("image","Could not parse image: %s",image);
	}
	else res=strcmp(registry_name,QUAY_REGISTRY);
	if (res)
	{
		if (result) *result=internal_error_create("Users Options","No a Quay image");
	}
	

	cJSON * headers=NULL;
	if (!res)
	{
		headers=cJSON_CreateObject();
		if (headers)
		{
			cJSON_add_string_v(headers, "Authorization", "Bearer %s",api_data->token);
			cJSON_add_string(headers,"Accept","application/json");
			
		}else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	
	if (!res)
	{
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, NULL, 0,headers ,&result_string, 0, NULL, "DELETE", "https://%s/api/v1/repository/%s/tag/%s",registry_name,namespace,tag);
		} while (((res>=500 && res<600) || res<0) && count<5);
		
		if (result_string && result) (*result)=cJSON_Parse(result_string);
		if (result && !*result)
		{
			*result=internal_http_error_create("Curl Error", res);
		}
	}
	if (headers) cJSON_Delete(headers);
	if(registry_name) oc_free(&registry_name);
	if(namespace) oc_free(&namespace);
	if(reference) oc_free(&reference);
	return res!=204;
}

int QUAY_api_QUAY_delete_repository(struct QUAY_api_data_s * api_data, const char *image, cJSON **result)
{
	char *registry_name=NULL;
	char *namespace=NULL;
	char *reference=NULL;
	char * result_string=NULL;
	int res=0;
	
	res=chop_image(image, &registry_name, &namespace, &reference);
	if (res)
	{
		if (result) *result=internal_error_create("image","Could not parse image: %s",image);
	}
	else res=strcmp(registry_name,QUAY_REGISTRY);
	if (res)
	{
		if (result) *result=internal_error_create("Users Options","No a Quay image");
	}
	
	cJSON * headers=NULL;
	if (!res)
	{
		headers=cJSON_CreateObject();
		if (headers)
		{
			cJSON_add_string_v(headers, "Authorization", "Bearer %s",api_data->token);
			cJSON_add_string(headers,"Accept","application/json");
			
		}else
		{
			if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			res=-1;
		}
	}
	
	if (!res)
	{
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, NULL, 0,headers ,&result_string, 0, NULL, "DELETE", "https://%s/api/v1/repository/%s",registry_name,namespace);
		} while (((res>=500 && res<600) || res<0) && count<5);
		
		if (result_string && result) (*result)=cJSON_Parse(result_string);
		if (result && !*result)
		{
			*result=internal_http_error_create("Curl Error", res);
		}
	}
	if (headers) cJSON_Delete(headers);
	if(registry_name) oc_free(&registry_name);
	if(namespace) oc_free(&namespace);
	if(reference) oc_free(&reference);
	return res!=204;
}


int QUAY_api_QUAY_read_repository(struct QUAY_api_data_s * api_data, const char *image, cJSON ** result)
{
	
		char *registry_name=NULL;
		char *namespace=NULL;
		char *reference=NULL;
		char * result_string=NULL;
		int res=0;
		
		res=chop_image(image, &registry_name, &namespace, &reference);
		if (res)
		{
			if (result) *result=internal_error_create("image","Could not parse image: %s",image);
		}
		else res=strcmp(registry_name,"quay.io");
		if (res)
		{
			if (result) *result=internal_error_create("Users Options","No a Quay image");
		}
		
		cJSON * headers=NULL;
		if (!res)
		{
			headers=cJSON_CreateObject();
			if (headers)
			{
				cJSON_add_string_v(headers, "Authorization", "Bearer %s",api_data->token);
				cJSON_add_string(headers,"Accept","application/json");
				
			}else
			{
				if (result) *result=internal_error_create("SYSTEM","Out Of Memory");
			}
		}
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res=custom_http_v(api_data->curl, NULL, 0,headers ,&result_string, 0, NULL, "GET", "https://%s/api/v1/repository/%s",QUAY_REGISTRY,namespace);
		} while (((res>=500 && res<600) || res<0) && count<5);
	   if (result_string && result) (*result)=cJSON_Parse(result_string);
		if (result && !*result)
		{
			*result=internal_http_error_create("Curl Error", res);
		}
		if (headers) cJSON_Delete(headers);
		if(registry_name) oc_free(&registry_name);
		if(namespace) oc_free(&namespace);
		if(reference) oc_free(&reference);
		return res;

}

int QUAY_api_init(struct QUAY_api_data_s ** api_data, const char *ca_cert_bundle ,const char * credentials)
{
	if (*api_data) QUAY_api_cleanup(api_data);
	
	(*api_data)=calloc(1,sizeof(struct QUAY_api_data_s));
	if (*api_data)
	{
		(*api_data)->curl =simple_http_init(ca_cert_bundle,easy_http_follow_location,easy_http_quiet, easy_http_SSL_VERIFYPEERL, PACKAGE_STRING);
		(*api_data)->retries=0;
		if ((*api_data)->curl)
		{
			if (ca_cert_bundle)(*api_data)->default_ca_bundle=strdup(ca_cert_bundle);
			if (credentials)(*api_data)->credential=strdup(credentials);
			curl_easy_setopt((*api_data)->curl, CURLOPT_NOPROGRESS, 1L);
			curl_easy_setopt((*api_data)->curl, CURLOPT_TIMEOUT, 60L); // 1min
			curl_easy_setopt((*api_data)->curl, CURLOPT_CONNECTTIMEOUT,30L); // 30 sec on connection
			return 0;
		}
		else
		{
			QUAY_api_cleanup(api_data);
			return -2;
		}
	}
	else return -1;
}

void QUAY_api_cleanup(struct QUAY_api_data_s ** api_data)
{
	if (*api_data)
	{
		curl_easy_setopt((*api_data)->curl, CURLOPT_CAINFO, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_READDATA,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEHEADER, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEDATA, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_WRITEFUNCTION, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_HEADERFUNCTION,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_READDATA, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_UPLOAD, 0L);
		curl_easy_setopt((*api_data)->curl, CURLOPT_INFILESIZE_LARGE,0L);
		curl_easy_setopt((*api_data)->curl, CURLOPT_POSTFIELDS,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_READFUNCTION, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_CUSTOMREQUEST,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_URL,NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_HTTPHEADER, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_PROGRESSDATA, NULL);
		curl_easy_setopt((*api_data)->curl, CURLOPT_PROGRESSFUNCTION, NULL);
		
		if ((*api_data)->credential) oc_free(&(*api_data)->credential);
		if ((*api_data)->token) oc_free(&(*api_data)->token);
		if ((*api_data)->errormsg) oc_free(&(*api_data)->errormsg);
		if ((*api_data)->default_ca_bundle) oc_free(&(*api_data)->default_ca_bundle);
		if ((*api_data)->curl) curl_easy_cleanup((*api_data)->curl);
		(*api_data)->curl=NULL;
		(*api_data)->credential=NULL;
		(*api_data)->default_ca_bundle=NULL;
		//debug("Cleanup *api_data\n");
		free(*api_data);
		debug("Done Cleanup *api_data\n");
		*api_data=NULL;
	}
	return ;
}

// internal helpers

cJSON* internal_error_create(const char * title,const char * detail,...)
{
	va_list vargs;
	va_start(vargs, detail);
	//title (string): Unique error code to identify the type of error.,
	//detail (string, optional): Details about the specific instance of the error.,
	cJSON*o=cJSON_CreateObject();
	cJSON_add_string(o, "title", title);
	cJSON_add_string_va(o, "detail", detail, vargs);
	va_end(vargs);
	return o;
}

cJSON* internal_http_error_create(const char * info,int code)
{
	//title (string): Unique error code to identify the type of error.,
	//detail (string, optional): Details about the specific instance of the error.,
	cJSON*o=cJSON_CreateObject();
	cJSON_add_string_v(o, "status", "%d",code);
	cJSON_add_string_v(o, "title", "curlerror %d",code);
	cJSON_add_string_v(o, "detail","%s: %s",info, print_curl_error(code));
	return o;
}
