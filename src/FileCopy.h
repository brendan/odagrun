//
//  FileCopy.h
//  odagrun
//
//  Created by Danny Goossen on 9/8/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __odagrun__FileCopy__
#define __odagrun__FileCopy__


#include "common.h"

int FileCopy(const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv);

#endif /* defined(__odagrun__FileCopy__) */
