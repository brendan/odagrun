/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file registry.h
 *  \brief Header file for Apllicatin api docker/ImageStream registry
 *  \author Created by Danny Goossen on 23/11/17.
 *  \copyright Copyright (c) 2017 Danny Goossen. All rights reserved.
 *
 */


#ifndef __odagrun__rootfs2registry__
#define __odagrun__rootfs2registry__

#include "common.h"
#include "dkr_api.h"
#include "openssl.h"
#include "wordexp_var.h"




/**
 \brief internal usage for dispatcher
*/
int rootfs_2_archive(struct trace_Struct *trace ,const char * image_dir, const char * rootfs_dir,char digest_tar[65+7],char digest_tar_gz[65+7],int *layer_size,const char * layername);

/**
 \brief process rootfs  and push to ImageStream registry
 
 archives rootfs while streaming to registry
 and pushes config and manifest.
 
 Pushes to openshift local image registry (ImageStream)
 
 Command line options:

 --rootfs[=<rootfs_dir>]
 or
 --archive[=<archive name>]
 
 
 --image=<full registry name of the image>
 --name=<image_name>
 --GLR (Gitlab Registry)
 --reference=<reference>
 --credentials=<base64 user:passwd>
 
 Defaults to:
 <rootfs>: <CI_PROJECT_DIR>/rootfs
 
 <reference>: latest
 
 <archive>: "<CI_PROJECT_DIR>/layer.tar.gz"
 
 availleble for use as build image 'ImageStream/is-<CI_PROJECT_PATH>[-<name>]:<reference>'
 
 \param job to execute
 \param trace the feedback structure for the build-trace in gitlab
 \param argc number of arguments
 \param argv the command line arguments from the build script
 \return 0 on success
 */
int registry_push(const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv);

/**
/brief bootstrap expander to get expansion of files in directory '--rootfs=directory'
 */
int bootstrap_expander_registry_push(word_exp_var_t * wev_main, const char* valuestring ,const cJSON * env_vars,const char * dir);

/*
 /brief registry push used internaly
 */
int int_registry_push(const cJSON * job,struct trace_Struct * trace,const char * rootfs,char * const *pathnames,size_t patname_cnt,const char *image,const char * from_image, const cJSON * environment);

/**
 /brief internal push image to ISR
 */
int image_2_ISR_int(struct dkr_api_data_s * apidata,struct trace_Struct * trace,char* image,char *image_dir, int ImageStream_v1Only);




/**
 \brief create a docker config_config V2 form docker_config.yml file *
 \param digest_tar the sha:256 digest of the tar file
 \param config_len pointer to result of the length of the created config
 \param digest_config digest of the created config is plae in this buffer
 \param docker_config cJSON object, if NULL, default will be created
 \param info_command to simulate docker
 \return image config V2 as char * buffer, Null on failure
 \note caller to free returned filename or config buffer
 */
char * create_config_json(const char digest_tar[SHA256_DIGEST_LENGTH_BLOBSUM], int * config_len,char digest_config[SHA256_DIGEST_LENGTH_BLOBSUM],const cJSON * docker_config, char * info_command );



/**
 \brief create a docker config.json v2 file or serialized json char *
 \param digest_tar the sha:256 digest of the tar file
 \param config_len pointer to result of the length of the created config
 \param digest_config digest of the created config is plae in this buffer
 \param filename varg style to put write config
 \return full filename or if filename was NULL, the config as char * buffer
 \note caller to free returned filename or config buffer
 */
char * create_config_json_file(const char digest_tar[SHA256_DIGEST_LENGTH_BLOBSUM], int * config_len,char digest_config[SHA256_DIGEST_LENGTH_BLOBSUM],const char * filename,... );

/**
 \brief create a v2 manifest for registry upload or serialized json char *
 \param digest_tar_gz digest layer.tar.gz
 \param digest_config digest config.yml
 \param layer_size size of the layer.tar.gz file
 \param config_len size of the config.yml file
 \param filename varg style filename to write manifest.yml to or NULL for serialized json char * return
 \returns filename manifest was writen to 
 \note caller is responsible to free the returned filename or serialized json char *
 */
char * create_manifest_json(const char digest_tar_gz[SHA256_DIGEST_LENGTH_BLOBSUM],const char digest_config[SHA256_DIGEST_LENGTH_BLOBSUM],int layer_size,int config_len,const char * filename,...);

/**
\brief  remote tag an image on a registry

tag's an existing image defaults to ImageStream:latest

Command line options:
 --image
 --name
 --GLR (Gitlab Registry)
 --reference=<reference>
 --credentials

Defaults to:
<reference>: latest

availleble for use as build image as:
 
 default        : 'ImageStream[/<name>]:[<reference>]'
          --GLR : 'Gitlab[/<name>]:[<reference>]'
 
 and external as:
 default     : '<oc-registry>/<oc-namespace>/is-<CI_PROJECT_PATH>[-<name>]:<reference>'
 
	   --GLR : '<CI-REGISTRY-IMAGE>[/<name>]:<reference>'
 
\param job to execute
\param trace the feedback structure for the build-trace in gitlab
\param argc number of arguments
\param argv the command line arguments from the build script
\return 0 on success
*/

int registry_tag_image(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv);


#endif /* defined(__odagrun__rootfs2registry__) */
