/*
 dyn_buffer.c
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

/** \file dyn_buffer.c
 *  \brief functions for Dynamic buffer manipulations
 *  \author Created by Danny Goossen on 23/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 */


#include "deployd.h"

/**
 * \brief Dynbuffer structure definition (internal)
 */
struct MemoryStruct {
	char *memory; /**< buffer */
	size_t size; /**< lenth of content of buffer */
	ssize_t read; /**< index in buffer for getc */
};

// functions for IO_Stream

int dynbuffer_ungetc(int c, dynbuffer *userp)
{
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
	if (mem->read >0)
	{
		mem->read--;
		
		mem->memory[mem->read]=(char)c;
		return 0;
	}
	else return -1;
	
}
int dynbuffer_getc(dynbuffer *userp)
{
	int c=0;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
	if (mem->read!=mem->size)
	{
		c=(int)mem->memory[mem->read];
		mem->read++;
		
	}
	else c=-1;
	return c;
}

size_t dynbuffer_fread( void * p, size_t s2, size_t s1,dynbuffer *userp)
{
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
	size_t len_requested=s2*s1;
	if (mem->read==mem->size) return 0;
	if (mem->size-mem->read>len_requested)
	{
		memcpy(p, mem->memory+mem->read, len_requested);
	    mem->read=mem->read+len_requested;
	    return len_requested;
	}
	else
	{
		len_requested=mem->size-mem->read;
		memcpy(p, mem->memory+mem->read, len_requested);
		mem->read=mem->read+len_requested;
		return len_requested;
	}
}

int dynbuffer_putc(int c,dynbuffer *userp)
{
	dynbuffer_write_n(&c, 1, userp);
	return 0;
}

size_t dynbuffer_fwrite(const void * p, size_t s2, size_t s1,dynbuffer *userp)
{
	
	return dynbuffer_write_n(p, s1*s2, userp);
}
int dynbuffer_fputs(const char * p,dynbuffer *userp)
{
	dynbuffer_write(p,userp);
	return 0;
}



/*------------------------------------------------------------------------
 * Helper function for Curl request
 *------------------------------------------------------------------------*/
size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, dynbuffer *userp)
{
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    void * tmp= realloc(mem->memory, mem->size + realsize + 1);
    if(tmp == NULL) {
       // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return 0;
       // LCOV_EXCL_STOP

    }
    else
        mem->memory=tmp;

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    return realsize;
}



// START Dynbuffer

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t dynbuffer_write(const void *contents, dynbuffer *userp)
{
    size_t realsize = strlen(contents);
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    void * tmp= realloc(mem->memory, mem->size + realsize + 1);
    if(tmp == NULL) {
      // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return 0;
      // LCOV_EXCL_STOP
    }
    else
        mem->memory=tmp;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}


/*------------------------------------------------------------------------
* Helper function write string to dynamic buffer
*------------------------------------------------------------------------*/
size_t dynbuffer_write_v( dynbuffer *userp,const char*data,...)
{
	char * contents=CSPRINTF(data);
	if (contents)
	{
		size_t realsize = strlen(contents);
		struct MemoryStruct *mem = (struct MemoryStruct *)userp;
		
		void * tmp= realloc(mem->memory, mem->size + realsize + 1);
		if(tmp == NULL) {
			// LCOV_EXCL_START
			/* out of memory! */
			// error("not enough memory (realloc returned NULL)\n");
			return 0;
			// LCOV_EXCL_STOP
		}
		else
		{
			mem->memory=tmp;
		
		}
		memcpy(&(mem->memory[mem->size]), contents, realsize);
		mem->size += realsize;
		mem->memory[mem->size] = 0;
		if (contents) free(contents);
		contents=NULL;
		return realsize;
	}
	else return 0;
}


/*------------------------------------------------------------------------
 * Helper function write non NULL string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t dynbuffer_write_n(const void *contents, size_t content_len, dynbuffer *userp)
{
    size_t realsize = content_len;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    void * tmp= realloc(mem->memory, mem->size + realsize + 1);
    if(tmp == NULL) {
        // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return 0;
        // LCOV_EXCL_STOP
    }
    else
        mem->memory=tmp;


    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

/*------------------------------------------------------------------------
 * intializes the dynamic buff
 *------------------------------------------------------------------------*/
dynbuffer * dynbuffer_init( void )
{
	struct MemoryStruct *mem = calloc(1,sizeof(struct MemoryStruct));
	if (mem)
	{
		mem->read=0;
		mem->memory = malloc( 1);
		mem->size=0;
		if(mem->memory == NULL) {
			free(mem);
			return NULL;
		}
		mem->memory[mem->size] = 0;
	}
	return mem;
}


/*------------------------------------------------------------------------
 * intializes the dynamic buff, with a given buffer, use pop to retrieve buff
 *------------------------------------------------------------------------*/
dynbuffer * dynbuffer_init_push( char ** ptr, size_t n)
{
	if (!ptr || !(*ptr)) return NULL;
	struct MemoryStruct *mem = calloc(1,sizeof(struct MemoryStruct));
	if (mem)
	{
		mem->memory = *ptr;
		*ptr=NULL;
		if (n!=0)
			mem->size=n;
		else
    		mem->size=strlen(mem->memory);
		mem->read=0;
		if(mem->memory == NULL) {
			free(mem);
			return NULL;
		}
	}
	return mem;
}


/*------------------------------------------------------------------------
 * frees the dynamic buff
 *------------------------------------------------------------------------*/
void dynbuffer_clear( dynbuffer **userp)
{
   struct MemoryStruct **mem = (struct MemoryStruct **)userp;
   if (mem && *mem)
   {
	   if ((*mem)->memory)free((*mem)->memory);
	   (*mem)->memory=NULL;
	   (*mem)->size=-0;
		free(*mem);
		*mem=NULL;
   }
   return ;
}


// detach data from dyn_buffer and free dynbuffer struct
void dynbuffer_pop(dynbuffer **userp, char ** buffer, size_t * len)
{
	struct MemoryStruct **mem = (struct MemoryStruct **)userp;
	if(mem && *mem && buffer)
	{
		*buffer=(*mem)->memory;
		(*mem)->memory=NULL;
		if (len) *len=(*mem)->size;
		(*mem)->size=0;
		free(*userp);
		*userp=NULL;
	}
}


const char * dynbuffer_get(dynbuffer *userp)
{
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
	return (const char *)mem->memory;
}

size_t dynbuffer_len(dynbuffer *userp)
{
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
	return mem->size;
}






