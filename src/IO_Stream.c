/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/*! \IO_Stream.c
 *  \brief helper callback to retrieve curl headers into cJSON object on the fly
 *  \author Created by Danny Goossen on  8/8/18.
 *  \copyright 2018, Danny Goossen. MIT License
*/


#include "IO_Stream.h"
#include "dyn_buffer.h"
#include <stdlib.h>
#include <stdio.h>


int IO_Stream_ferror_file(IO_Stream*self);
int IO_Stream_ferror_dynbuf(__attribute__((unused))IO_Stream*self);


int IO_stream_getc_file(IO_Stream *self);
int IO_Stream_getc_dynbuf(IO_Stream *self);
int IO_stream_ungetc_file(IO_Stream *self,int c);
int IO_Stream_ungetc_dynbuf(IO_Stream *self,int c);
size_t IO_Stream_fwrite_file(IO_Stream *self,const void * p, size_t s1, size_t s2);
size_t IO_Stream_fwrite_dynbuf(IO_Stream *self,const void * p, size_t s1, size_t s2);
int	 IO_Stream_fputs_file( IO_Stream * self,const char * p);
int	 IO_Stream_fputs_dynbuf(IO_Stream * self,const char * p);
int	  IO_Stream_putc_file(IO_Stream *self,int c);
int	  IO_Stream_putc_dynbuffer(IO_Stream *self,int c);
size_t IO_Stream_fread_file(IO_Stream *self,void * p, size_t s1, size_t s2);
size_t IO_Stream_fread_dynbuf(IO_Stream *self, void * p, size_t s1, size_t s2);

//TODO: define block size to copy
int IO_Stream_copy(IO_Stream * in, IO_Stream * out, char ** errormsg ,size_t BlockSize)
{
	int res=0;
	if (BlockSize==0) BlockSize=4096;
	char *buff=calloc(1,BlockSize);
	if (buff)
	{
		size_t rn_len=0;
		size_t wn_len=0;
		do{
			rn_len=IO_Stream_fread(in, buff, 1, BlockSize);
			if (rn_len>0)
			{
				wn_len=IO_Stream_fwrite(out, buff, 1, rn_len);
				if (wn_len==0)
				{
					res=-1;
					asprintf(errormsg, "Problem writing Stream: %s\n",strerror(errno));
				}
			}
			else if (IO_Stream_ferror(in)<0)
			{
				res=-1;
				asprintf(errormsg, "Problem reading Stream\n");
			}
		} while(rn_len&&wn_len);
		free(buff);
	}
	else
	{
	asprintf(errormsg, "Could not allocate copy buffer\n");
	}
	return res;
}


IO_Stream * IO_Stream_init(void * p,io_type_t type)
{
	if (!p) return NULL;
	struct IO_Stream_s * self=calloc(1, sizeof(struct IO_Stream_s));
	self->type=type;
	self->file=p;
	switch (type) {
		case io_type_file:
			self->getc=&IO_stream_getc_file;
			self->fputs=&IO_Stream_fputs_file;
			self->fwrite=&IO_Stream_fwrite_file;
			self->fread=&IO_Stream_fread_file;
			self->putc=&IO_Stream_putc_file;
			self->ungetc=&IO_stream_ungetc_file;
			self->ferror=&IO_Stream_ferror_file;
			break;
		case io_type_DynBuffer:
			self->getc=&IO_Stream_getc_dynbuf;
			self->fputs=&IO_Stream_fputs_dynbuf;
			self->fwrite=&IO_Stream_fwrite_dynbuf;
			self->fread=&IO_Stream_fread_dynbuf;
			self->putc=&IO_Stream_putc_dynbuffer;
			self->ungetc=&IO_Stream_ungetc_dynbuf;
			self->ferror=&IO_Stream_ferror_dynbuf;
			break;
	}
	return self;
}

void IO_Stream_clear(IO_Stream**self)
{
	if (self)
	{
		if (*self) free(*self);
		*self=NULL;
	}
}



int IO_Stream_ferror_file(IO_Stream*self)
{
	int error=0;
	{
		error=ferror (self->file);
		return error;
	}
	return(0);
}

int IO_Stream_ferror_dynbuf(__attribute__((unused))IO_Stream*self)
{
	return(0);
}


int IO_stream_getc_file(IO_Stream *self) {
	return getc(self->file);
}

int IO_Stream_getc_dynbuf(IO_Stream *self) {
	return dynbuffer_getc(self->file);
}

int IO_stream_ungetc_file(IO_Stream *self,int c) {
	return ungetc(c,self->file);
}

int IO_Stream_ungetc_dynbuf(IO_Stream *self,int c) {
	return dynbuffer_ungetc(c,self->file);
}



size_t IO_Stream_fwrite_file(IO_Stream *self,const void * p, size_t s1, size_t s2)
{
	return fwrite(p, s1, s2, self->file);
}

size_t IO_Stream_fwrite_dynbuf(IO_Stream *self,const void * p, size_t s1, size_t s2)
{
	return dynbuffer_fwrite(p, s1, s2, self->file);
}

size_t IO_Stream_fread_file(IO_Stream *self,void * p, size_t s1, size_t s2)
{
	return fread(p, s1, s2, self->file);
}

size_t IO_Stream_fread_dynbuf(IO_Stream *self, void * p, size_t s1, size_t s2)
{
	return dynbuffer_fread(p, s1, s2, self->file);
}

int	 IO_Stream_fputs_file( IO_Stream * self,const char * p)
{
	return fputs(p, self->file);
}

int	 IO_Stream_fputs_dynbuf(IO_Stream * self,const char * p)
{
	return dynbuffer_fputs(p,self->file);
}

int	  IO_Stream_putc_file(IO_Stream *self,int c)
{
	return putc(c,self->file);
}
int	  IO_Stream_putc_dynbuffer(IO_Stream *self,int c)
{
	return dynbuffer_putc(c,self->file);
}

