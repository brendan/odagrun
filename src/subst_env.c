/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


 Modified code from:

 http://git.savannah.gnu.org/cgit/gettext.git/tree/gettext-runtime/src/envsubst.c

 function : subst_from_stdin ()

 For use with IO_Stream and Environment, and error handling.

 License Source: GPL3

 Substitution of environment variables in shell format strings.
 Copyright (C) 2003-2007, 2012, 2015-2018 Free Software Foundation, Inc.
 Written by Bruno Haible <bruno@clisp.org>, 2003.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 */

/**
 * \file subst_env.c
 * \brief Substitude variables in IO_Stream from Environment
 * \author Created by Danny Goossen on  8/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 * \note modified Source from: http://git.savannah.gnu.org/cgit/gettext.git/tree/gettext-runtime/src/envsubst.c
 *
 */


#include "subst_env.h"
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "IO_Stream.h"
#include "Environment.h"



static int
do_getc ( IO_Stream * in_stream , char**errormsg)
{
	int c = IO_Stream_getc(in_stream); //((in_stream)->getc) (in_stream);

	if (c == EOF)
	{
		int error=0;
		if ((error=in_stream->ferror (in_stream))<0)
			asprintf(errormsg,"while reading input");
	}
	return c;
}

static inline void
do_ungetc (int c, IO_Stream * in_stream)
{
	if (c != EOF)
		in_stream->ungetc (in_stream,c);
}

/* modified source from:
  http://git.savannah.gnu.org/cgit/gettext.git/tree/gettext-runtime/src/envsubst.c
 subst_from_stdin ()
 Copies stdin to stdout, performing substitutions.
*/
int subst_IO_Stream(IO_Stream * in_stream,IO_Stream * out_stream,Environment * e,char**errormsg)
{
	static char *buffer=NULL;
	static size_t bufmax;
	static size_t buflen;
	int c;
	int res=0;
	for (;;)
	{
		c = do_getc ( in_stream , errormsg);
		if (c == EOF)
			break;
		/* Look for $VARIABLE or ${VARIABLE}.  */
		if (c == '$')
		{
			bool opening_brace = false;
			bool closing_brace = false;

			c = do_getc ( in_stream , errormsg);
			
		
			if (c == '{')
			{
				opening_brace = true;
				c = do_getc ( in_stream , errormsg);
			}
			
				
			if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_')
			{
				bool valid;

				/* Accumulate the VARIABLE in buffer.  */
				buflen = 0;
				do
				{
					if (buflen >= bufmax)
					{
						size_t new_bufmax= 2 * bufmax + 255;
						char * temp= realloc (buffer, new_bufmax);
						if (temp){
							buffer=temp;
							bufmax = new_bufmax;
						}
						else
						{
							res=-1;
							break;
						}
					}
					buffer[buflen++] = (char)c;
					c = do_getc ( in_stream , errormsg);
				}
				while ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')
					   || (c >= '0' && c <= '9') || c == '_');
				if (res) break;
				if (c=='$') {
					// double c set unvalid
					valid = false;
					
				}
				else if (opening_brace)
				{
					if (c == '}')
					{
						closing_brace = true;
						valid = true;
					}
					else
					{
						valid = false;
						
						do_ungetc (c,in_stream);
					}
				}
				else
				{
					valid = true;
				
					do_ungetc (c,in_stream);
				}
			
				if (valid)
				{
					/* Terminate the variable in the buffer.  */
					if (buflen >= bufmax)
					{
						size_t new_bufmax= 2 * bufmax + 255;
						char * temp= realloc (buffer, new_bufmax);
						if (temp){
							buffer=temp;
							bufmax = new_bufmax;
						}
						else
						{
							res=-1;
							break;
						}
					}
					buffer[buflen] = '\0';

				}

				if (valid)
				{
					/* Substitute the variable's value from the environment.  */
					
					const char *env_value = (e->getenv)(e,buffer);
				
					if (env_value != NULL)
						IO_Stream_fputs(out_stream,env_value);
					
				}
				else
				{
					/* Perform no substitution at all.  Since the buffered input
					 contains no other '$' than at the start, we can just
					 output all the buffered contents.  */
			
					IO_Stream_putc ( out_stream,'$');
					if (opening_brace)
						IO_Stream_putc ( out_stream,'{');
					IO_Stream_fwrite ( out_stream,buffer, buflen, 1);
					if (closing_brace)
						IO_Stream_putc ( out_stream,'}');
				}
			}
			
			else
			{
				
				if (c!='$')
					do_ungetc (c,in_stream);
				
				IO_Stream_putc ( out_stream,'$');
				if (opening_brace)
					IO_Stream_putc ( out_stream,'{');
				
			}
		}
		else
			IO_Stream_putc ( out_stream,c);
	} // for ; ;
	if (errormsg && *errormsg) res=1;
	return res;
}
