/*
 Copyright © 2018 by Danny Goossen, Gioxa Ltd. All rights reserved.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

/**
 * \file FileCopy.c
 * \brief odagrun command FileCopy
 * \author Created by Danny Goossen on  9/8/18.
 * \copyright 2018, Danny Goossen. MIT License
 */


#include "deployd.h"
#include "FileCopy.h"
#include "Environment.h"
#include "dyn_buffer.h"
#include "IO_Stream.h"
#include "subst_env.h"

/**
 \brief typedef parameter structure, \b  curl_parameter_s
 */
typedef  struct filecopy_parameter_s filecopy_parameter_t;

/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the curl API sub programs
 */
struct filecopy_parameter_s
{
	u_int8_t verbose; /**< verbose output */
	u_int8_t quiet;   /**< no output */
	u_int8_t allow_fail; /**< allow failure */
	u_int8_t substitute; /**< substitute stream with env */
	u_int8_t append; /**< apend to output */
	u_int8_t term; /**< output to terminal */
	char * to_file; /**< file to copy from */
	char * from_file; /**< file to copy from */
	char * from_text; /**< variable to copy from */
	char * to_var; /**< variable to copy to */
	u_int8_t skip; /**< on execute if not empty and strlen>0 */
};

/**
 \brief internal odagrun curl_post command
 */
int process_options_filecopy(size_t argc, char * const *argv, filecopy_parameter_t * parameters,char ** errormsg);

/**
 \brief internal free parameters
 */
void clear_filecopy_parameters(filecopy_parameter_t **parameter);



int FileCopy(const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv)
{
	int res=0;
	char *errormsg=NULL;
	
	debug("start filecopy\n");
	filecopy_parameter_t * parameters=calloc(1, sizeof(struct filecopy_parameter_s));
	if (!parameters)
	{
		error(" *** ERROR out of memory \n");
		Write_dyn_trace(trace, red, "\nError Out Of MEMORY\n");
		update_details(trace);
		return -1;
	}
	int argscount=0;
	parameters->skip=0;
	argscount=process_options_filecopy(argc, argv,parameters,&errormsg);
	if (errormsg) debug("stderr processoptions: %s\n",errormsg);
	if (argscount < 1 )
	{
		res=-1;
		error(" *** ERRORin command line options, exit \n");
		Write_dyn_trace(trace, red, "\nError command line options\n");
		Write_dyn_trace(trace, cyan, "\n %s\n",errormsg);
		update_details(trace);
		if(errormsg) oc_free(&errormsg);
		if(parameters->allow_fail) { Write_dyn_trace(trace, yellow, " Warning: ");Write_dyn_trace(trace, none, "allowed to fail, exit success\n");res= 0;}
		if (parameters) clear_filecopy_parameters(&parameters);
		return res;
	}
	else if (parameters->skip)
	{
		res=0;
		if(errormsg) oc_free(&errormsg);
		Write_dyn_trace(trace, none, "Info: skip execution through if-condition\n");
		update_details(trace);
		if (parameters) clear_filecopy_parameters(&parameters);
		return res;
		
	}
	else if (!parameters->from_file && !parameters->from_text)
	{
		res=-1;
		Write_dyn_trace(trace, red, "\nError command line options\n");
		Write_dyn_trace(trace, cyan, "\n missing source, define --from_file=<file> OR --from_text=<text>\n");
		update_details(trace);
		if(errormsg) oc_free(&errormsg);
		if(parameters->allow_fail) { Write_dyn_trace(trace, yellow, " Warning: ");Write_dyn_trace(trace, none, "allowed to fail, exit success\n");res= 0;}
		if (parameters) clear_filecopy_parameters(&parameters);
		return res;

	}
	else if (parameters->from_file && parameters->from_text)
	{
		res=-1;
		Write_dyn_trace(trace, red, "\nError command line options\n");
		Write_dyn_trace(trace, cyan, "\n can't process --from_file AND --from_text, chose one only!\n");
		update_details(trace);
		if(errormsg) oc_free(&errormsg);
		if(parameters->allow_fail) { Write_dyn_trace(trace, yellow, " Warning: ");Write_dyn_trace(trace, none, "allowed to fail, exit success\n");res= 0;}
		if (parameters) clear_filecopy_parameters(&parameters);
		return res;
	}
	else if (!parameters->to_file && !parameters->to_var && !parameters->term)
	{
		res=-1;
		Write_dyn_trace(trace, red, "\nError: command line options\n");
		Write_dyn_trace(trace, cyan, "\n missing destination, define --to_file=<file> OR --to_var=<var> OR --to_term\n");
		update_details(trace);
		if(errormsg) oc_free(&errormsg);
		if(parameters->allow_fail) {Write_dyn_trace(trace, yellow, " Warning: ");Write_dyn_trace(trace, none, "allowed to fail, exit success\n");res= 0;}
		if (parameters) clear_filecopy_parameters(&parameters);
		return res;
	}
	else if ( (parameters->to_file && parameters->to_var) || (parameters->to_file && parameters->term) || (parameters->term && parameters->to_var))
	{
		res=-1;
		Write_dyn_trace(trace, red, "\nError: command line options\n");
		Write_dyn_trace(trace, cyan, "\n can't process --to_file AND --to_var, chose one only!\n");
		update_details(trace);
		if(parameters->allow_fail) { Write_dyn_trace(trace, yellow, " Warning: ");Write_dyn_trace(trace, none, "allowed to fail, exit success\n");res= 0;}
		if (parameters) clear_filecopy_parameters(&parameters);
		if(errormsg) oc_free(&errormsg);
		return res;
	}
	
	else if ( (argc-argscount)>0)
	{
		Write_dyn_trace(trace, yellow, " Warning: ");
		Write_dyn_trace(trace, none, "ignoring non option arguments\n");
	
	}
	if(!parameters->quiet) parameters->verbose=1;
	cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
	
	FILE * in_f=NULL;
	FILE * out_f=NULL;
	dynbuffer * in_db=NULL;
	dynbuffer * out_db=NULL;
	IO_Stream * in_stream=NULL;
	IO_Stream * out_stream=NULL;
	Environment * e=NULL;
	char * command=NULL;
	char * insucces=NULL;
	char * outsucces=NULL;
	
	size_t BlockSize=0;
	if (parameters->from_file)
	{
			in_f=fopen(parameters->from_file, "r");
		if (in_f)
		{
			int fd = fileno(in_f);
			struct stat buf;
			fstat(fd, &buf);
			BlockSize = buf.st_size;
			if (BlockSize>4096L*1024L) BlockSize=4096L*1024L;
			in_stream=IO_Stream_init(in_f, io_type_file);
			if (in_stream)
			{
				asprintf(&insucces, "%s",parameters->from_file);
				//debug("insuccess\n");
			}
			else
			{
				if (!errormsg)
					asprintf(&errormsg, "problem init in_stream");
				error("problem init in_stream\n");
				res=-1;
			}
		}
		else
		{
			if (!errormsg)
				asprintf(&errormsg, "Error Open file in: %s",strerror(errno));
			res=-1;
		}
	}
	if (!res&& parameters->to_file)
	{
		if(parameters->append)
			out_f=fopen(parameters->to_file, "a");
		else
			out_f=fopen(parameters->to_file, "w");
		if (out_f)
		{
			out_stream=IO_Stream_init(out_f, io_type_file);
			if (!out_stream)
			{
				
				if (!errormsg)
					asprintf(&errormsg, "problem init out_stream");
				error("problem init out_stream\n");
				res=-1;
			}
			//else debug("have_outStream\n");
		}
		else
		{
			if (!errormsg)
				asprintf(&errormsg, "Open file out: %s",strerror(errno));
			debug("%s\n",errormsg);
			res=-1;
		}
		
	}
	
    if (!res && (parameters->substitute || parameters->to_var))
	{
		e=Environment_init(env_vars, environment_type_cJSON);
		if (!e)
		{
			error("init environment\n");
			if (!errormsg)
				asprintf(&errormsg, "Problem init Environment\n");
			res=-1;
		}
		//else debug("have init Environment\n");
	}
	if (!res && parameters->from_text)
	{
		
		in_db=dynbuffer_init();
		if (in_db)
		{
			dynbuffer_write(parameters->from_text, in_db);
			in_stream=IO_Stream_init(in_db, io_type_DynBuffer);
			BlockSize=dynbuffer_len(in_db);
		}
		if (in_stream) asprintf(&insucces, "from_text");
				else
		{
			if (!errormsg)
				asprintf(&errormsg, "Out of memory");
			res=-1;
		}
	}
	if (!res && parameters->to_var)
	{
		if (strlen(parameters->to_var)>0)
		{
			out_db=dynbuffer_init();
			if (out_db) out_stream=IO_Stream_init(out_db, io_type_DynBuffer);
			if (out_stream)
			{
				if (parameters->append)
				{
					const char * outvar=Environment_getenv(e,parameters->to_var);
					IO_Stream_fwrite(out_stream, outvar, strlen(outvar), 1);
				}
				asprintf(&outsucces, "$%s",parameters->to_var);
			}
			else
			{
				if (!errormsg)
					asprintf(&errormsg, "Out of Memory");
			}
		}
		else
		{
			if (!errormsg)
				asprintf(&errormsg, "\'--to_var=\' need an option argument");
			res=-1;
		}
	}
	if (!res && parameters->term)
	{
			out_db=dynbuffer_init();
			out_stream=IO_Stream_init(out_db, io_type_DynBuffer);
		    if (parameters->append)
			{
				Write_dyn_trace(trace, yellow, "Warning: ");
				Write_dyn_trace(trace, none, "Ignoreing option --append for output to Terminal\n");
				update_details(trace);
			}
	}
	//code for job
	
	if (!res && parameters->substitute)
	{
		//debug("start substitude\n");
		res=subst_IO_Stream(in_stream, out_stream, e, &errormsg);
		if (!res){
			if (!parameters->append || parameters->term)
			asprintf(&command, "substitute");
			else
				asprintf(&command, "substitute & append");
		}
		else
		{
			error("Substitute\n");
		}
	}
	else if(!res)
	{
		res=IO_Stream_copy(in_stream,out_stream,&errormsg,BlockSize);
		if (!res){
			if (!parameters->append || parameters->term)
			asprintf(&command, "copy");
			else
				asprintf(&command, "append");
		}
	}
	
	if (!res && e &&  parameters->to_var )
		Environment_setenv(e, parameters->to_var, dynbuffer_get(out_db), 1);
	if (!res && parameters->term)
		asprintf(&outsucces,"terminal");
	//debug("closing files");
	if (in_f) fclose(in_f);
	if(out_f)
	{
		res=fclose(out_f);
		if (res==EOF)
		{
		
			if (!errormsg)
				asprintf(&errormsg, "Closing file out: %s",strerror(errno));
			debug("errormsg:%s\n",errormsg);
			res=-1;
		}
		else
			asprintf(&outsucces, "%s",parameters->to_file);
	}
	if (e) Environment_clear(&e);
	if(res)
	{
		if (errormsg)
		{
			Write_dyn_trace(trace, red, "\n ERROR: ");
			Write_dyn_trace(trace,none, "%s\n",errormsg);
		}
		if (  parameters->allow_fail)
		{
			Write_dyn_trace(trace, yellow, " Warning: ");Write_dyn_trace(trace, none, "allowed to fail, exit success\n");
			res=0;
		}
	}
	else
	{
		if (parameters->verbose) Write_dyn_trace(trace, none, "%s %s to %s\n",command,insucces,outsucces);
		if (parameters->verbose && parameters->term) Write_dyn_trace(trace, none, ">>>>");
		if (parameters->term) Write_dyn_trace(trace, none, "%s" ,dynbuffer_get(out_db));
		if (parameters->verbose && parameters->term) Write_dyn_trace(trace, none, "<<<<");
	}
	update_details(trace);
	if(in_db) dynbuffer_clear(&in_db);
	if (out_db) dynbuffer_clear(&out_db);
	if (errormsg)
	{
		oc_free(&errormsg);
	}
	if (command) oc_free(&command);
	if(insucces) oc_free(&insucces);
	if(outsucces) oc_free(&outsucces);
	if (parameters)clear_filecopy_parameters(&parameters);
	
	return res;
}

// --from_file / --to_file --from_var / --to_var --substitude

int process_options_filecopy(size_t argc, char * const *argv, filecopy_parameter_t * parameters, char ** errormsg)
{
	debug("start process_options_filecopy\n");
	int my_pipe[2];
	my_pipe[0]=-1;
	my_pipe[1]=-1;
	int saved_stderr=pipe_redirect_stderr(my_pipe);
    //debug("saved_stdr: %d, pipe0: %d, pipe1: %d\n",saved_stderr,my_pipe[0],my_pipe[1]);
	//printf("Filecopy\n");
	//for (int i=0;i<argc;i++) printf("%s\n",argv[i]);
	struct option long_options[] = {
		
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ "from_file",     1, NULL, 'u' },
		{ "from_text",     1, NULL, 'w' },
		{ "to_file",       1, NULL, 'U' },
		{ "to_var",        1, NULL, 'W' },
		{ "substitute",    0, NULL, 's' },
		{ "append",        0, NULL, 'a' },
		{ "to_term",       0, NULL, 'T' },
		{ "if_value",      2, NULL, 'N' },
		{ "if_zero",       2, NULL, 'Z' },
		{ NULL,            0, NULL,  0  }
	};
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameters->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						pipe_redirect_undo(my_pipe, saved_stderr, errormsg);
						return -1;
					}
					else
					{
						parameters->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameters->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						pipe_redirect_undo(my_pipe, saved_stderr, errormsg);
						return -1;
					}
					else
						parameters->quiet = 1;
				}
					break;
				case 'f': {
					parameters->allow_fail=1;
				}
					break;
				case 'T': {
					parameters->term=1;
				}
					break;
				case 's': {
					parameters->substitute=1;
				}
					break;
				case 'a': {
					parameters->append=1;
				}
					break;
				case 'u': {
					parameters->from_file =get_opt_value(optarg, NULL);
				}
					break;
				case 'Z': {
					char * t =get_opt_value(optarg, "");
					size_t len=strlen(t);
					debug("Z : >%s< len=%d\n",t,len);
					parameters->skip=(parameters->skip || (len!=0));
					oc_free(&t);
				}
					break;
				case 'N': {
					char * t =get_opt_value(optarg, "");
					size_t len=strlen(t);
					debug("N : >%s< len=%d\n",t,len);
					parameters->skip=(parameters->skip || (len==0));
					oc_free(&t);
				}
					break;
				case 'w': {
					parameters->from_text=get_opt_value(optarg, NULL);
				}
					break;
				case 'U': {
					parameters->to_file =get_opt_value(optarg, NULL);
				}
					break;
				case 'W': {
					parameters->to_var=get_opt_value(optarg, NULL);
				}
					break;
					/* otherwise    : display usage information */
				default:
					pipe_redirect_undo(my_pipe, saved_stderr, errormsg);
					return -1;
					break;
			}
		}
	}
	//debug("undirect stderr\n");
	pipe_redirect_undo(my_pipe, saved_stderr, errormsg);
	debug("end_process_options\n");
	return optind;
}

#define FREE_MEM(x) {if (x) free(x); x=NULL;}

void clear_filecopy_parameters(filecopy_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		FREE_MEM((*parameter)->from_file);
		FREE_MEM((*parameter)->from_text);
		FREE_MEM((*parameter)->to_file);
		FREE_MEM((*parameter)->to_var);
		FREE_MEM(*parameter);
	}
	return;
}
