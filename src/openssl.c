/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! @file openssl.c
 *  @brief openssl api functions
 *  @author danny@gioxa.com
 *  @date 9 Sep 2017
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#include "openssl.h"
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <openssl/err.h>
#include "error.h"


// For thread safety curl https

#define MUTEX_TYPE       pthread_mutex_t
#define MUTEX_SETUP(x)   pthread_mutex_init(&(x), NULL)
#define MUTEX_CLEANUP(x) pthread_mutex_destroy(&(x))
#define MUTEX_LOCK(x)    pthread_mutex_lock(&(x))
#define MUTEX_UNLOCK(x)  pthread_mutex_unlock(&(x))
#define THREAD_ID        pthread_self()


/**
 This array will store all of the mutexes available to OpenSSL. 
 \see https://curl.haxx.se/libcurl/c/opensslthreadlock.html
*/
static MUTEX_TYPE *mutex_buf= NULL;

/**
 \see https://curl.haxx.se/libcurl/c/opensslthreadlock.html
 */
static void locking_function(int mode, int n, __attribute__((unused)) const char *file, __attribute__((unused)) int line)
{
   if(mode & CRYPTO_LOCK)
      MUTEX_LOCK(mutex_buf[n]);
   else
      MUTEX_UNLOCK(mutex_buf[n]);
}

/**
 \see https://curl.haxx.se/libcurl/c/opensslthreadlock.html
 */
static unsigned long id_function(void)
{
   return ((unsigned long)THREAD_ID);
}

int thread_setup(void)
{
   int i;

   mutex_buf = malloc(CRYPTO_num_locks() * sizeof(MUTEX_TYPE));
   if(!mutex_buf)
      return 0;
   for(i = 0;  i < CRYPTO_num_locks();  i++)
      MUTEX_SETUP(mutex_buf[i]);
   CRYPTO_set_id_callback(id_function);
   CRYPTO_set_locking_callback(locking_function);
   return 1;
}

int thread_cleanup(void)
{
   int i;

   if(!mutex_buf)
      return 0;
   CRYPTO_set_id_callback(NULL);
   CRYPTO_set_locking_callback(NULL);
   for(i = 0;  i < CRYPTO_num_locks();  i++)
      MUTEX_CLEANUP(mutex_buf[i]);
   free(mutex_buf);
   mutex_buf = NULL;
   return 1;
}

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>


void sha256_digest_string (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[SHA256_DIGEST_LENGTH_BLOBSUM])
{
	int i = 0;
	snprintf(outputBuffer,SHA256_DIGEST_LENGTH_BLOBSUM,"sha256:");
	size_t offset=strlen("sha256:");
	for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
	{
		size_t o=offset+ (i * 2);
		int buffleft=SHA256_DIGEST_LENGTH_BLOBSUM-(int)o;
		snprintf(outputBuffer + o,buffleft,"%02x", hash[i]);
	}
}

void sha256_digest_string_hash (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[SHA256_DIGEST_LENGTH_HASH])
{
	int i = 0;
	for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
	{
		int o=(i * 2);
		int buffleft=SHA256_DIGEST_LENGTH_HASH-o;
		snprintf(outputBuffer + o,buffleft,"%02x", hash[i]);
	}
}

int calc_sha256 (const char* path, char output[SHA256_DIGEST_LENGTH_BLOBSUM])
{
	FILE* file = fopen(path, "rb");
	if(!file) return -1;
	
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	const int bufSize = 32768;
	char* buffer = malloc(bufSize);
	int bytesRead = 0;
	if(!buffer) return -1;
	while((bytesRead = (int)fread(buffer, 1,bufSize, file)))
	{
		SHA256_Update(&sha256, buffer, bytesRead);
	}
	SHA256_Final(hash, &sha256);
	
	sha256_digest_string(hash, output);
	fclose(file);
	free(buffer);
	return 0;
}
int calc_sha256_e (const char* path, char output[SHA256_DIGEST_LENGTH_BLOBSUM],int * len)
{
	FILE* file = fopen(path, "rb");
	if(!file) return -1;
	*len=0;
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	const int bufSize = 32768;
	char* buffer = malloc(bufSize);
	int bytesRead = 0;
	if(!buffer) return -1;
	while((bytesRead = (int)fread(buffer, 1,bufSize, file)))
	{
		(*len)=(*len)+bytesRead;
		SHA256_Update(&sha256, buffer, bytesRead);
	}
	SHA256_Final(hash, &sha256);
	
	sha256_digest_string(hash, output);
	fclose(file);
	free(buffer);
	return 0;
}

int calc_sha256_buff (const char* buff,int size, char output[SHA256_DIGEST_LENGTH_BLOBSUM])
{
	if (!size) size=(int)strlen(buff);
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, buff, size);
	
	SHA256_Final(hash, &sha256);
	
	sha256_digest_string(hash, output);
	
	return 0;
}

int calc_sha256_digest_hash (const char* buff,int size, char output[SHA256_DIGEST_LENGTH_HASH])
{
	if (!size) size=(int)strlen(buff);
	unsigned char hash[SHA256_DIGEST_LENGTH];
	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, buff, size);
	SHA256_Final(hash, &sha256);
	sha256_digest_string_hash(hash, output);
	return 0;
}

