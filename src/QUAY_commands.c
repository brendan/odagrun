/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file QUAY_commands.h
 *  \brief odagrun internal commands for QUAY registry
 *  \author Created by Danny Goossen on 19/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 *
 */


#include "QUAY_commands.h"
#include "deployd.h"
#include "b64.h"
#include "QUAY_api.h"

/**
 \brief typedef parameter structure, \b  dkr_parameter_s
 */
typedef  struct QUAY_parameter_s QUAY_parameter_t;

/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the dkr API sub programs
 */
struct QUAY_parameter_s
{
	u_int8_t allow_fail;   /**< if set, on failure, exit code will be 0 */
	char * reference;     /**< tag reference to push image*/
	char * image;         /**< full registry image name */
	char * image_nick;         /**< full registry image name */
	char * creds;         /**< credentials base64 usr/token */
	char * image_description;     /**< the description as per QUAY_api */
	char * const * pathname; /**< pointer list of pathname to add to layer */
	size_t pathname_cnt; /**< number of pathnames */
	int private;         /**< Set docker registry private */
};

/**
 \brief clear parameters for QUAY api
 */
void clear_QUAY_parameters(QUAY_parameter_t **parameter);

/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options_QUAY_api_delete_tag(size_t argc, char * const *argv, QUAY_parameter_t *parameter, char ** error_msg);
/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options_QUAY_api_description(size_t argc, char * const *argv, QUAY_parameter_t *parameter, char ** error_msg);
/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter docker registry image parameters
 
 */
int process_options_QUAY_api_delete(size_t argc, char * const *argv, QUAY_parameter_t *parameter, char ** error_msg);

/**
 */
int QUAY_command_process_credentials(cJSON* env,struct QUAY_api_data_s * api_data);
/**
 */
int QUAY_command_get_oauth_token (struct QUAY_api_data_s * api_data);



int QUAY_command_set_description(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const *argv)
{
	int res=0;
	struct QUAY_api_data_s * apidata=NULL;
	
	QUAY_parameter_t * parameters=calloc(1, sizeof(struct QUAY_parameter_s));
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	int argscount=0;
	char *error_msg;
	argscount=process_options_QUAY_api_description(argc, argv,parameters,&error_msg);

	if (argscount < 0 || !parameters->image)
	{
		error(" *** ERRORin command line options, exit \n");
		if (argscount<0)
		{
			Write_dyn_trace(trace, red, "\n\tError processing option: ");
			Write_dyn_trace(trace, magenta, "\t%s\n",error_msg);
		}
		else
		{
			Write_dyn_trace(trace, red, "\nError missing options\n");
			if(!parameters->image)
				Write_dyn_trace(trace, cyan, "\n --image=<image> is mandatory\n");
		}
		update_details(trace);
		clear_QUAY_parameters(&parameters);
		if (error_msg) oc_free(&error_msg);
		return -1;
	}
	if (argscount!=argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	
	
	
	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_REGISTRY);
	if (!image)
	{
		Write_dyn_trace(trace, red, "\nError processing image nick\n");
		update_details(trace);
		clear_QUAY_parameters(&parameters);
		return -1;
	}
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	res=chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,QUAY_REGISTRY)!=0 || ! namespace)
	{
		Write_dyn_trace(trace, red, "\nNot a QUAY_api image\n");
		update_details(trace);
		clear_QUAY_parameters(&parameters);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		return -1;
	}
	else if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		clear_QUAY_parameters(&parameters);
		return -1;
	}
	
	res=QUAY_api_init(&apidata, DEFAULT_CA_BUNDLE, parameters->creds);
	if (!res)
	{
		res=QUAY_command_process_credentials(env_vars, apidata);
		if (res)
		{
			Write_dyn_trace(trace, red, "\n\tError Processing credentials for QUAY api.\n");
			if (apidata->errormsg)
				Write_dyn_trace(trace, magenta, "\t%s\n",apidata->errormsg);
			else
				Write_dyn_trace(trace, magenta, "\tReason not specified\n");
			update_details(trace);
		}
	}
	if(!res)
	{
		cJSON* quay_image_desc=cJSON_CreateObject();
		if (quay_image_desc)
		{
			char * filedata=NULL;
			if (parameters->image_description)
			{
				cJSON_add_string(quay_image_desc, "description", parameters->image_description);
			}
			else if(cJSON_get_key(env_vars, "ODAGRUN_IMAGE_DESCRIPTION"))
				cJSON_add_string_from_object(quay_image_desc, "description", env_vars, "ODAGRUN_IMAGE_DESCRIPTION");
			
			else
			{
				filedata=read_a_file_v("%s/description.md",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
				if (filedata)
				{
					// TODO change this, need command to put string in without copy, this can be big
					cJSON_add_string(quay_image_desc, "description", filedata);
					oc_free(&filedata);
				}
				else
				{
					filedata=read_a_file_v("%s/README.md",cJSON_get_key(env_vars, "CI_PROJECT_DIR"));
					if (filedata)
					{
						// TODO change this, need command to put string in without copy, this can be big
						cJSON_add_string(quay_image_desc, "description", filedata);
						oc_free(&filedata);
					}
				}
			}
			
			
			Write_dyn_trace(trace, none, "\n   Update QUAY_api repository: ");
			Write_dyn_trace(trace, cyan, "%s\n",namespace);
			
			
			// CI_PROJECT_VISIBILITY	10.3	all	The project visibility (internal, private, public)
			if (parameters->private==1)
			{
				Write_dyn_trace(trace, none, "   Setting Repository visibility: ");
				Write_dyn_trace(trace, red, "Private\n");
				cJSON_add_string(quay_image_desc, "visibility", "private");
			}
			else if(parameters->private==-1)
			{
				
				Write_dyn_trace(trace, none, "   Setting Repository visibility: ");
				Write_dyn_trace(trace, green, "Public\n");
				cJSON_add_string(quay_image_desc, "visibility", "public");
			}
			else if (parameters->private != 2)
			{
				int is_private=(cJSON_validate_contains_match(env_vars, "CI_PROJECT_VISIBILITY", "private") || cJSON_validate_contains_match(env_vars, "CI_PROJECT_VISIBILITY", "internal")  );
				Write_dyn_trace(trace, none, "   Setting Repository visibility: ");
				if (is_private)
				{
					Write_dyn_trace(trace, red, "Private\n");
					cJSON_add_string(quay_image_desc, "visibility", "private");
					
				}
				else
				{
					Write_dyn_trace(trace, green, "Public\n");
					cJSON_add_string(quay_image_desc, "visibility", "public");
				}
			}
			print_json(quay_image_desc);
			cJSON*result_read=NULL;
			if(!res)
			{
				Write_dyn_trace(trace, none,"\n");
				Write_dyn_trace_pad(trace, yellow, 60,"   * Read Repository %s",namespace);
				update_details(trace);
				
				res=QUAY_api_QUAY_read_repository(apidata, image,&result_read);
				print_json(result_read);
			}
			if (res==200)
			{
				if(cJSON_safe_IsTrue(result_read, "is_public"))
					cJSON_add_string(result_read, "visibility", "public");
				else
					cJSON_add_string(result_read, "visibility", "private");
		
				res=0;
				Write_dyn_trace(trace, green, "  [OK]\n");
				if (cJSON_strcmp_valuestring(result_read, quay_image_desc, "description")!=0)
				{
					Write_dyn_trace_pad(trace, yellow, 60,"   * update description...");
					update_details(trace);
					cJSON*result=NULL;
					res=QUAY_api_QUAY_set_description(apidata, image, cJSON_get_key(quay_image_desc, "description"), &result);
					if (!res)
					{
						Write_dyn_trace(trace, green, "  [OK]\n");
					}
					else
					{
						Write_dyn_trace(trace, red, "[FAIL]\n");
						Write_dyn_trace(trace, red, "\t%s\n",cJSON_get_key(result, "title"));
						Write_dyn_trace(trace, magenta, "\t%s\n",cJSON_get_key(result, "detail"));
					}
					if (result) cJSON_Delete(result);
					
				}
				else Write_dyn_trace_pad(trace, none, 60,"   * description up to date\n");
				update_details(trace);
				if (!res && cJSON_get_key(quay_image_desc, "visibility")&&cJSON_strcmp_valuestring(result_read, quay_image_desc, "visibility")!=0)
				{
					Write_dyn_trace_pad(trace, yellow, 60,"   * update visibility...");
					update_details(trace);
					cJSON*result=NULL;
					res=QUAY_api_QUAY_set_visibility(apidata, image, cJSON_get_key(quay_image_desc, "visibility"), &result);
					if (!res)
					{
						Write_dyn_trace(trace, green, "  [OK]\n");
					}
					else
					{
						Write_dyn_trace(trace, red, "[FAIL]\n");
						Write_dyn_trace(trace, red, "\t%s\n",cJSON_get_key(result, "title"));
						Write_dyn_trace(trace, magenta, "\t%s\n",cJSON_get_key(result, "detail"));
					}
					if (result) cJSON_Delete(result);
					
				}
				else Write_dyn_trace_pad(trace, none, 60,"   * visibility up to date \n");
				update_details(trace);
			}
			else
			{
				if (result_read && validate_key(result_read, "title", "not_found")!=0)
				{
					Write_dyn_trace(trace, red, "[FAIL]\n");
					Write_dyn_trace(trace, red, "\t%s\n",cJSON_get_key(result_read, "title"));
					Write_dyn_trace(trace, magenta, "\t%s\n",cJSON_get_key(result_read, "detail"));
				}
				else
				{
					Write_dyn_trace(trace, yellow, "[FAIL]\n");
					const char*description=cJSON_get_key(quay_image_desc, "description");
					const char*visibility=cJSON_get_key(quay_image_desc, "visibility");
					cJSON*result=NULL;
					Write_dyn_trace_pad(trace, yellow, 60,"   * Create QUAY image...");
					update_details(trace);
					res= QUAY_api_QUAY_create_repository(apidata, image,description , visibility, &result);
					if (!res)
					{
						Write_dyn_trace(trace, green, "  [OK]\n");
					}
					else
					{
						Write_dyn_trace(trace, red, "[FAIL]\n");
						Write_dyn_trace(trace, red, "\t%s\n",cJSON_get_key(result, "title"));
						Write_dyn_trace(trace, magenta, "\t%s\n",cJSON_get_key(result, "detail"));
					}
					if (result) cJSON_Delete(result);
				}
				update_details(trace);
			}
			
			if (result_read) cJSON_Delete(result_read);
			result_read=NULL;
		}
	}
	if(image) oc_free(&image);
	if (registry_name) oc_free(&registry_name);
	if (namespace) oc_free(&namespace);
	if (reference) oc_free(&reference);
	
	int af= parameters->allow_fail;
	clear_QUAY_parameters(&parameters);
	
	QUAY_api_cleanup(&apidata);
	
	if(res && af)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		update_details(trace);
		return 0;
	}
	
	return res;
}

int QUAY_command_delete_repository(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv)
{
	int res=0;
	struct QUAY_api_data_s * apidata=NULL;
	
	QUAY_parameter_t * parameters=calloc(1, sizeof(struct QUAY_parameter_s));
	int argscount=0;
	
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");

	char *error_msg=NULL;
	argscount=process_options_QUAY_api_delete(argc, argv,parameters,&error_msg);
	
	
	if (argscount < 0  || !parameters->image )
	{
		if (argscount<0)
		{
			Write_dyn_trace(trace, red, "\n\tError processing option: \n");
			if (error_msg)Write_dyn_trace(trace, magenta, "\t%s\n",error_msg);
		}
		else
		{
			Write_dyn_trace(trace, red, "\n\tError missing options\n");
			if(!parameters->image)
				Write_dyn_trace(trace, cyan, "\n\t --image=<image> is mandatory\n");
		}
		update_details(trace);
		error(" *** ERRORin command line options, exit \n");
		clear_QUAY_parameters(&parameters);
		if (error_msg) oc_free(&error_msg);
		return -1;
	}
	if (argscount!=argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	
	
	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_REGISTRY);
	
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	
	if (!res && image)res=chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,QUAY_REGISTRY)!=0 || ! namespace || !image)
	{
		Write_dyn_trace(trace, red, "\nNot a QUAY image\n");
		update_details(trace);
		clear_QUAY_parameters(&parameters);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		return -1;
	}
	else if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		clear_QUAY_parameters(&parameters);
		return -1;
	}
	res= QUAY_api_init(&apidata, DEFAULT_CA_BUNDLE, parameters->creds);
	if(res)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, red, "\nSystem Error: ");
		if (res==-1)
			Write_dyn_trace(trace, none, "Out Of Memeory\n");
		else if(res==-1)
			Write_dyn_trace(trace, none, "Failed to get new Curl Handle\n");
		update_details(trace);
	}
	else
	{
		Write_dyn_trace(trace, none, "\n   For Image: ");
		Write_dyn_trace(trace, cyan, "%s\n",namespace);
	}
	if (!res)
	{
		res=QUAY_command_process_credentials(env_vars, apidata);
		if (res)
		{
			Write_dyn_trace(trace, red, "\n\tError Processing credentials for QUAY api.\n");
			if (apidata->errormsg)
				Write_dyn_trace(trace, magenta, "\t%s\n",apidata->errormsg);
			else
				Write_dyn_trace(trace, magenta, "\tReason not specified\n");
			update_details(trace);
		}
	}
	if(!res)
	{
		Write_dyn_trace_pad(trace, yellow, 60,"   * Delete Image ");
		update_details(trace);
		cJSON*result=NULL;
		res=QUAY_api_QUAY_delete_repository(apidata, image,&result);
		if (!res)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
		}
		else if (validate_key(result, "title", "not_found")==0)
		{
			if (result && validate_key(result, "title", "not_found")!=0)
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, red, "\t%s\n",cJSON_get_key(result, "title"));
				Write_dyn_trace(trace, magenta, "\t%s\n",cJSON_get_key(result, "detail"));
			}
			else
			{
				res=0;
				Write_dyn_trace(trace, yellow, "  [OK]\n");
				Write_dyn_trace(trace, cyan, "\t%s\n",cJSON_get_key(result, "title"));
				Write_dyn_trace(trace, yellow, "\t%s\n",cJSON_get_key(result, "detail"));
			}
			
		}
		
		if (result) cJSON_Delete(result);
	}
	
	if(image) oc_free(&image);
	if (registry_name) oc_free(&registry_name);
	if (namespace) oc_free(&namespace);
	if (reference) oc_free(&reference);
	
	
	if (apidata)QUAY_api_cleanup(&apidata);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	update_details(trace);
	
	clear_QUAY_parameters(&parameters);
	return res;
}

int QUAY_command_delete_tag(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const *argv)
{
	int res=0;
	struct QUAY_api_data_s * apidata=NULL;
	
	QUAY_parameter_t * parameters=calloc(1, sizeof(struct QUAY_parameter_s));
	int argscount=0;
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	
	char *errormsg=NULL;
	argscount=process_options_QUAY_api_delete_tag(argc, argv,parameters,&errormsg);
	
	if (argscount < 0 || !parameters->image )
	{
		if (argscount<0)
		{
			Write_dyn_trace(trace, red, "\n\tError processing option: \n");
			Write_dyn_trace(trace, magenta, "\t%s\n",errormsg);
		}
		else
		{
			Write_dyn_trace(trace, red, "\nError missing options\n");
			if(!parameters->image)
				Write_dyn_trace(trace, cyan, "\n\t --image=<image> is mandatory\n");
		}
		error(" *** ERRORin command line options, exit \n");
		update_details(trace);
		clear_QUAY_parameters(&parameters);
		if (errormsg) oc_free(&errormsg);
		return -1;
	}
	if (argscount!=argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	
	
	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_REGISTRY);
	
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	
	if (!res && image)res=chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,QUAY_REGISTRY)!=0 || ! namespace || !image)
	{
		Write_dyn_trace(trace, red, "\nNot a QUAY_api image\n");
		update_details(trace);
		clear_QUAY_parameters(&parameters);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		return -1;
	}
	else if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		clear_QUAY_parameters(&parameters);
		return -1;
	}
	else if (!reference && !parameters->reference)
	{
		Write_dyn_trace(trace, red, "\nError missing a tag for image\n");
		Write_dyn_trace(trace, none, "\tAdd option:\n");
		Write_dyn_trace(trace, cyan, "\t --reference=<");
		Write_dyn_trace(trace, none, "reference");
		Write_dyn_trace(trace, cyan, ">\n");
		Write_dyn_trace(trace, none, "\tOR define an image with a reference e.g.:");
		Write_dyn_trace(trace, cyan, "\t quay.io/<namepath>:<");
		Write_dyn_trace(trace, none, "reference");
		Write_dyn_trace(trace, cyan, ">\n");
		update_details(trace);
		if(image) oc_free(&image);
		if (registry_name) oc_free(&registry_name);
		if (namespace) oc_free(&namespace);
		if (reference) oc_free(&reference);
		clear_QUAY_parameters(&parameters);
		return -1;
		
	}
	if (!parameters->reference && reference) parameters->reference=strdup(reference);
	res= QUAY_api_init(&apidata, DEFAULT_CA_BUNDLE, parameters->creds);
	if(res)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, red, "\nSystem Error: ");
		if (res==-1)
			Write_dyn_trace(trace, none, "Out Of Memeory\n");
		else if(res==-1)
			Write_dyn_trace(trace, none, "Failed to get new Curl Handle\n");
		update_details(trace);
	}
	else
	{
		Write_dyn_trace(trace, none, "\n   For image: ");
		Write_dyn_trace(trace, cyan, "%s\n",namespace);
	}
	if (!res)
	{
		res=QUAY_command_process_credentials(env_vars, apidata);
		if (res)
		{
			Write_dyn_trace(trace, red, "\n\tError Processing credentials for QUAY api.\n");
			if (apidata->errormsg)
				Write_dyn_trace(trace, magenta, "\t%s\n",apidata->errormsg);
			else
				Write_dyn_trace(trace, magenta, "\tReason not specified\n");
			update_details(trace);
		}
	}
	if(!res)
	{
		
		
		Write_dyn_trace_pad(trace, yellow, 60,"   * Delete tag %s",parameters->reference);
		update_details(trace);
		cJSON*result=NULL;
		res=QUAY_api_QUAY_delete_tag(apidata, image, parameters->reference,&result);
		if (!res)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
		}
		else if (validate_key(result, "title", "not_found")==0)
		{
			if (result && validate_key(result, "title", "not_found")!=0)
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, red, "\t%s\n",cJSON_get_key(result, "title"));
				Write_dyn_trace(trace, magenta, "\t%s\n",cJSON_get_key(result, "detail"));
			}
			else
			{
				res=0;
				Write_dyn_trace(trace, yellow, "  [OK]\n");
				Write_dyn_trace(trace, cyan, "\t%s\n",cJSON_get_key(result, "title"));
				Write_dyn_trace(trace, yellow, "\t%s\n",cJSON_get_key(result, "detail"));
			}
			
		}
		
		if (result) cJSON_Delete(result);
	}
	if (apidata) QUAY_api_cleanup(&apidata);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	update_details(trace);
	clear_QUAY_parameters(&parameters);
	if(image) oc_free(&image);
	if (registry_name) oc_free(&registry_name);
	if (namespace) oc_free(&namespace);
	if (reference) oc_free(&reference);
	return res;
}

int process_options_QUAY_api_description(size_t argc, char * const *argv, QUAY_parameter_t *parameter, char ** error_msg)
{
	struct option long_options[] = {
		{ "description",      1, NULL, 'd' },
		{ "image",            1, NULL, 'i' },
		{ "credentials",      1, NULL, 'e' },
		{ "allow-fail",       0, NULL, 'f' },
		{ "allow_fail",       0, NULL, 'f' },
		{ "allow-failure",       0, NULL, 'f' },
		{ "allow_failure",       0, NULL, 'f' },
		{ "set-private",      2, NULL, 'p' },
		{ NULL,               0, NULL, 0   }   };
	STDERR_REDIRECT
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			switch (which) {
				case 'd': {
					parameter->image_description =get_opt_value(optarg, NULL);
				}break;
				case 'i': {
					parameter->image =get_opt_value(optarg, NULL);
				}break;
				case 'e': {
					parameter->creds =get_opt_value(optarg, NULL);
				}
					break;
				case 'p': {
					if(optarg)
					{
						if (strcmp(optarg,"yes")==0)
							parameter->private = 1;
						else if (strcmp(optarg,"no")==0)
							parameter->private = -1;
						else if (strcmp(optarg,"none")==0)
							parameter->private = -1;
						else parameter->private = 0;
						
					}
					else
					{
						parameter->private = 1;
					}
				}
					break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
					/* otherwise    : display usage information */
				default:
					return -1;
					break;
			}
		}
	}
	STDERR_UNDIRECT(error_msg)
	return optind;
}

int process_options_QUAY_api_delete(size_t argc, char * const *argv, QUAY_parameter_t *parameter, char ** error_msg)
{
	struct option long_options[] = {
		{ "image",         1, NULL, 'i' },
		{ "allow-fail",       0, NULL, 'f' },
		{ "allow_fail",       0, NULL, 'f' },
		{ "allow-failure",       0, NULL, 'f' },
		{ "allow_failure",       0, NULL, 'f' },		{ "credentials",   1, NULL, 'e' },
		{ NULL,            0, NULL, 0 }
	};
	STDERR_REDIRECT
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			switch (which) {
				case 'i': {
					asprintf(&parameter->image,"%s",optarg);
				}break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
				case 'e': {
					asprintf(&parameter->creds,"%s",optarg);
				}
					break;
					/* otherwise    : display usage information */
				default:
					return -1;
					break;
			}
		}
	}
	STDERR_UNDIRECT(error_msg)
	return optind;
}

int process_options_QUAY_api_delete_tag(size_t argc, char * const *argv, QUAY_parameter_t *parameter, char ** error_msg)
{
	struct option long_options[] = {
		
		{ "reference",     1, NULL, 'r' },
		{ "tag",           1, NULL, 'r' },
		{ "image",         1, NULL, 'i' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ "credentials",   1, NULL, 'e' },
		{ NULL,         0, NULL, 0 } };
	STDERR_REDIRECT
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			/* depending on which option we got */
			switch (which) {
				case 'r': {
					asprintf(&parameter->reference,"%s",optarg);
				}break;
				case 'i': {
					asprintf(&parameter->image,"%s",optarg);
				}break;
				case 'e': {
					asprintf(&parameter->creds,"%s",optarg);
				}
					break;
				case 'f': {
					parameter->allow_fail=1;
				}
					break;
					/* otherwise    : display usage information */
				default:
					return-1;
					break;
			}
		}
	}
	STDERR_UNDIRECT(error_msg)
	return optind;
}


int QUAY_command_get_oauth_token (struct QUAY_api_data_s * api_data)
{
	int res=-1;
	unsigned char* up=NULL;
	jose_err err;
	size_t up_len=0;
	
	if (base64_decode(api_data->credential, strlen(api_data->credential), &up, &up_len, &err))
	{
		// {"username": "'${UNAME}'", "password": "'${UPASS}'"}
		// name=daniel&project=curl
		debug("uand p: %s",up);
		if(up)
		{
			char * tok=strchr((char*)up,':');
			int len_n=(int)(tok-(char*)up);
			int lenpw=(int)(up_len-len_n-1);
			
			if (strncmp((char*)up, "$oauthtoken", len_n)==0)
			{
				asprintf(&api_data->token,"%.*s",lenpw,tok+1);
				res=0;
			}
			else
			{
				if (!api_data->errormsg) api_data->errormsg=strdup("No credentials nor token availleble");
			}
			oc_free((char**)&up);
		}
	}
	return res;
}


int QUAY_command_process_credentials(cJSON* env,struct QUAY_api_data_s * api_data)
{
	int res=-1;
	if (!api_data->credential)
	{
		const char *quay_token=cJSON_get_key(env, "QUAY_OAUTH_TOKEN");
		const char *quay_credentials=cJSON_get_key(env, "QUAY_CREDENTIALS");
		if(quay_token)
		{
			api_data->token=strdup(quay_token);
			res=0;
		}
		else if (quay_credentials)
		{
			api_data->credential=strdup(quay_credentials);
			res=QUAY_command_get_oauth_token(api_data);
		}
		else
		{
			if (!api_data->errormsg) api_data->errormsg=strdup("No credentials nor token availleble");
		}
	}
	else
	{
		res=QUAY_command_get_oauth_token(api_data);
	}
	return res;
}

void clear_QUAY_parameters(QUAY_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		oc_free(&(*parameter)->image_description);
		oc_free(&(*parameter)->reference);
		
		oc_free(&(*parameter)->creds);
		oc_free(&(*parameter)->image_nick);
		free(*parameter);
		(*parameter)=NULL;
	}
	return;
}

