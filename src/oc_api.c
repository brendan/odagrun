/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file oc_api.c
 *  \brief api wrapper for openshift
 *  \author Danny Goossen
 *  \date 19/10/17
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */


#include "deployd.h"
#include "oc_api.h"
#include "c-ares.h"

void oc_api_cleanup(struct oc_api_data_s ** oc_api_data)
{
   if (*oc_api_data)
   {
		if ((*oc_api_data)->headers) curl_slist_free_all((*oc_api_data)->headers);
		(*oc_api_data)->curl=NULL;

		if ((*oc_api_data)->curl) curl_easy_cleanup((*oc_api_data)->curl);
		(*oc_api_data)->headers=NULL;

		oc_free(&(*oc_api_data)->master_url);
		oc_free(&(*oc_api_data)->token);
		oc_free(&(*oc_api_data)->headertoken);
		oc_free(&(*oc_api_data)->ca_cert_bundle);

		free(*oc_api_data);
		*oc_api_data=NULL;
   }
   return ;
}

int oc_api_init(  struct oc_api_data_s ** oc_api_data, const char * ca_cert_bundle, const char * master_url, const char * token)
{
   if (*oc_api_data) oc_api_cleanup(oc_api_data);
   *oc_api_data=calloc(1,sizeof(struct oc_api_data_s));
   char headertoken[0x10000];
   snprintf(headertoken,0x10000, "Authorization: Bearer %s",token);
   (*oc_api_data)->headertoken=strdup(headertoken);
   (*oc_api_data)->master_url=strdup(master_url);
   if (token)(*oc_api_data)->token=strdup(token);
   else (*oc_api_data)->token=NULL;
   (*oc_api_data)->curl = curl_easy_init();
   (*oc_api_data)->ca_cert_bundle=strdup(ca_cert_bundle);
   if ((*oc_api_data)->curl)
   {
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_CAINFO, (*oc_api_data)->ca_cert_bundle);
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_SSL_VERIFYPEER, 0L);
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_FOLLOWLOCATION, 1L);
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_VERBOSE, 0L);
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_NOPROGRESS, 1L);
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_TIMEOUT, 60L);
	  curl_easy_setopt((*oc_api_data)->curl, CURLOPT_USERAGENT, PACKAGE_STRING);
      // headers
      (*oc_api_data)->headers = curl_slist_append((*oc_api_data)->headers, (*oc_api_data)->headertoken);
      (*oc_api_data)->headers = curl_slist_append((*oc_api_data)->headers, "accept: application/json");
      (*oc_api_data)->headers = curl_slist_append((*oc_api_data)->headers, "expect:");
      (*oc_api_data)->headers = curl_slist_append((*oc_api_data)->headers, "Content-Type: application/json");
      curl_easy_setopt((*oc_api_data)->curl, CURLOPT_HTTPHEADER, (*oc_api_data)->headers);
      return (0);
   }
   else
   {
      oc_api_cleanup(oc_api_data);
      return 1;
   }
}

int oc_api( struct oc_api_data_s * oc_api_data ,char * methode, cJSON * data_json, cJSON ** result, const char * api, ...)
{
	CURLcode res;

	if (result && *result)
	{
		cJSON_Delete(*result);
		*result=NULL;
	}
	int exit_code=0;
	char * api_buf=NULL;
	api_buf=CSPRINTF(api);
	if (!api)
	{
		return -1;
	}
	
   //build request
	char * request=NULL;
	asprintf(&request, "%s%s",oc_api_data->master_url,api_buf);
	if (api_buf) oc_free(&api_buf);
	api_buf=NULL;

	if(oc_api_data->curl && request)
	{
	  // prepare mem
      dynbuffer * chunk=dynbuffer_init();
      
      debug("\n%s %s\n",methode,request);
      if (strcmp(methode, "HEAD")==0)
	  {
         curl_easy_setopt(oc_api_data->curl, CURLOPT_NOBODY, 1L);
	  }
	  else
	  {
         curl_easy_setopt(oc_api_data->curl, CURLOPT_NOBODY, 0L);
	  }
	  // set request
      curl_easy_setopt(oc_api_data->curl, CURLOPT_URL, (char*)request);
      
      // set REQUEST FIELDS, if any
      if (data_json)
      {
         char *data=cJSON_PrintUnformatted(data_json);
         curl_easy_setopt(oc_api_data->curl, CURLOPT_POSTFIELDS, data);
      }
      else curl_easy_setopt(oc_api_data->curl, CURLOPT_POSTFIELDS, "");
      
      // set methode ( GET, POST ... )
      curl_easy_setopt(oc_api_data->curl, CURLOPT_CUSTOMREQUEST, methode);
      /* send all data to this function  */
      curl_easy_setopt(oc_api_data->curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
      /* we pass our 'chunk' struct to the callback function */
      curl_easy_setopt(oc_api_data->curl, CURLOPT_WRITEDATA, chunk);
      
      // execute request
      res = curl_easy_perform(oc_api_data->curl);
      
      // if any data returned, put parse into result
      if (dynbuffer_len(chunk)>1 && result) *result = cJSON_Parse(dynbuffer_get(chunk));
      
      //process http_code
      long http_code = 0;
      if (res==CURLE_OK)
         curl_easy_getinfo (oc_api_data->curl, CURLINFO_RESPONSE_CODE, &http_code);
      else
         http_code=-1;
      if (http_code >= 200 && http_code < 300)
      {
         exit_code=0;
      }
      else
      {
         if (res!=CURLE_OK)
         {
            error("oc_api: curl error#%d for %s %s\n",res,methode,request);
            exit_code=res;
         }
         else
         {
            error("oc_api: HTTP_code %ld for %s %s\n",http_code,methode,request);
            debug("oc_api: return data for #%d: %s\n",http_code,dynbuffer_get(chunk));
            exit_code=(int)http_code;
         }
         
      }
      dynbuffer_clear(&chunk);
   }
   else
      exit_code=1;
   oc_free(&request);
   return exit_code;
}

int create_imagestream(struct oc_api_data_s * oc_api_data,const cJSON * job,const char * oc_image_name)
{
	int res=0;
	if (oc_api_data)
	{
		const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
		const char * OKD_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
		res=oc_api(oc_api_data, "GET", NULL, NULL, "/oapi/v1/namespaces/%s/imagestreams/%s",OKD_NAMESPACE,oc_image_name);
		debug("result imagestream GET %d ",res);
		if (res==404)
		{
			cJSON * imagestream=make_imagestream_template( OKD_NAMESPACE,oc_image_name);
			if (imagestream)
			{
				res=oc_api(oc_api_data, "POST", imagestream, NULL, "/oapi/v1/namespaces/%s/imagestreams",OKD_NAMESPACE);
				debug("result imagestream POST %d ",res);
			}
			if (res==201) res=1;
		}
		else if (res==200) res=1;
		
	}
	return res;
}

int oc_api_get_restrictions(struct oc_api_data_s * oc_api_data,char * name_space, char * hostname,uid_t * start_uid, uid_t * range,char ** scc, char ** node)
{

   cJSON * result=NULL;
   /* => get uid constrains within pro<#char *methode#>ject
    curl -k -H "Authorization: Bearer hC414BBg0mlvv-viCpOLALC1zrzTWznK0UWhKg4Lr9I" -H 'Accept: application/json' https://oc.gioxa.com:8443/api/v1/namespaces/odagrun
    {
    "kind": "Namespace",
    "apiVersion": "v1",
    "metadata": {
    "name": "odagrun",
    "selfLink": "/api/v1/namespaces/odagrun",
    "uid": "0cb74717-be11-11e7-96f8-001e673ea749",
    "resourceVersion": "492286",
    "creationTimestamp": "2017-10-31T07:56:49Z",
    "annotations": {
    "openshift.io/sa.scc.mcs": "s0:c11,c0",
    "openshift.io/sa.scc.supplemental-groups": "1000110000/10000",
    "openshift.io/sa.scc.uid-range": "1000110000/10000"
    }
    },
    ............
    */
   
   int res=oc_api(oc_api_data, "GET", NULL, &result, "/api/v1/namespaces/%s",name_space);
   if (!res)
   {
      if (result)
      {
         cJSON * metadata=cJSON_GetObjectItem(result, "metadata");
         if (metadata)
         {
         cJSON * annotations=cJSON_GetObjectItem(metadata, "annotations");
         if (annotations)
         {
            const char * uid_range=cJSON_get_key(annotations, "openshift.io/sa.scc.uid-range");
            debug("api uid_range:%s",uid_range);
            if (uid_range)
            {
               char *endptr=NULL;
               *start_uid=(uid_t)strtol(uid_range, &endptr, 10);
               char * temp=endptr+1;
               endptr=NULL;
               *range=(uid_t)strtol(temp, &endptr, 10);
            }
         }
         }
         cJSON_Delete(result);
         result=NULL;
      }
      else debug("no result from api call \n");
   }
   /* => get  "openshift.io/scc": "restricted" // or "openshift.io/scc": "anyuid" or ....
    curl -k -H "Authorization: Bearer $TOKEN" -H 'Accept: application/json' https://$ENDPOINT/api/v1/namespaces/odagrun/pods/dc-odagrun-9-2w48j
    {
    "kind": "Pod",
    .....
    "annotations": {
    "kubernetes.io/created-by": "{\"kind\":\"SerializedReference\",\"apiVersion\":\"v1\",\"reference\":{\"kind\":\"ReplicationController\",\"namespace\":\"odagrun\",\"name\":\"dc-odagrun-9\",\"uid\":\"c4e220aa-c74c-11e7-88ee-0ab8769191d3\",\"apiVersion\":\"v1\",\"resourceVersion\":\"1666387558\"}}\n",
    "openshift.io/deployment-config.latest-version": "9",
    "openshift.io/deployment-config.name": "dc-odagrun",
    "openshift.io/deployment.name": "dc-odagrun-9",
    "openshift.io/scc": "restricted"
    },
    ....
    */
	if (result) cJSON_Delete(result);
   result=NULL;
   if (!res)
     res=oc_api(oc_api_data, "GET", NULL, &result, "/api/v1/namespaces/%s/pods/%s",name_space,hostname);
   if (!res)
   {
      if (result)
      {
         cJSON * metadata=cJSON_GetObjectItem(result, "metadata");
         if (metadata)
         {
            cJSON * annotations=cJSON_GetObjectItem(metadata, "annotations");
            if (annotations) *scc=cJSON_get_stringvalue_dup(annotations, "openshift.io/scc");
         }
         if (node)
         {
            *node=NULL;
             cJSON * spec=cJSON_GetObjectItem(result, "spec");
             if (spec)
             {
                *node=cJSON_get_stringvalue_dup(spec, "nodeName");
             }
             if (*node && strcmp(*node,"localhost")==0)
             {
                 cJSON * status=cJSON_GetObjectItem(result, "status");
                 if (status)
                 {
                    const char * hostIP=cJSON_get_key(status, "hostIP");
                    if (hostIP) {
                       char * hostipdomain=gethost(hostIP);
                       if (hostipdomain)
                          *node=hostipdomain;
                       else
                          *node=strdup(hostIP);
                    }
                 }
             }
            
         }
	 }
   }
	else
    	debug("no result from api call \n");
	if (result) cJSON_Delete(result);
	result=NULL;
   return res;
}

cJSON* make_imagestream_template( const char * namespace, const char * image_name)
{
	cJSON* ImageStream_map=cJSON_CreateObject();
	cJSON * metadata=cJSON_CreateObject();
	cJSON * spec=cJSON_CreateObject();
	cJSON * local=cJSON_CreateObject();
	
	cJSON_AddStringToObject(ImageStream_map, "apiVersion", "v1");
	cJSON_AddStringToObject(ImageStream_map, "kind",       "ImageStream");
	cJSON_AddItemToObject  (ImageStream_map, "metadata",    metadata);
	cJSON_AddStringToObject(metadata, "name",image_name);
	cJSON_AddStringToObject(metadata, "namespace",namespace);
	cJSON_AddItemToObject  (ImageStream_map, "spec",  spec);
	cJSON_AddItemToObject(spec, "lookupPolicy", local);
	cJSON_AddFalseToObject(local,"local");
	
	return ImageStream_map;
}

