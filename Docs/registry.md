
# Build-in commands

<br>

## Registry API

<br>

### Registry Push

**`registry_push [options] [pathname ...]`**

##### options:

 - `--rootfs[=<path2rootfs>]` push a rootfs to registry, default `./rootfs`.
 - `--archive[=<path2archive.tar.gz>]` push an archive as layer, default `./layer.tar.gz`.
 - `--from_image=<repo_path>[:tag]` from a qualified reponame
 - `--from_ISR` default:`<IS_REGISTRY>/namespace/is-project_path_slug[-name][:latest]`
 - `--from_GLR` default:`<CI_REGISTRY>/<CI_PROJECT_PATH>[/name][:latest]`
 - `--from_name` optional name only with:
      - `--from_ISR`
      - `--from_GLR`.
 - `--from_reference` optional reference only with:
      - `--from_ISR`
      - `--from_GLR`
 - `--from_credentials` for pulling an image,
 - `--image=<repo_path>[:tag]` push to image.
 - `--ISR` defaults to `<IS_REGISTRY>/<namespace>/is-<project_path_slug>[-<--name>][:latest]`
 - `--GLR`  defaults to `<CI_REGISTRY>/<CI_PROJECT_PATH>[/<--name>][:latest]`
 - `--name`  optional name only valid with:
      - `--ISR`
      - `--GLR`
 - `--reference`  optional reference only valid with:
      - `--ISR`
      - `--GLR`
 - `--credentials` credentials to be used to push image format:
      - format: `base64("<user>:<token>")`
 - `--config` docker config file, default: `./config.yaml`
 - `--u2g` copies the permissions of the owner to the group.
 - `--skip_label` will not append a dummy layer with container command history and not update the `io.or-runner.from.xxx`, only valid on a pure Transfer, thus:
      - `--from_xxx`
      - no `--config` option present
      - no `--rootfs` option present
      - no `--archive` option present


##### pathname:

 Files and/or directories to include in the image layer. (filter)

- optional defaults to `.` when `--rootfs` is defined
- supports for simple variable expansion, through `wordexp()`,
- If `--rootfs` is present, wildcards (`*`) are expanded relative to what is defined in `--rootfs`


<br>
<div class="alert alert-warning"><strong>Warning!</strong> <code>pathname</code> does not support brace expansion: e.g. <code>registry_push usr/{bin,lib}</code> will fail!</div>
<br>

#### registry_push Functions:

##### Authorisation:

By default, the oc-dispatcher **and** the oc-executor will use all the availeble tokens at hand:

1. for dockerHub: the secret variable `DOCKER_CREDENTIALS`.
2. for Quay.io: the secret variable `QUAY_CREDENTIALS` or `QUAY_OAUTH_TOKEN`
3. for any image mathing the `CI_REGISTRY`, the `CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD`.
4. for ImageStream the service account token of the `oc-dispatcher`.

This default behavior can be overwritten with the options:

| option | target |
|-------------------------|----------------|
| **credentials=**<br>`eyxxxxxxxxxxxx==` | for pushing an image |
| **from_credentials=**<br>`eyxxxxxxxxxxxx==` | for pulling an image |

<br>

##### Create an image layer from a Directory or File(s):

Can be acomplished in many ways:

1. by not defining any `(from_xxx)` nor `--archive` nor `--rootfs` nor `pathname`: this will default with root at `$PWD/rootfs` and pathname=`.`
2. by defining only `pathname`, e.g. `.` : the layer `rootfs` is the current Directory. 
3. with `--rootfs` and `pathname` defined
4. with `--rootfs` only:

|option| rootfs path |
|------------------|------------------|
|`--rootfs`| `$PWD/rootfs` |
|`--rootfs=test`|`$PWD/test` |
|`--rootfs=/test`| `/test` |
| `--rootfs=`<br>`$CI_PROJECT_DIR` | `project_dir` |



<br>

<div class="alert alert-warning" role="alert"><strong>Warning!</strong><br> The Owner and Group of all files and directories are changed to <code>root:root</code> </div>

<br>
<div class="alert alert-primary" role="alert"> <strong>tip</strong> Use the <code>--u2g</code> options to copy owner permisions to the group file/directory permissions, and thus have a writable image layer as a <code>non-root:root</code> run user.</div>
<br>

##### Create an image layer from an archive.

- `--archive` defaults to layer.tar.gz at current directory
- `--archive=myarchive.tar.gz`

The archive is streamed as is, while content and blob SHA256SUM is calculated for manifest and/or config creation.

<br>
<div class="alert alert-warning"><strong>Warning!</strong> <code>--archive</code> cannot be used in conjunction with <code>--rootfs</code> **nor** <code>pathname</code> </div>
<br>


##### Create an image layer from an image.

1. create image layers `--from_image=repository_name`
    - dockerhub: `centos:latest`
    - Full Repo Name: `registry.gitlab.com/mygroup/myproject[myname][:myreference]`
    - ImageStream: `ImageStream[/myname][:myreference]`
    - Gitlab_registry: `Gitlab[/myname][:myreference]`

2. create image layers from ImageStream or Gitlab-registry
    - Define from which registry
        - `--from_ISR`
        - `--from_GLR`
    - Optional define `--from_image_name=myname`
    - Optional define `--from_reference=myreference` defaults to `latest`

<br>

##### Docker Config

 **`--config[=docker_config.yml]`**

to modify or define the image configuration, we define it in a `yaml` file.

**Sample `docker_config.yml`**

```yaml
Hostname: "test"
# Delete Entrypoint if present in --from_image
Entrypoint: null
WorkDir: "/"
Cmd:
  # delete all Cmd Entries if present in --from_image
  - _*: null
  # and add new entries for Cmd
  - /bin/bash
  - -c
  - echo hello
User: ""
Volumes:
  # delete volume /home if present in --from_image, and add /build
  - /home: null
  - /build
ExportPorts:
  - 8080/tcp
  # and delete 9090/udp entry if present in --from_image
  - 9090/udp: null
Labels:
  maintainer: "$GITLAB_USER_EMAIL"
  title: |
          Test Image for building with --config
  description: |
             Test Image for building and testing odagrun :
             Push and so on
  vendor: Gioxa Ltd. HongKong                
  tags: "$IMAGE_TAGS"
```

Parsing of the `docker_config.yml`:

- support for bash variable substitution, if the StringValue value has double quotes
- Single quotes will force a string=> no variable substitution.
- a number or True or False without quotes will result in a json-number and json-boolean object!

**On a Merge** *(when using `--from_image=....`)*

Old value's are merged,deleted or overwritten.

- Set an item as:

    ```yaml
  key/value-item: null
```
and  all old `key:values` are deleted and the `key/value-item` is set to null.

-  set `key/value` with

    ```yaml
key/value-item:
  _*: null
  key: value
```
to delete all old `key:value` and append new values on Merge

- set an `key/value` to

    ```yaml
key/value-item:
  key: null
```
to delete that key on Merge

- set to "" :

    ```yaml
key/value-item:
  key:
```

will set `"key": ""` after merge, no delete!!

<br>

#### Autolabelling

when building a docker image from a `rootfs` or `archive`, labels are automatically added:

- For images created from a `rootfs` or `archive`:

```yaml
labels:
  com.odagrun-version: 0.0.33
  com.odagrun.build.image-digest: sha256:2a61f8abd6250751c4b1dd3384a2bdd8f87e0e60d11c064b8a90e2e552fee2d7
  com.odagrun.build.image: registry.hub.docker.com/library/centos:7.4.1708
  com.odagrun.build.image-nick: centos:7.4.1708
  com.odagrun.build.image-tag: 7.4.1708
  com.odagrun.build.commit.author: Danny <danny.goossen@gioxa.com>
  com.odagrun.build.commit.id: e2b011bfbecaa4a6a843cc3014d54ed891344e37
  com.odagrun.build.commit.message: prepend project dir for workspaces if not start with /
  com.odagrun.build.commit.ref: 52-change-registry-push-content-to-allow-bash-like-expansion
  com.odagrun.build.job-url: https://gitlab.gioxa.com/deployctl/odagrun/-/jobs/49769
  com.odagrun.build.environment: production
  com.odagrun.build.source.tree: https://gitlab.gioxa.com/deployctl/odagrun/tree/e2b011bf
```

- Additional for all(`*`) `registry_push` with a `--from_xxx` :

```yaml
labels:
  com.odagrun.build.from-image.tag: latest
  com.odagrun.build.from-image.digest: sha256:2a61f8abd6250751c4b1dd3384a2bdd8f87e0e60d11c064b8a90e2e552fee2d7
  com.odagrun.build.from-image.nick-name: centos
  com.odagrun.build.from-image.docker-repo: registry.hub.docker.com/library/centos
```

<br>
<div class="alert alert-warning" role="alert"><strong>Note!</strong><br> Unless <code>--skip_label</code> was defined, <br>the <code>com.odagrun.build.from-xxxx</code> labels are skipped!</div>
<br>

- See [variables](#odagrun-variables) section for the [org.opencontainer.image](https://github.com/opencontainers/image-spec/blob/master/annotations.md#pre-defined-annotation-keys) label scheme.

<br>

### Examples Registry Push

#### keep it real simple

```bash
registry_push
```

**OR**

```bash
registry_push \
  [--GLR] \
  [--reference=latest] \
  [--rootfs[=./rootfs]]
```

- will create from `./rootfs` directory a new image layer
- and push that to registry as `Gilab:latest`
- externaly availleble as `$CI_REGISTRY_IMAGE:latest`

<br>
<div class="alert alert-info" role="alert"><strong> Defaults:</strong> The <code>[optional]</code> items in above example are the defaults!</div>
<br>

#### keep it simple

```bash
registry_push --name=test .
```

- will create from `.` of the current directory a new image layer
- pushed to registry as `Gilab:latest`
- externaly availleble as `$CI_REGISTRY_IMAGE:latest`

<br>

#### Create single layer image from defined filesystem:

```bash
registry_push \
--ISR \
--rootfs=/ \
--u2g \
/opt/el7/x86_64/*.rpm \
/etc/myconfig
```

- will create an image layer
- push it to the ImageStream,
- will be containing:
  - `/opt/el7/x86_64/*.rpm`
  - `/etc/myconfig`
- will have same group permissions (root) as owner (non-root)
- accesible in CI as:
  - `ImageStream`
  - `ImageStream:latest`

<br>

#### Create single layer from existing archive

```bash
registry_push \
  --archive \
  --ISR \
  --name=temp1 \
  --reference=first
```

- will create a single layer image from `./layer.tar.gz`
- push it to the ImageStream
- accesible in CI as: `ImageStream/temp1:first`

<br>

#### Append a rootfs to an existing image

```bash
registry_push \
--from_image=centos \
[--GLR] \
--name=test \
--reference=1 \
--rootfs[=./rootfs] \
/opt/el7/x86_64/*.rpm \
/etc/myconfig
```

- will append a new layer to `centos`
- from `./rootfs`
- with content `/rootfs/opt/el7/x86_64/*.rpm` and `/etc/myconfig`
- and push it to Gitlab registry `$CI_REGISTRY_IMAGE/test:1`
- accesible in CI as: `Gitlab/test:1`

<br>

#### Transfer an Image

```bash
registry_push \
--from_image=ubuntu \
--GLR \
--name=ubuntu \
--reference=1 \
--config=myubuntu.yaml
```

- transfer an image from a docker repository to our Gilab-Registry/project_path
- with modified docker config as defined in `./myubuntu.yaml`
- and pushed it to Gitlab registry `$CI_REGISTRY_IMAGE/ubuntu:1`
- accesible in CI as:`Gitlab/ubuntu:1`

<br>

#### Transfer Image without history

```bash
registry_push \
--from_image=Gitlab/ubuntu:1 \
--image=mynamespace/ubuntu:1 \
--skip-label
```

- transfer an image from our Gitlab registry to dockerhub
- skip history layer, so we keep info regarding the build in the labels from the build phase.

<br>


### Remote Tag an Image


remote tag an existing image in a repository, without downloading the layers!


```bash
registry_tag_image \
   [--image=repo-name] | \
   [ [--name=name] [--GLR | [--ISR]] [--reference=reference] ]
   --tag=new-tag
```

Defaults to:

`reference`: latest

available for use as build image as:

- default (`--ISR`) :

    `ImageStream[/<name>]:[<new-tag>]`
- with `--GLR`:

    `Gitlab[/<name>]:[<new-tag>]`

and external as:

- with default (`--ISR`):

    `<oc-registry>/<oc-namespace>/is-<CI_PROJECT_PATH>[-<name>]:<new-tag>`
- with `--GLR`:

    `<CI-REGISTRY-IMAGE>[/<name>]:<new-tag>`

<br>

[top](#odagrun)

