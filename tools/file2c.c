//
//  file2c.c
//  deployctl
//
//  Created by Danny Goossen on 13/6/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

//
//  escape.c
//  tBlast_tunnel
//
//  Created by Danny Goossen on 25/4/16.
//  Copyright (c) 2016 Danny Goossen. All rights reserved.
//

#ifdef WINHOST
#include "../tblast_src/compat.h"
#endif


#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

#define CHUNK (1024*512)



static FILE *open_file ( char *file, char *mode )
{
	FILE *fp = fopen ( file, mode );
	
	if ( fp == NULL ) {
		perror ( "Unable to open file" );
		exit ( EXIT_FAILURE );
	}
	
	return fp;
}

int main ( int argc, char *argv[] )
{
	//u_int8_t ch;
	FILE *in;
	FILE *out,*out_h;
	unsigned char * out_buf=NULL;
	ssize_t out_size;
	ssize_t file_size;
	
	if ( argc != 4 ) {
		fprintf ( stderr, "Usage: %s <readfile> <output_path> <name>\n", argv[0] );
		exit ( EXIT_FAILURE );
	}
	else
		printf("reading: %s\r\n...\r\n",argv[1]);
	ssize_t len=strlen(argv[2]) + strlen(argv[3]);
	
	char *out_name_c=calloc(1, len+6);
	char *out_name_h=calloc(1, len+6);
	
	sprintf(out_name_c, "%s/%s.c",argv[2],argv[3]);
	sprintf(out_name_h, "%s/%s.h",argv[2],argv[3]);
	
	
	in = open_file ( argv[1], "rb" );
	fseek(in, 1, SEEK_END);
	
	file_size = ftello(in)-1;
	if (file_size == -1) {
		/* Handle error */
	}
	rewind(in);
	
	/*
	 if( def(in, &out_buf, file_size, &out_size, 1)!=0)
	 {
	 if (out_buf) free(out_buf);
	 fclose ( in );
	 exit(1);
	 }
	 */
	//fread(&my_record,sizeof(struct rec),1,ptr_myfile);
	
	out_buf=calloc(1,file_size);
	out_size=fread(out_buf,1,file_size,in);

	out = open_file ( out_name_c, "w" );
	out_h = open_file ( out_name_h, "w" );
	
	char * clean_name=calloc(1,strlen(argv[3])+1);
	sprintf(clean_name,"%s",argv[3]);
	int  i=0;
	for (i=0;i<strlen(argv[3]);i++)
	{
		if (clean_name[i]=='-' || clean_name[i]=='.') clean_name[i]='_';
	}
	
	fprintf(out_h,"#ifndef tb_%s_h\n",clean_name);
	fprintf(out_h,"#define tb_%s_h\n",clean_name);
	
	fprintf(out_h,"#define %s_len %zd\n",clean_name,out_size);
	fprintf(out_h,"#define %s_source \"%s\"\n",clean_name,argv[1]);
	fprintf(out_h,"extern const char %s[%zd];\n",clean_name,out_size);
	fprintf(out_h,"#endif\n");
	
	fprintf(out,"const char %s[%zd] = \n\t{\n\t",clean_name,out_size);
	
	ssize_t thiscount=0;
	while ( thiscount < out_size)
	{
		fprintf(out,"0x%02x",out_buf[thiscount]);
		if (thiscount < out_size -1)
			fprintf(out,", ");
		else
			fprintf(out,"\n\t");
		thiscount++;
		if (! (thiscount%32)  ) fprintf(out,"\n\t");
	}
	fprintf(out,"};");
	fclose ( in );
	fclose ( out );
	fclose(out_h);
	printf("wrote: %s/%s.c \n",argv[2],argv[3]);
	printf("wrote: %s/%s.h",argv[2],argv[3]);
	printf(" Content lengt : %zd\n",out_size);
	
	return EXIT_SUCCESS;
}